import { environment } from './../environments/environment';

export const AppSettings = {
    apiurls: environment.APIURLS,
};