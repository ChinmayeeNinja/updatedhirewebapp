import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import {ViewcredentialComponent} from '../credentialRequest/viewcredential/viewcredential.component'
import {PharmacyCredentialComponent} from '../credentialRequest/pharmacy-credential/pharmacy-credential.component'
import { from } from 'rxjs';
import {PharmacistCredentialComponent} from '../credentialRequest/pharmacist-credential/pharmacist-credential.component'
const routes: Routes = [
  {
  path: '',
  component: ViewcredentialComponent,
  children: [
    { path: 'credentialrequest', component: PharmacyCredentialComponent },

    { path: 'pharmacist-credential-request', component: PharmacistCredentialComponent },
  ],
  
},
]
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CredentialRequestRoutingModule { }
