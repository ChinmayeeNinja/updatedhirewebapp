import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ViewcredentialComponent} from '../credentialRequest/viewcredential/viewcredential.component'
import {PharmacyCredentialComponent} from '../credentialRequest/pharmacy-credential/pharmacy-credential.component'
import { from } from 'rxjs';
import {PharmacistCredentialComponent} from '../credentialRequest/pharmacist-credential/pharmacist-credential.component'
import { ToastrModule, ToastNoAnimation, ToastNoAnimationModule } from 'ngx-toastr';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {CredentialRequestRoutingModule} from './credential-request-routing.module'
@NgModule({
  declarations: [
    PharmacyCredentialComponent,
    ViewcredentialComponent,
    PharmacistCredentialComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    CredentialRequestRoutingModule,
    ReactiveFormsModule
  ]
})
export class CredentialRequestModule { }
