import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { AppSettings } from '../../../app.settings';
import { Observable, throwError, from, Subject } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Headers, RequestOptions, Request, RequestMethod } from '@angular/http';
@Injectable({
  providedIn: 'root'
})
export class CredentialRequestService {

  constructor(private http: HttpClient) { }

  public GetPharmacyCredentialList(page): Observable<any> {
    return this.http
      .get(
        AppSettings.apiurls.LISTPHARMACYCREDENTIALREQUEST +`&limit=10&offset=${(page - 1) * 10}`
        )
      .pipe(
        map((res) => {
          console.log(res);
          return res;
        }),
        catchError(this.errorHandler)
      );
   }

   public GetPharmacistCredentialList(page): Observable<any> {
    return this.http
      .get(
        AppSettings.apiurls.LISTPHARMACISTCREDENTIALREQUEST +`&limit=10&offset=${(page - 1) * 10}`)
      .pipe(
        map((res) => {
          console.log(res);
          return res;
        }),
        catchError(this.errorHandler)
      );
   }

   updateCredential(credentialdataData, credentialidId) {
    return this.http
      .put<any>(AppSettings.apiurls.UPDATECREDENTIAL + credentialidId, credentialdataData)
      .pipe(
        map((credential) => {
          console.log(
            'Pharmacy User Data--------->',
            JSON.stringify(credential)
          );
          return JSON.stringify(credential);
        }),
        catchError(this.errorHandler)
      );
  }

   errorHandler(error: any) {
    console.log('Error--->', error);
    return throwError(error);
  }
}
