import { Component, OnInit } from '@angular/core';
import { CredentialRequestService } from '../credential-request.service';
import { first } from 'rxjs/operators';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import {ToastrService} from '../../../../toastr.service'
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-pharmacist-credential',
  templateUrl: './pharmacist-credential.component.html',
  styleUrls: ['./pharmacist-credential.component.css']
})
export class PharmacistCredentialComponent implements OnInit {
  formVar: FormGroup;
public  credentialpharmacistlist =[];
public errorMessage :any;
public collectionsize :any;
page = 1;
submitted = false;
public modalShow = false;
public licensearr =[];
public refid :any;
public licensetype:any;
  constructor(
    private pharmacycredential: CredentialRequestService,
    private router: Router,
    private fb: FormBuilder,
    private tosterservice:ToastrService,
    private spinner:NgxSpinnerService,
  ) {
this.licensearr =[{key:'Vaccination',value:1},{key:'Pharmacist',value:2}]
  }
  ngOnInit(): void {
    this.formVar = this.fb.group({
     
    });
    let role = JSON.parse(localStorage.getItem('currentUser')).data?.role
    if(role===1){
      this.pharmacycredentiallist(this.page)
    }
   else if(role===5){
      this.pharmacycredentiallist(this.page)
    }
    else{
      this.router.navigate(['/login']);

    }
  }
  reloadData(){
    this.pharmacycredentiallist(this.page)

  }

  deletedetalis(credentialreferenceid){
    var request = {
      status:"declined"

    }
    Swal.fire({
  title: 'Are you sure?',
  text: "You want to decline the pharmacist credential !",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, decline it!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Declined!',
      'Your pharmacist credential has been declined.',
      'success'
    )
      this.pharmacycredential
    .updateCredential(request, credentialreferenceid)
    .subscribe(
      (data) => {
        if (data) {
          this.errorMessage = '';
            // this.tosterservice.Success('Your jobpost has been deleted Successfully')
            this.pharmacycredentiallist(this.page)

        } else {
          this.errorMessage = 'Unable to Save credential decline';
        }
      },
      (error) => {
        this.errorMessage = error.error;
      }
    );
  }
})
  
  }
  modalHideShow(refid) {
    this.formVar.reset();
    this.submitted = false;
      this.modalShow = !this.modalShow;
      this.refid=refid
    }
    modalHideShoww() {
        this.modalShow = !this.modalShow;
  
  }
  checkboxclick(valuee){
    // alert(valuee)
this.licensetype=valuee;
  }
  onSubmit(){
var request={
  status:"pending",
  "licenseType":this.licensetype,
}
  this.pharmacycredential
    .updateCredential(request, this.refid)
    .subscribe(
      (data) => {
        if (data) {
          this.errorMessage = '';
          this.modalShow = !this.modalShow;

            // this.tosterservice.Success('Your jobpost has been deleted Successfully')
            this.pharmacycredentiallist(this.page)

        } else {
          this.errorMessage = 'Unable to Save credential decline';
        }
      },
      (error) => {
        this.errorMessage = error.error;
      }
    );
}
  
  pharmacycredentiallist(page): void {
    this.spinner.show()

    this.pharmacycredential
      .GetPharmacistCredentialList(page)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            var dataaa = data;
             this.collectionsize = dataaa?.data?.pagination?.totalCount;

            this.credentialpharmacistlist = dataaa?.data?.listResult;
            this.spinner.hide()

          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }

}
