import { Component, OnInit } from '@angular/core';
import { CredentialRequestService } from '../credential-request.service';
import { first } from 'rxjs/operators';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import {ToastrService} from '../../../../toastr.service'
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-pharmacy-credential',
  templateUrl: './pharmacy-credential.component.html',
  styleUrls: ['./pharmacy-credential.component.css']
})
export class PharmacyCredentialComponent implements OnInit {
public credentialpharmacylist =[];
public errorMessage :any;
public collectionsize :any;
public page = 1;
  constructor(
    private pharmacycredential: CredentialRequestService,
    private router: Router,
          private fb: FormBuilder,
    private tosterservice:ToastrService,
    private spinner:NgxSpinnerService,

  ) {}
  ngOnInit(): void {
    let role = JSON.parse(localStorage.getItem('currentUser')).data?.role
    if(role===1){
      this.pharmacycredentiallist(this.page)
    }
   else if(role===5){
      this.pharmacycredentiallist(this.page)
    }
    else{
      this.router.navigate(['/login']);

    }
  }

  pharmacycredentiallist(page): void {
    this.spinner.show()
    this.pharmacycredential
      .GetPharmacyCredentialList(page)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            var dataaa = data;
             this.collectionsize = dataaa?.data?.pagination?.totalCount;

            this.credentialpharmacylist = dataaa?.data?.listResult;
            this.spinner.hide()

          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }
}
