import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-viewcredential',
  templateUrl: './viewcredential.component.html',
  styleUrls: ['./viewcredential.component.css']
})
export class ViewcredentialComponent implements OnInit {

  constructor(    
    private router: Router,
    ) { }

  ngOnInit(): void {
  }
  pharmacycredential() {
    this.router.navigate(['/superadmin/credentialrequest']);
    }
  
    pharmacistcredential() {
    this.router.navigate(['/superadmin/pharmacist-credential-request']);
  }
  isLinkActive(url): boolean {
    const queryParamsIndex = this.router.url.indexOf('?');
    const baseUrl =
      queryParamsIndex === -1
        ? this.router.url
        : this.router.url.slice(0, queryParamsIndex);
    return baseUrl === url;
  }
}
