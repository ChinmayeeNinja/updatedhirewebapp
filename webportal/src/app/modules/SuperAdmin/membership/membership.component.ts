import { Component, OnInit } from '@angular/core';
import { MembershipService } from '../membership/membership.service';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ToastrService } from '../../../toastr.service';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-membership',
  templateUrl: './membership.component.html',
  styleUrls: ['./membership.component.css'],
})
export class MembershipComponent implements OnInit {
  formVar: FormGroup;
  public errorMessage: any;
  submitted = false;
  public modalShow = false;
  public membershiplist = [];
  public membershiprefid: any;
  constructor(
    private membership: MembershipService,
    private router: Router,
    private tosterservice: ToastrService,
    private fb: FormBuilder,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.formVar = this.fb.group({
      name: ['', [Validators.required,Validators.pattern(/^[A-Za-z]+$/)]],
      state: ['', [Validators.required,Validators.pattern(/^[A-Za-z]+$/)]],
      county: ['', [Validators.required,Validators.pattern(/^[A-Za-z]+$/)]],
      monthlyCost: ['', Validators.required],
      contractLength: ['', Validators.required],
    });
    let role = JSON.parse(localStorage.getItem('currentUser')).data?.role;
    if (role === 1) {
      this.getmembershipdetail();
    } else if (role === 5) {
      this.getmembershipdetail();
    } else {
      this.router.navigate(['/login']);
    }
  }
  get f() {
    return this.formVar.controls;
  }
  modalHideShow() {
    this.formVar.reset();
    this.submitted = false;
    this.modalShow = !this.modalShow;
  }
  modalHideShoww() {
    this.modalShow = !this.modalShow;
  }

  getmembershipdetail(): void {
    this.spinner.show();
    this.membership
      .GetMembershipList()
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            this.membershiplist = data?.data;
            this.spinner.hide();
          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }

  onlyNumber(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }

    return true;
  }
  onlyText(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (
      charCode >= 48 &&
      charCode <= 57 &&
      (charCode >= 96 || charCode <= 105)
    ) {
      //console.log(charCode)
      return false;
    }
    return true;
  }

  onSubmit() {
    this.submitted = true;
    if (this.formVar.invalid) {
      return;
    }
    if (
      this.membershiplist.filter(
        (item, i) => item.name === this.formVar.value.name
      ).length > 0
    ) {
      this.tosterservice.Error(
        'Membership Alredy Exist.Please Enter Different Membership Details'
      );
    }
    else{
      if (this.membershiprefid) {
        this.membership
          .Updatemembership(this.membershiprefid, this.formVar.value)
          .subscribe(
            (dataa) => {
              if (dataa) {
                this.errorMessage = '';
                this.tosterservice.Success(
                  'Your Membership Updated Successfully'
                );
                this.getmembershipdetail();
                this.modalShow = false;
              } else {
                this.errorMessage = 'Unable to Update membership';
              }
            },
            (error) => {
              this.errorMessage = error.error;
            }
          );
      } else {
        this.membership.Createmembership(this.formVar.value).subscribe(
          (dataa) => {
            if (dataa) {
              this.errorMessage = '';
              this.tosterservice.Success('Your Membership Created Successfully');
              this.getmembershipdetail();
              this.modalShow = false;
            } else {
              this.errorMessage = 'Unable to Create membership';
            }
          },
          (error) => {
            this.errorMessage = error.error;
          }
        );
      }
    }
  
  }

  Editmembership(
    membershiprefId,
    name,
    state,
    country,
    contractlength,
    monthlycost
  ) {
    this.formVar.reset();
    this.submitted = false;
    this.modalShow = !this.modalShow;
    this.membershiprefid = membershiprefId;
    if (this.membershiprefid === null) {
      this.formVar.patchValue({
        name: this.formVar.value.name,
        state: this.formVar.value.state,
        county: this.formVar.value.county,
        monthlyCost: this.formVar.value.monthlyCost,
        contractLength: this.formVar.value.contractLength,
      });
    } else {
      this.formVar.patchValue({
        name: name,
        state: state,
        county: country,
        monthlyCost: monthlycost,
        contractLength: contractlength,
      });
    }
  }

  Deletemembership(membershiprefId) {
    var request = {
      isDeleted: 1,
    };
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to delete this membership card!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire('Deleted!', 'Your card has been deleted.', 'success');
        this.membership.Updatemembership(membershiprefId, request).subscribe(
          (data) => {
            if (data) {
              this.errorMessage = '';
              this.getmembershipdetail();
            } else {
              this.errorMessage = 'Unable to Dalete Membership ';
              console.log('Unable to Dalete Membership');
            }
          },
          (error) => {
            console.log('Error Message: ', error.error);
            this.errorMessage = error.error;
          }
        );
      }
    });
  }
}
