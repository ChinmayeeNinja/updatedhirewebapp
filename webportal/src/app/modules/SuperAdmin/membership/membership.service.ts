import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { AppSettings } from '../../../app.settings';
import { Observable, throwError, from, Subject } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Headers, RequestOptions, Request, RequestMethod } from '@angular/http';
@Injectable({
  providedIn: 'root'
})
export class MembershipService {

  constructor(private http: HttpClient) { }

  public GetMembershipList(): Observable<any> {
    return this.http
      .get(
        AppSettings.apiurls.LISTMEMBERSHIP)
      .pipe(
        map((res) => {
          console.log(res);
          return res;
        }),
        catchError(this.errorHandler)
      );
   }


  Createmembership(data) {
    return this.http
      .post<any>(AppSettings.apiurls.CREATEMEMBERSHIP, { ...data })
      .pipe(
        map((response) => {
          return JSON.stringify(response);
        }),
        catchError(this.errorHandler)
      );
  }

  Updatemembership(refid ,data) {
    return this.http
      .put<any>(AppSettings.apiurls.UPDATEMEMBERSHIP + refid,{...data})
      .pipe(
        map((response) => {
          return JSON.stringify(response);
        }),
        catchError(this.errorHandler)
      );
  }

  errorHandler(error: any) {
    console.log('Error--->', error);
    return throwError(error);
  }
}
