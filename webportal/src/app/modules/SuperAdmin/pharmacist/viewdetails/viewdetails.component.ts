import { Component, OnInit } from '@angular/core';
import { PharmacistService } from '../pharmacist.service';
import { first } from 'rxjs/operators';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import {ToastrService} from '../../../../toastr.service'
import { NgxSpinnerService } from "ngx-spinner";
@Component({
  selector: 'app-viewdetails',
  templateUrl: './viewdetails.component.html',
  styleUrls: ['./viewdetails.component.css']
})
export class ViewdetailsComponent implements OnInit {
  public errorMessage: any;
   public pharmacistuserreferenceid: any;
  public pharmacyuserId: any
  public name:any;
  public email:any;
  public phone:any
  public pharmacistlist =[]
  constructor(private Pharmacist: PharmacistService,
    private router: Router,
          private fb: FormBuilder,
          private route: ActivatedRoute,
    private tosterservice:ToastrService,
    private spinner : NgxSpinnerService) { }

  ngOnInit(): void {
    var sub = this.route.queryParams.subscribe((params) => {
      this.pharmacistuserreferenceid = atob(params.id);
      this.pharmacyuserId = JSON.parse(atob(params.pharmacyuserId));
      this.name=atob(params.name)
      this.email=atob(params.email)
      this.phone=atob(params.phone)
     });
     this.getviewpharmacistlist(this.pharmacistuserreferenceid)
  }
  getviewpharmacistlist(pharmacistuserreferenceid): void {
    console.log('Inside getjoblisting');
    this.Pharmacist
      .getpharmacistdetaillist(pharmacistuserreferenceid)
      .pipe(first())
      .subscribe(
        (data) => {
          console.log(data);
          if (data) {
            var dataaa = data;

            this.pharmacistlist = dataaa?.data;
            this.errorMessage = '';
          }
        },
        (error) => {
          console.log('#66Error Message: ', error);
          this.errorMessage = error.text;
        }
      );
  }

}
