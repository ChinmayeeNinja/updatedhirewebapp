import { Component, OnInit } from '@angular/core';
import { PharmacyService } from '../pharmacy.service';
import { first } from 'rxjs/operators';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import {ToastrService} from '../../../../toastr.service'
@Component({
  selector: 'app-account-details',
  templateUrl: './account-details.component.html',
  styleUrls: ['./account-details.component.css']
})
export class AccountDetailsComponent implements OnInit {
 
  public errorMessage: any;
  public pharmacylist = [];
   public pharmacyuserreferenceid: any;
  public pharmacyuserId: any
  public superadminUserDetails =[];
  public list : any;
  public name:any;
  public email:any;
  public phone:any
  constructor(
     private Pharmacy: PharmacyService,
    private router: Router,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private tosterservice:ToastrService
  ) { }

  ngOnInit(): void {
     var sub = this.route.queryParams.subscribe((params) => {
      this.pharmacyuserreferenceid = atob(params.id);
      this.pharmacyuserId = JSON.parse(atob(params.pharmacyuserId));
      this.name=atob(params.name)
      this.email=atob(params.email)
      this.phone=atob(params.phone)
     });
    this.superadminUserDetails.push(JSON.parse(localStorage.getItem('currentUser')))
    // console.log(this.superadminUserDetails)
    this.list =  JSON.parse(localStorage.getItem('pharmacylist'))
        this.getjoblisting(this.pharmacyuserreferenceid);

  }


 getjoblisting(pharmacyRefrenceId): void {
    this.Pharmacy
      .getpharmacydetaillist(pharmacyRefrenceId)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            var dataaa = data;

            this.pharmacylist = dataaa?.data;
            this.errorMessage = '';
          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }
}
