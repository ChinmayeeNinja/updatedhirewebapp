import { Component, OnInit } from '@angular/core';
import { PharmacyService } from '../pharmacy.service';
import { first } from 'rxjs/operators';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import {ToastrService} from '../../../../toastr.service'
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-joblisting',
  templateUrl: './joblisting.component.html',
  styleUrls: ['./joblisting.component.css']
})
export class JoblistingComponent implements OnInit {
Object = Object;
  open = false;
  close =true
  page = 1;
  public collectionsize: number;
  public errorMessage: any;
  public jobListing = {};
  public clickedid = ""
   public pharmacyuserreferenceid: any;
  public pharmacyuserId : any
  constructor(
  private Pharmacy: PharmacyService,
    private router: Router,
    private fb: FormBuilder,
        private route: ActivatedRoute,
    private tosterservice:ToastrService,
    private spinner : NgxSpinnerService

  ) { }

  ngOnInit(): void {
       var sub = this.route.queryParams.subscribe((params) => {
      this.pharmacyuserreferenceid = atob(params.id);
      this.pharmacyuserId = JSON.parse(atob(params.pharmacyuserId));
    });
        this.getjoblisting(this.pharmacyuserreferenceid, this.page);

  }


    openclose(id) {
    this.open = !this.open
    this.clickedid = id
  }
  closeopen(id) {
      this.close =true
    this.clickedid = ""

  }
  reloadData() {
        this.getjoblisting(this.pharmacyuserreferenceid, this.page);
  }
  
  viewjobdetals(id, jobId) {
  
  }

  editjobdetails(id) {
    
  }
  toNumber(n){
return Number(n)
  }
  deletejobdetalis(jobReferenceId){
    var request = {
      isDeleted: 1

    }
    Swal.fire({
  title: 'Are you sure?',
  text: "You want to delete this jobpost!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deleted!',
      'Your jobpost has been deleted.',
      'success'
    )
      this.Pharmacy
    .updateJobPost(request, jobReferenceId)
    .subscribe(
      (data) => {
        if (data) {
          this.errorMessage = '';
            // this.tosterservice.Success('Your jobpost has been deleted Successfully')
            this.getjoblisting(this.pharmacyuserreferenceid, this.page);

        } else {
          this.errorMessage = 'Unable to Save Jobpost';
        }
      },
      (error) => {
        this.errorMessage = error.error;
      }
    );
  }
})
  
  }

  getjoblisting(pharmacyRefrenceId, page): void {
    this.spinner.show()
    this.Pharmacy
      .getPharmacyJoblisting(pharmacyRefrenceId, page)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            var dataaa = data;
            this.collectionsize = dataaa?.data?.pagination?.totalCount;

            this.jobListing = dataaa?.data?.listResult;
            this.errorMessage = '';
            this.spinner.hide()

          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }

}
