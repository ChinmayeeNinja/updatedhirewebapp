import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PharmacylistComponent } from './pharmacylist/pharmacylist.component';
import { JoblistingComponent } from './joblisting/joblisting.component';
import { AccountDetailsComponent } from './account-details/account-details.component';
import { WalletComponent } from './wallet/wallet.component';
import { RatingReviewComponent } from './rating-review/rating-review.component';
import { ViewDetailComponent } from './view-detail/view-detail.component';

const routes: Routes = [
  {
    path: '',
    component: PharmacylistComponent,
      },
      { path: 'viewdetails', component: ViewDetailComponent ,

    children: [
        {
            path: '',
            component: AccountDetailsComponent,
          },
          { path: 'joblisting', component: JoblistingComponent },
        
          { path: 'wallet', component: WalletComponent },
          { path: 'rating&review', component: RatingReviewComponent },
        ],
    },
]
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PharmacyRoutingModule { }
