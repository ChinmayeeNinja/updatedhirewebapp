import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { BrowserModule } from '@angular/platform-browser';
import { PharmacylistComponent } from './pharmacylist/pharmacylist.component';
import { JoblistingComponent } from './joblisting/joblisting.component';
import { AccountDetailsComponent } from './account-details/account-details.component';
import { WalletComponent } from './wallet/wallet.component';
import { RatingReviewComponent } from './rating-review/rating-review.component';
import { ViewDetailComponent } from './view-detail/view-detail.component';
import {PharmacyRoutingModule} from './pharmacy-routing.module'
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EditComponent } from './edit/edit.component';


@NgModule({
  declarations:
    [
      PharmacylistComponent,
      JoblistingComponent,
      AccountDetailsComponent,
      WalletComponent,
      RatingReviewComponent,
      ViewDetailComponent,
      EditComponent
    ],
  imports: [
FormsModule,
    ReactiveFormsModule,
    CommonModule,
    PharmacyRoutingModule,
    HttpClientModule,
    NgbModule,
    // BrowserModule
  ]
})
export class PharmacyModule { }
