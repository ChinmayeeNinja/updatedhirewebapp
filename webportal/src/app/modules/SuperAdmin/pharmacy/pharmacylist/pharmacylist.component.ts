import { Component, OnInit } from '@angular/core';
import { PharmacyService } from '../pharmacy.service';
import { first } from 'rxjs/operators';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import {ToastrService} from '../../../../toastr.service'
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-pharmacylist',
  templateUrl: './pharmacylist.component.html',
  styleUrls: ['./pharmacylist.component.css']
})
export class PharmacylistComponent implements OnInit {
    formVar: FormGroup;
  public errorMessage: any
  public collectionsize: any;
  public pharmacylist = [];
    page = 1;
  public clickedid = ""
  open = false;
  close =true
  public filter='all';
   public valuee :'';
   public filterkey=[];
 constructor(
    private Pharmacy: PharmacyService,
    private router: Router,
          private fb: FormBuilder,
    private tosterservice:ToastrService,
    private spinner : NgxSpinnerService

  ) {}
  ngOnInit(): void {
    this.filterkey=[{key:'All',value:'all'},{key:'Name',value:'name'},{key:'Status',value:'status'},{key:'Pharmacies',value:'pharmacies'}]
   
    this.formVar = this.fb.group({
      search: [''],

    });
    let role = JSON.parse(localStorage.getItem('currentUser')).data?.role
    if(role===1){
      this.Totalpharmacylist(this.filter,this.valuee,this.page)
    }
   else if(role===5){
      this.Totalpharmacylist(this.filter,this.valuee,this.page)
    }
    else{
      this.router.navigate(['/login']);

    }
  }
   openclose(id) {
    this.open = !this.open
    this.clickedid = id
  }
  closeopen(id) {
      this.close =true
    this.clickedid = ""

  }
    reloadData() {
    this.Totalpharmacylist(this.filter,this.valuee,this.page)
  }
  viewdetails(refid, pharmacyId,email,name,phone,walletPoints) {
    this.router.navigate(['/superadmin/pharmacy/viewdetails'], {
      queryParams: { id: btoa(refid), pharmacyuserId: btoa(pharmacyId),name:btoa(name),email:btoa(email),phone:btoa(phone),points:btoa(walletPoints) },
    });

  }
 
    editdetails(id) {
    }
  toNumber(n){
return Number(n)
  }
  deletedetalis(pharmacyUserReferenceId){
    var request = {
      isDeleted: 1

    }
    Swal.fire({
  title: 'Are you sure?',
  text: "You want to delete this jobpost!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deleted!',
      'Your jobpost has been deleted.',
      'success'
    )
      this.Pharmacy
    .updatePharmacy(request, pharmacyUserReferenceId)
    .subscribe(
      (data) => {
        if (data) {
          this.errorMessage = '';
            // this.tosterservice.Success('Your jobpost has been deleted Successfully')
            this.Totalpharmacylist(this.filter,this.valuee,this.page)

        } else {
          this.errorMessage = 'Unable to Save Jobpost';
        }
      },
      (error) => {
        this.errorMessage = error.error;
      }
    );
  }
})
  
  }

  searchkey(event){
    this.valuee=event
    this.Totalpharmacylist(this.filter,this.valuee,this.page)
  }
 
  onOptionsSelected(filtername){
    this.filter=filtername
    if(this.valuee === this.formVar.value.search){
      this.Totalpharmacylist(this.filter,this.valuee,this.page)
    }
      }


 Totalpharmacylist(filter,value,page): void {
  this.spinner.show();

    this.Pharmacy
      .getpharmacylist(filter,value,page)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            var dataaa = data;
             this.collectionsize = dataaa?.data?.pagination?.totalCount;

            this.pharmacylist = dataaa?.data?.listResult;
            this.spinner.hide();

          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }
}
