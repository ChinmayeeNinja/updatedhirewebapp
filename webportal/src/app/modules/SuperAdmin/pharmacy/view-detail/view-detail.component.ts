import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
@Component({
  selector: 'app-view-detail',
  templateUrl: './view-detail.component.html',
  styleUrls: ['./view-detail.component.css']
})
export class ViewDetailComponent implements OnInit {
  public pharmacyuserreferenceid: any;
  public pharmacyuserId : any;
  public name :any;
  public email:any;
  public phone:any;
  public points :any
  constructor(
     private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
       var sub = this.route.queryParams.subscribe((params) => {
      this.pharmacyuserreferenceid = atob(params.id);
      this.pharmacyuserId = JSON.parse(atob(params.pharmacyuserId));
      this.name=atob(params.name)
      this.email=atob(params.email)
      this.phone=atob(params.phone);
      this.points=JSON.parse(atob(params.points))

    });
  }
    viewaccount() {
    this.router.navigate(['/superadmin/pharmacy/viewdetails'],{
      queryParams: { id: btoa(this.pharmacyuserreferenceid), pharmacyuserId: btoa(this.pharmacyuserId),name:btoa(this.name),email:btoa(this.email),phone:btoa(this.phone),points:btoa(this.points) },

    });
    }
  
    viewwallet() {
    this.router.navigate(['/superadmin/pharmacy/viewdetails/wallet'],{
      queryParams: { id: btoa(this.pharmacyuserreferenceid), pharmacyuserId: btoa(this.pharmacyuserId),name:btoa(this.name),email:btoa(this.email),phone:btoa(this.phone),points:btoa(this.points) },
    });
    }
     viewJoblist() {
    this.router.navigate(['/superadmin/pharmacy/viewdetails/joblisting'],{
      queryParams: { id: btoa(this.pharmacyuserreferenceid), pharmacyuserId: btoa(this.pharmacyuserId),name:btoa(this.name),email:btoa(this.email),phone:btoa(this.phone),points:btoa(this.points) },
    });
  }
  viewRatingReview() {
    this.router.navigate(['/superadmin/pharmacy/viewdetails/rating&review'],{
      queryParams: { id: btoa(this.pharmacyuserreferenceid), pharmacyuserId: btoa(this.pharmacyuserId),name:btoa(this.name),email:btoa(this.email),phone:btoa(this.phone),points:btoa(this.points) },
    });
  }
  isLinkActive(url): boolean {
    const queryParamsIndex = this.router.url.indexOf('?');
    const baseUrl =
      queryParamsIndex === -1
        ? this.router.url
        : this.router.url.slice(0, queryParamsIndex);
    return baseUrl === url;
  }
}
