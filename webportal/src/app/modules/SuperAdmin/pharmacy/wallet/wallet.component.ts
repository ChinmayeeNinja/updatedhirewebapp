import { Component, OnInit } from '@angular/core';
import { PharmacyService } from '../pharmacy.service';
import { first } from 'rxjs/operators';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import {ToastrService} from '../../../../toastr.service';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.css']
})
export class WalletComponent implements OnInit {
  page = 1;
  public pharmacyuserreferenceid: any;
  public pharmacyuserId: any
    public walletlist =[]
  public collectionsize: any
    public errorMessage :any;
public points : any;
public name : any;
  constructor(
    private Pharmacy: PharmacyService,
    private router: Router,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private tosterservice: ToastrService,
    private spinner : NgxSpinnerService

  ) { }

  ngOnInit(): void {
      var sub = this.route.queryParams.subscribe((params) => {
      this.pharmacyuserreferenceid = atob(params.id);
      this.pharmacyuserId = JSON.parse(atob(params.pharmacyuserId));
      this.points=JSON.parse(atob(params.points));
      this.name = atob(params.name)
    });
        this.getwalletlist(this.pharmacyuserreferenceid, this.page)

  }
  reloadDataa(){
    this.getwalletlist(this.pharmacyuserreferenceid, this.page)

  }
 getwalletlist(pharmacyuserreferenceid, page): void {
                this.spinner.show();
    this.Pharmacy
      .getlistwallet(pharmacyuserreferenceid, page)
      .pipe(first())
      .subscribe(
        (data) => {
          // console.log(data);
          if (data) {
            this.walletlist = data.data.listResult;
            this.collectionsize = data.data.pagination.totalCount

            var tt = data.data.listResult

            this.spinner.hide();

          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }
}
