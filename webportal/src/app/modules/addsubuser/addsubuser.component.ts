import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import {
  FormBuilder,
  FormControl,
    ValidatorFn,
  FormGroup,
  FormArray,
  Validators,
} from '@angular/forms';
import {Section} from '../../shared/enum/constants'
import { UserService } from '../user/user.service';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from '../../toastr.service'

@Component({
  selector: 'app-addsubuser',
  templateUrl: './addsubuser.component.html',
  styleUrls: ['./addsubuser.component.css']
})
export class AddsubuserComponent implements OnInit {
 formVar: FormGroup;
  edit: 'false';
   disableddd : 'true'
public submitted =false
public errorMessage: any;
public pharmacuuserid:number;
public sectionconst :any;
public sectionvalue =[];
public subuser :any;

constructor(
    private user: UserService,
    private router: Router,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private tosterservice:ToastrService,

    config: NgbPaginationConfig
) {
      this.sectionconst = Section;
//console.log(this.sectionconst);
  //  this.pharmacyUserDetailsLocalStorage = JSON.parse(
  //     localStorage.getItem('pharmacylist')
  //   );
  this.subuser = JSON.parse(localStorage.getItem('subuseritem'))
  // for (var i = 0; i < this.subuser.Section.length; i++){
  //   var sectionkey = this.subuser.Section[i]
  //   console.log(sectionkey)
  // }
 
   this.formVar = this.fb.group({
      // jobName: [''],
     firstname: ['',[Validators.required]],
      lastname: ['', Validators.required],
      phone: ['', [Validators.required,Validators.pattern(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/)
]],
email: ['', [Validators.required, Validators.pattern(/^([a-zA-Z]{1,})+([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/)]],
        myCategory: this.fb.array(
        this.sectionconst.map(
          (item) => {
            console.log(this.getstatus(item.key))
            // return item.Value === this.subuser?.Section[`${item.key.toLowerCase()}`]
             return this.getstatus(item.key)
          }
        ),
                 this.minSelectedCheckboxes(0)

      ),
   });
    this.formVar.get('firstname').enable();
    this.formVar.get('lastname').enable();
    this.formVar.get('email').enable();
    this.formVar.get('phone').enable();
    config.size = 'sm';
    config.boundaryLinks = true;
  }

  getstatus(a){
    if(this.subuser){
      console.log(this.subuser)
      let x = this.subuser.Section.filter(item => item.key === a && item.value ===true )   
      return x.length > 0
    }
      }

  ngOnInit(): void {
    console.log(this.formVar.value.myCategory)
    var sub = this.route.queryParams.subscribe((params) => {
      this.edit = params.edit;
      this. disableddd = params.disabled
    });
    var pharmacylist = JSON.parse(localStorage.getItem('pharmacyUserId')).data 
    // this.subuser = JSON.parse(localStorage.getItem('subuseritem'))
      this.pharmacuuserid=pharmacylist.id
    this.sectionconst = Section;
    if (this.subuser) {
          this.onload()
      // this.disabled = 'false';
    }

  }

    minSelectedCheckboxes(min = 0 ) {
    const validator: ValidatorFn = (formArray: FormArray) => {
      const totalSelected = formArray.controls
        // get a list of checkbox values (boolean)
        .map((control) => control.value)
        // total up the number of checked checkboxes
        .reduce((prev, next) => (next ? prev + next : prev), 0);

      // if the total is not greater than the minimum, return the error message
      return totalSelected > min  ? null : { required: true };
    };

    return validator;
  }
  get f() {
    return this.formVar.controls;
  } 
  onload(){
    this.formVar.patchValue({
      firstname: this.subuser?.firstname ,
      lastname: this.subuser?.lastname,
      email: this.subuser?.email,
      phone: this.subuser?.phone,
    });
    this.formVar.get('firstname').disable();
        this.formVar.get('lastname').disable();
    this.formVar.get('email').disable();
    this.formVar.get('phone').disable();


  }


  onlyNumber(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;

    }
      
    return true;
  }
  onlyText(event): boolean {
     const charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode >= 48 && charCode <= 57 )&& (charCode >= 96 || charCode <= 105)) {
      //console.log(charCode)
      return false;

    }
        return true;

  }
  cancel(){
    this.router.navigate(['pharmacy/user']);

  }
  onSubmit() {
    this.sectionvalue=this.sectionconst.map((item,i)=>{
      let obj = {key:item.key,value:this.formVar.value.myCategory[i]}
      return obj
    })
    // console.log(this.sectionvalue)

    if (this.edit) {
      this.submitted = true;
      if (this.formVar.invalid) {
        return;
      }
      var requestt = {
          section: this.sectionvalue
      };
      //console.log(request);
      this.user.Updatepharmacysubuser(requestt, this.subuser.userReferenceID).subscribe(
        (data) => {
          if (data) {
           //console.log(data)
  this.tosterservice.Success('SubUser Update  Successfully ')
                localStorage.removeItem('subuseritem')
                this.router.navigate(['pharmacy/user']);


          } else {
            this.errorMessage = 'Unable to Save Subuser';
            console.log('Unable to Save Subuser');
          }
        },
        (error) => {
          console.log('Error Message: ', error.error);
          this.errorMessage = error.error;
        }
      );
    } 

   else{
    this.submitted = true;
    if (this.formVar.invalid) {
      return;
    }
       var request = {
      role:4,
      pharmacyuserId:this.pharmacuuserid,
      firstname: this.formVar.value.firstname,    
      lastname: this.formVar.value.lastname,
      email: this.formVar.value.email,
      phone: this.formVar.value.phone,
      superadminUser: 0,

      // section: this.sectionvalue
          section: this.sectionvalue
    };
    this.user.Createsubuser(request).subscribe(
      (data) => {
        if (data) {
          this.errorMessage = '';
          console.log('review Post Saved', data);
            this.tosterservice.Success('SubUser Create  Successfully ,Check Subusers Email To Create a Password ')
            localStorage.removeItem('subuseritem')
            this.router.navigate(['pharmacy/user']);


        } else {
          this.errorMessage = 'Unable to Save review';
          console.log('Unable to Save review');
        }
      },
      (error) => {
        console.log('Error Message: ', error);
        this.errorMessage = error;
      }
    );
  }

  }
}
