import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { CodeComponent } from './code/code.component';
import { ProfileComponent } from './profile/profile.component';
import { MembershipComponent } from './membership/membership.component';
// import { PaymentPage1Component } from './payment-page1/payment-page1.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { SetPasswordComponent } from './set-password/set-password.component';
// import { SubscriptionComponent} from '../subscription/subscription.component';
import { UnauthLayoutComponent } from '../shared/layout/unauth-layout/unauth-layout.component';
import { CheckoutComponent } from '../auth/paymentView/checkout/checkout.component';
import {ForgotpasswordmailComponent} from '../../modules/auth/forgotpasswordmail/forgotpasswordmail.component'
import { CheckoutachComponent } from '../auth/paymentView/checkoutach/checkoutach.component';
import { from } from 'rxjs';
import { PaymentComponent } from '../auth/paymentView/payment/payment.component';

const routes: Routes = [
  {
    path: '',
    component: UnauthLayoutComponent,
    children: [
      {
        path: '',
        component: LoginComponent,
      },
      {
        path: 'login',
        component: LoginComponent,
      },
      {
        path: 'signup',
        component: SignupComponent,
      },
      {
        path: 'code',
        component: CodeComponent,
      },
      {
        path: 'profile',
        component: ProfileComponent,
      },
      {
        path: 'membership',
        component: MembershipComponent,
      },

      // {
      //   path: 'forgotpassword',
      //   component: userForgotPasswordComponent,
      // },
      // {
      //   path: 'changepassword',
      //   component: UserChangePasswordComponent,
      // },
      {
        path: 'payment',
        component: PaymentComponent,
        children: [
               {
        path: '',
        component: CheckoutComponent,
      },
           {
        path: 'paymentach',
        component: CheckoutachComponent,
      },
        ]
      },
      // {
      //   path: 'payment',
      //   component: CheckoutComponent,
      // },
      // {
      //   path: 'paymentach',
      //   component:CheckoutachComponent
      // },
    
      {
        path: 'forgotpassword',
        component:ForgotPasswordComponent
      },
      {
        path: 'setpassword',
        component:SetPasswordComponent
      },
      {
        path: 'forgotpasswordemail',
        component:ForgotpasswordmailComponent
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {}
