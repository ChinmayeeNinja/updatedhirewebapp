import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
// import { CodeVerificationComponent } from './code-verification/code-verification.component';
import { SignupComponent } from './signup/signup.component';
// import { SubscriptionComponent } from '../subscription/subscription.component';
import { AuthRoutingModule } from './auth-routing.module';
// import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CodeComponent } from './code/code.component';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProfileComponent } from './profile/profile.component';
import { CheckoutComponent } from '../auth/paymentView/checkout/checkout.component';
import { CheckoutachComponent } from '../auth/paymentView/checkoutach/checkoutach.component';
import { MembershipComponent } from './membership/membership.component';
import {ForgotPasswordComponent}from './forgot-password/forgot-password.component';
import { SetPasswordComponent } from  './set-password/set-password.component'
// import {ChangePasswordComponent} from './change-password/change-password.component';
import { ForgotpasswordmailComponent } from './forgotpasswordmail/forgotpasswordmail.component'
import { CarouselModule } from 'ngx-owl-carousel-o';
import { PaymentComponent } from '../auth/paymentView/payment/payment.component';

@NgModule({
  declarations: [
    LoginComponent,
    SignupComponent,
    CodeComponent,
    ProfileComponent,
    CheckoutComponent,
    MembershipComponent,
    ForgotPasswordComponent,
    SetPasswordComponent,
    // ChangePasswordComponent,
    ForgotpasswordmailComponent,
    CheckoutachComponent,
    PaymentComponent
    // CodeVerificationComponent,
    // ProfileComponent,
    // SubscriptionComponent,
  ],
  imports: [
    FormsModule,
    CommonModule,
    AuthRoutingModule,
    // HttpClientModule,
    ReactiveFormsModule,
    NgbModule,
    GooglePlaceModule,
    CarouselModule,
    // NgForm,
    
  ],
})
export class AuthModule {}
