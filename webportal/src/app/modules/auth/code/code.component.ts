import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { first } from 'rxjs/operators';
import {ToastrService} from '../../../toastr.service'

@Component({
  selector: 'app-code',
  templateUrl: './code.component.html',
  styleUrls: ['./code.component.css'],
})
export class CodeComponent implements OnInit {
  formVar: FormGroup;
  private ngUnsubscribe = new Subject();
  public loading = false;
  submitted = false;
  public returnUrl: string;
  public errorMessage: any;
  private referenceId: any;
  private signup: any;
  public pharmacyUser: any;
  public authenticationId: any;
 public verifird=true;
 public loginn=false;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private tosterservice:ToastrService

  ) {}

  ngOnInit(): void {
    this.formVar = this.fb.group({
      code: ['', [Validators.required, Validators.pattern('[0-9]{6}')]],
    });

    this.signup = localStorage.getItem('fromSignUp');
    this.referenceId = localStorage.getItem('signupUserReferenceId');
    this.authenticationId = this.referenceId;
    if (this.referenceId) {
      this.generateCode();
    }
  }
  get f() {
    return this.formVar.controls;
  }
  gosignup(){
    this.router.navigate(['/signup']);

  }
  onSubmit() {
    this.submitted = true;

    if (this.formVar.invalid) {
      return;
    }
    // console.log(this.formVar.value);
    this.verifyCode();
  }

  generateCode() {
    //console.log('entered generate sms code');
    this.authService
      .generateCode(this.referenceId)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            //console.log('generate code');
          } else {
            this.loading = false;
            this.errorMessage = '';
          }
        },
        (error) => {
          console.log('Error Message: ', error.error.text);
          this.errorMessage = error.text;
        }
      );
    this.loading = true;
  }
  resendcode(){
    this.generateCode();

  }
  verifyCode() {
    this.authService
      .VerifyCode(this.referenceId, this.f.code.value)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            this.createPharmacyUser();
            //this.createPharmacyUser();
            this.tosterservice.Success('Code Verified Successfully');
            this.verifird =false;
            this.loginn =true
          } else {
            // console.log(
            //   'code could not be verified ' + this.formVar.value.code
            // );
          }
        },
        (error) => {
          this.errorMessage = error;
          console.log(error)
          this.tosterservice.Error('The Added Code Does Not Match.Please Enter Valid Code Which Is Sent Through Email');

        }
      );
  }
  login(){
   this.router.navigate(['/login']);

  }
  createPharmacyUser() {
    this.loading = true;
    this.pharmacyUser = {
      authenticationId: this.authenticationId,
      status: 'pending',
    };
    this.authService
      .CreatePharmacyUser(this.pharmacyUser)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            localStorage.setItem('pharmacyUserId', JSON.stringify(data)); //data.UserId ;
            // this.router.navigate(['/login']);
          } else {
            console.log('Unable to create pharmacy user');
          }
        },
        (error) => {
          console.log('#158Error Message: ', error);
          this.errorMessage = error.text;
        }
      );
  }
}
