import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { AuthService } from '../auth.service';
import { first } from 'rxjs/operators';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  public errorMessage: any;
  public token: any
    formVar: FormGroup;
  submitted = false;
  public setpassword :any
    private ngUnsubscribe = new Subject();
  constructor(
    private authService: AuthService,
  private route: ActivatedRoute,
    private router: Router,    private fb: FormBuilder,
    config: NgbPaginationConfig
  ) {
    config.size = 'sm';
    config.boundaryLinks = true;
  }
  ngOnInit(): void {
     var sub = this.route.queryParams.subscribe((params) => {
      this.token = params.token;
     });
      this.formVar = this.fb.group({
      passwordd: ['', [Validators.required]],
      confirmpassword: ['', Validators.required],
    },
    {
      validator: this.MustMatch('passwordd', 'confirmpassword')
  });
    this.gettokenvalidate(this.token)
  }

   get f() {
    return this.formVar.controls;
   }

   MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];

        if (matchingControl.errors && !matchingControl.errors.mustMatch) {
            // return if another validator has already found an error on the matchingControl
            return;
        }

        // set error on matchingControl if validation fails
        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ mustMatch: true });
        } else {
            matchingControl.setErrors(null);
        }
    }
}
  
gettokenvalidate(token): void {
    this.authService
      .Resetpassword(token)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {


          }
        },
        (error) => {
          console.log('#66Error Message: ', error);
          this.errorMessage = error.text;
        }
      );
}
  
  
  onSubmit() {
      this.submitted = true;

    if (this.formVar.invalid) {
      return;
    }
      var request = {
      password :this.formVar.value.passwordd
    }
    
    // console.log(request)
    this.authService
      .Resetpasswordform(this.token, request)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        (data) => {
          if (data) {
                  this.router.navigate(['/']);

            this.errorMessage = '';
          } else {
          
          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }
  
 
}
