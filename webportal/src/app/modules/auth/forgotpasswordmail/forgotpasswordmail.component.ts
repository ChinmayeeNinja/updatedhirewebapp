import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { status } from '../../../shared/enum/constants';
import { first } from 'rxjs/operators';
import {ToastrService} from '../../../toastr.service'

@Component({
  selector: 'app-forgotpasswordmail',
  templateUrl: './forgotpasswordmail.component.html',
  styleUrls: ['./forgotpasswordmail.component.css']
})
export class ForgotpasswordmailComponent implements OnInit {
    formVar: FormGroup;
  public errorMessage: any
  public submitted = false
    private ngUnsubscribe = new Subject();

  constructor(
    private authService: AuthService,
  private route: ActivatedRoute,
    private router: Router,    private fb: FormBuilder,
    private tosterservice:ToastrService

  ) {
  }
  ngOnInit(): void {
       this.formVar = this.fb.group({
      email: ['', [Validators.required, Validators.pattern(/^([a-zA-Z]{1,})+([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/)]],

    });
  }
  get f() {
    return this.formVar.controls;
  }
  gologin() {
        this.router.navigate(['/']);

  }
   onSubmit() {
    this.submitted = true;

    if (this.formVar.invalid) {
      return;
    }

     this.authService
       .forgotpassword(this.formVar.value)
       .pipe(takeUntil(this.ngUnsubscribe))
       .subscribe(
         (data) => {
           this.tosterservice.Success('We send the request on your varified email Address')

         },
        (error) => {
          this.errorMessage = error;
          //console.log(error);
        }
      );
  }
}
