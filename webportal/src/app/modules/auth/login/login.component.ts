import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { status } from '../../../shared/enum/constants';
import { first } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  formVar: FormGroup;
  submitted = false;
  public authenticationId: any;
  public pharmacyUser: any;
  public loading = false;
  public errorMessage: any;
  public errordisplay = [];
  private ngUnsubscribe = new Subject();
  public pharmacyuserdetails:any;
  public card :any
public state :any
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,

  ) {}

  ngOnInit(): void {
    this.formVar = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }

  get f() {
    return this.formVar.controls;
  }

  forgotpass() {
      this.router.navigate(['/forgotpasswordemail']);
  }
  signup(){
    this.router.navigate(['/signup']);
    localStorage.removeItem('pharmacyUser');
    localStorage.removeItem('currentUser');
    localStorage.removeItem('signupUserReferenceId');
    localStorage.removeItem('accessToken');
    localStorage.removeItem('pharmacyUserId');
    localStorage.removeItem('pharmacylist');

  }
  onSubmit() {
    this.submitted = true;

    if (this.formVar.invalid) {
      return;
    }

    this.authService
      .login(this.formVar.value)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        (data) => {
          if (data.data) {
            this.authenticationId = data?.data?.referenceId;
            //check if user has filled pharmacy details
            // this.Getpharmacysubuserbyauthid(this.authenticationId)

            if (data.data.role === 1) {
               this.router.navigate(['/superadmin/dashboard']);
               localStorage.removeItem('subusersection')
               localStorage.removeItem('subuserpharmacyuserid')
            }
            else if (data.data.role === 4) {
              if (data.data.isCodeVerified === null) {
                this.router.navigate(['/code']);
              } 
              else{
                this.authService
                .GetpharmacysubuserbyAuthId(this.authenticationId)
                .pipe(first())
                .subscribe(
                  (dataa) => {
                    if (dataa) {        
                      var dataaa=dataa.data
                      localStorage.setItem('subusersection', JSON.stringify(dataaa))
                      localStorage.removeItem('subuserpharmacyuserid')
                      var section = dataaa.section
                      var section = dataaa[0].section[0].key

                      if(section==='Contracts'){
                        this.router.navigate(['/pharmacy/contracts']);
                      }
                      else if(section==='Hire'){
                        this.router.navigate(['/pharmacy/hire']);

                      }
                      // else if(section==='Message'){
                      //   this.router.navigate(['/pharmacy/'+section]);
                      // }
                      else if(section==='Jobs'){
                        this.router.navigate(['/pharmacy/jobpost']);

                      } else if(section==='Wallet'){
                        this.router.navigate(['/pharmacy/wallet']);

                      }

                    }
                  },
                  (error) => {
                    console.log('#66Error Message: ', error);
                    this.errorMessage = error.text;
                  }
                );
              }
            }
            else if (data.data.role === 5) {
              if (data.data.isCodeVerified === null) {
                this.router.navigate(['/code']);
              } 
              else{
                this.authService
                .GetpharmacysubuserbyAuthId(this.authenticationId)
                .pipe(first())
                .subscribe(
                  (dataa) => {
                    if (dataa) {        
                      var dataaa=dataa.data
                      localStorage.setItem('subusersection', JSON.stringify(dataaa))
                      // localStorage.removeItem('subuserpharmacyuserid')
                      var section = dataaa.section
                      var section = dataaa[0].section[0].key

                      if(section==='Pharmay'){
                        this.router.navigate(['superadmin/pharmacy']);
                      }
                      else if(section==='Pharmacist'){
                        this.router.navigate(['superadmin/pharmacist']);

                      }
                    
                      else if(section==='Voucher'){
                        this.router.navigate(['superadmin/voucher']);

                      } else if(section==='Credential Request'){
                        this.router.navigate(['superadmin/credentialrequest']);

                      }
                      else if(section==='User'){
                        this.router.navigate(['superadmin/user']);

                      }
                      else if(section==='Membership'){
                        this.router.navigate(['superadmin/membership']);

                      }

                    }
                  },
                  (error) => {
                    console.log('#66Error Message: ', error);
                    this.errorMessage = error.text;
                  }
                );
              }
             
            }else {
              localStorage.removeItem('subusersection')

            if (data.data.isCodeVerified === null) {
              this.router.navigate(['/code']);
            } else {
              this.checkPharmacyUserExists();
            }
            }
              
          }
          else {
            console.log('Unable to Login');
          }
        },
        (error) => {
          console.log('Error Message: ', error);
          this.errorMessage = error;

          // Swal.fire({
          //   icon: 'error',
          //   title: 'Oops...',
          //   text: error.error,
          // });
        }
      );
  }

  checkPharmacyUserExists(): void {
    var pdata;
    this.loading = true;
    this.pharmacyUser = {
      authenticationId: this.authenticationId,
      status: status.ACTIVE,
    };
    this.authService
      .GetPharmacyUserByAuthId()
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            this.getPharmacyListByAuthId();
            localStorage.setItem('pharmacyUserId', JSON.stringify(data));
            this.pharmacyuserdetails=data

          } else {
            this.authService
              .CreatePharmacyUser(this.pharmacyUser)
              .pipe(first())
              .subscribe(
                (data) => {
                  if (data) {
                    this.errorMessage = '';
                    localStorage.setItem(
                      'pharmacyUserId',
                      JSON.stringify(data)
                    ); //data.UserId ;
                  } else {
                    console.log('Unable to create pharmacy user');
                  }
                },
                (error) => {
                  console.log('#115Error Message: ', error);
                  this.errorMessage = error.text;
                }
              );
            console.log('Unable to create pharmacy user');
            this.router.navigateByUrl('profile');
          }
          return data;
        },
        (error) => {
          console.log('#66Error Message: ', error);
          this.errorMessage = error.text;
        }
      );
  }

  getPharmacyListByAuthId(): void {
    this.authService
      .GetPharmacyListByAuthId()
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            localStorage.setItem('pharmacylist', JSON.stringify(data));
            let pharmacylistt = data;
            for (var i = 0; i < pharmacylistt.length; i++) {
              this.state = pharmacylistt[i].state;
            }
            this.Getmembershipbystate(this.state)
              if (data?.data?.length === 0) {
                console.log('to profile');
                this.router.navigateByUrl('profile');
              } else {
                console.log('to subscription');
                this.router.navigateByUrl('pharmacy/dashboard');
              }
          }
       
        },
        (error) => {
          console.log('#66Error Message: ', error);
          this.errorMessage = error.text;
        }
      );
  }

  Getmembershipbystate(state){
    this.authService
    .Getmembershipbystate(state)
    .pipe(first())
    .subscribe(
      (data) => {
        if (data) {
          this.errorMessage = '';
          this.card = data.data;
          console.log(this.card)
          console.log(this.pharmacyuserdetails?.data)

          for(var i=0;i<this.card.length;i++){
              if(this.card[i].membershipId === this.pharmacyuserdetails?.data?.membershipId){
                console.log(this.card[i].membershipId === this.pharmacyuserdetails.data.membershipId)
                localStorage.setItem("membershipname",this.card[i].name)
              }
          }  
        }
      },
      (error) => {
        console.log('#66Error Message: ', error);
        this.errorMessage = error.text;
      }
    );
  }

  // Getpharmacysubuserbyauthid(authid): void {
  //   this.authService
  //     .GetpharmacysubuserbyAuthId(authid)
  //     .pipe(first())
  //     .subscribe(
  //       (data) => {
  //         console.log(data.data);
  //         if (data) {
  //           var dataa=data.data
  //           localStorage.setItem('subusersection',JSON.stringify(dataa))
  //         }
  //       },
  //       (error) => {
  //         console.log('#66Error Message: ', error);
  //         this.errorMessage = error.text;
  //       }
  //     );
  // }

}
