import {
  Component,
  OnInit,
  AfterViewInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../auth.service';
import { first } from 'rxjs/operators';
import { NgForm } from '@angular/forms';
import { AngularStripeService } from '@fireflysemantics/angular-stripe-service';
// import {Paymentmethod} from '../shared/enum/constants'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {PaymentService} from '../../../settings/payment/payment.service';

@Component({
  selector: 'app-checkoutach',
  templateUrl: './checkoutach.component.html',
  styleUrls: ['./checkoutach.component.css']
})
export class CheckoutachComponent implements OnInit {
  @ViewChild('cardInfo', { static: false }) cardInfo: ElementRef;

  stripe;
  loading = false;
  confirmation;
      formVar: FormGroup;    
  model: any = {};

  public errorMessage: any;
  public stiprkey: string;
  card: any;
  public request: any;
  public tokenn: any;
  public selectpaymentmethod: any
  // cardHandler = this.onChange.bind(this);
  error: string;
  constructor(
    private cd: ChangeDetectorRef,
    private stripeService: AngularStripeService,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
        private fb: FormBuilder,
        private paymentService: PaymentService


  ) {}
  ngOnInit(): void {
    this.formVar = this.fb.group({
      paymentmethod: [''],
    
    });
    this.GetStripeKeyy();
    this.getCustomer();
  }
  logout() {
    localStorage.removeItem('pharmacyUser');
    localStorage.removeItem('currentUser');
    localStorage.removeItem('signupUserReferenceId');
    localStorage.removeItem('accessToken');
    localStorage.removeItem('pharmacyUserId');
    localStorage.removeItem('pharmacylist');

    this.router.navigate(['/login']);
  }
   selectedQuantity = "Connect Bank";
onOptionsSelected(value: string) {
  if(value === 'Credit/Debit'){
            this.router.navigate(['/payment']);
  }
  else{
            this.router.navigate(['/paymentach']);
  }
  }
  GetStripeKeyy() {
    this.authService
      .GetStripeKey()
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            this.stiprkey = data.public_key;
                        this.init();

          }
        },
        (error) => {
          console.log('#66Error Message: ', error);
          this.errorMessage = error.text;
        }
      );
  }

  init() {
    this.stripeService.setPublishableKey(this.stiprkey).then((stripe) => {
      this.stripe = stripe;
    });
  }

  async onSubmit(form: NgForm) {
    this.request = {
      country: 'US',
      currency: 'USD',
      account_number: this.model.account_number,
      account_holder_name: this.model.account_holder_name,
      account_holder_type: this.model.account_holder_type,
      routing_number: this.model.routing_number,
      paymentType:'ACH'
    };
    const { token, error } = await this.stripe.createToken(
      'bank_account',
      this.request
    );

    if (error) {
      console.log('Error:', error);
    } else {
      console.log('Success!', token);

      this.createcustomerinfo(token.id);
    }
  }

  createcustomerinfo(token) {
    this.authService
      .Createcustomer(this.request)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            var dataa = JSON.parse(data)
            this.getCustomer();

            this.request = {
              stripecustomerId : dataa?.data?.stripecustomerId,
              country: 'US',
              currency: 'USD',
              account_number: this.model.account_number,
              account_holder_name: this.model.account_holder_name,
              account_holder_type: this.model.account_holder_type,
              routing_number: this.model.routing_number,
              paymentType:'ACH'
            };
            this.paymentService.PaymentMethodACH(this.request)
            .pipe(first())
            .subscribe(
        
                (response) => {
        
                  let data = JSON.parse(response)

                  if(data.error == false)
                  {
                     this.errorMessage = "Payment Method added successfully."
                     this.router.navigate(['pharmacy/dashboard']);

                  }
                  else
                  {  
                   this.errorMessage = "Error in adding Bank Info."
                  }
        
        
                },
                (error) => {
        
                  this.errorMessage = "Error in adding Bank Info."
        
                }
        
            )
          } else {
            console.log('Unable to create customer');
          }
        },
        (error) => {
          console.log('#66Error Message: ', error);
          // this.errorMessage = error.text;
        }
      );
  }


  getCustomer(){
    //console.log(" getcustomer")
    this.paymentService
    .GetCustomer()
    .pipe(first())
    .subscribe(
          (resp) =>{
              
              let paymentCustomerInfo = JSON.parse(resp)
              if(paymentCustomerInfo.data == null)
               {
                  //console.log(atob(localStorage.getItem("email")))
                  // create customer
               }
               else
               {
                  localStorage.setItem("stripecustomerId",btoa(paymentCustomerInfo.data.stripecustomerId))
               }

          },
          (error) => {
              console.log('Error getCustomer: ', error);
          });
  }

  // attachPaymentMethod(token){
      
  //   let stripecustomerid = atob(localStorage.getItem("stripecustomerId"))
  //   this.paymentService
  //     .PaymentMethodCC({
  //       paymentMethod: token.paymentMethod.id,
  //       stripecustomerid: stripecustomerid
  //     })
  //     .pipe(first())
  //     .subscribe(

  //       (response) => {
  //           //console.log(response)
  //            let data = JSON.parse(response)

  //            if(data.error == false)
  //            {
  //               this.errorMessage = "Payment Method added successfully."
  //               this.router.navigate(['pharmacy/dashboard']);

  //            }
  //            else
  //            {  
  //             this.errorMessage = "Error in adding Payment Method."
  //            }


  //       },
  //       (error) => {
  //           //console.log(error) 
  //           this.errorMessage = "Error in adding Payment Method."  
  //       }

  //     );
  
  // }

}
