import {
  Component,
  OnInit,
  AfterViewInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../auth.service';
import { first } from 'rxjs/operators';
import { NgForm } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AngularStripeService } from '@fireflysemantics/angular-stripe-service';
import {Paymentmethod} from '../../../../shared/enum/constants'
@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
  formVar: FormGroup;

  public Paymentmethod = Paymentmethod
      public title ='Enter Your Card Details'

  constructor(
    private cd: ChangeDetectorRef,
    private stripeService: AngularStripeService,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,
  ) {
    localStorage.setItem('Paymentmethodvalue',this.Paymentmethod[0].Value)
    this.formVar = this.fb.group({
      Paymentmethod: ['']})
    this.formVar.controls.Paymentmethod.patchValue(
      Paymentmethod[
        Paymentmethod.findIndex(
                  (x) =>x.Value === localStorage.getItem('Paymentmethodvalue')
                )
                ].Value
              );
   }

  ngOnInit(): void {

  }
    logout() {
    localStorage.removeItem('pharmacyUser');
    localStorage.removeItem('currentUser');
    localStorage.removeItem('signupUserReferenceId');
    localStorage.removeItem('accessToken');
    localStorage.removeItem('pharmacyUserId');
    localStorage.removeItem('pharmacylist');

    this.router.navigate(['/login']);
  }
  gotodashboard(){
    this.router.navigate(['/pharmacy/dashboard']);

  }
  opencc(value) {
    if (value == 'Credit/Debit') {
      this.router.navigate(['/payment']);
      this.title = 'Enter Your Card Details'

    }
    if (value =='Connect Bank') {
            this.router.navigate(['/payment/paymentach']);
      this.title = 'Enter Your Account Details'

    }

  }  
}
