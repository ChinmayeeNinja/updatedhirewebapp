import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthService } from '../auth.service';
import * as _ from 'lodash';
import { ToastrService } from '../../../toastr.service';
import { SettingsService } from '../../settings/settings.service';
import { StatesService } from '../../../shared/states-cities/states.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  public states: any;
  public cities: any;
  // public show = false
  public submitted = false;
  public errorMessage: any;
  public stateCities: any;
  public loading = true;
  public pharmacyUserLocalStorage: any;
  public authenticationId: any;
  public pharmacyidd: any;
  public pharmaciesid: any;
  public pharmacylist = [];

  options = {
    componentRestrictions: {
      country: ['US'],
    },
  };
  componentForm = {
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'long_name',
    country: 'long_name',
    postal_code: 'short_name',
  };
  formVar: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private stateService: StatesService,
    private router: Router,
    private tosterservice: ToastrService,
    private setting: SettingsService
  ) {
    this.getUSStates();
  }

  ngOnInit(): void {
    this.authenticationId = localStorage.getItem('signupUserReferenceId');
    this.pharmacyUserLocalStorage = localStorage.getItem('pharmacyUserId');
    var dd = this.pharmacyUserLocalStorage;
    this.pharmacyidd = JSON.parse(dd);
    console.log(this.pharmacyidd.id);

    this.formVar = this.fb.group({
      pharmacyName: ['', Validators.required],
      phoneNumber: [
        '',
        [
          Validators.required,
          Validators.pattern(
            /^\(?[+. ]?([0-1])?([ ])?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/
          ),
        ],
      ],
      pharmacyAddress: ['', Validators.required],
      state: ['', Validators.required],
      city: ['', Validators.required],
      country: ['', Validators.required],
      postalCode: ['', Validators.required],
      licenseNumber: ['', Validators.required],
      pharmacyUserId: this.pharmacyidd.data.id,
    });
    this.getPharmacyListByAuthId();
  }
  get f() {
    return this.formVar.controls;
  }

  onlyNumber(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      console.log(charCode);
      return false;
    }

    return true;
  }
  onlyText(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (
      charCode >= 48 &&
      charCode <= 57 &&
      (charCode >= 96 || charCode <= 105)
    ) {
      console.log(charCode);
      return false;
    }
    return true;
  }
  getUSStates() {
    this.stateService.getUSStates().subscribe(
      (data) => {
        console.log(data);
        this.stateCities = JSON.parse(JSON.stringify(data));
        this.states = _.uniqBy(this.stateCities, 'state');
        this.states = _.orderBy(this.states, ['state'], ['asc']);
        console.log('States', this.states);
      },
      (error) => {
        this.errorMessage = error.message;
      }
    );
  }

  getCityByState(state) {
    console.log(state);
    this.cities = _.filter(this.stateCities, { state: state });
    console.log('City', this.cities);
  }
  addpharmacy() {
    if (
      this.formVar.value.city !== '' &&
      this.formVar.value.country !== '' &&
      this.formVar.value.licenseNumber !== '' &&
      this.formVar.value.pharmacyAddress !== '' &&
      this.formVar.value.pharmacyName !== '' &&
      this.formVar.value.phoneNumber !== '' &&
      this.formVar.value.state !== '' &&
      this.formVar.value.postalCode !== ''
    ) {
      window.location.reload();
    }
  }
  logout() {
    localStorage.removeItem('pharmacyUser');
    localStorage.removeItem('currentUser');
    localStorage.removeItem('signupUserReferenceId');
    localStorage.removeItem('accessToken');
    localStorage.removeItem('pharmacyUserId');
    localStorage.removeItem('pharmacylist');

    this.router.navigate(['/login']);
  }

  gotologin() {
    this.router.navigate(['/login']);
  }
  onSubmit() {
    console.log('jghj');
    this.submitted = true;

    if (this.formVar.invalid) {
      return;
    }
    if (this.submitted) {
      if (
        this.pharmacylist.filter(
          (item, i) =>
            item.phoneNumber === this.formVar.value.phoneNumber ||
            item.licenseNumber === this.formVar.value.licenseNumber
        ).length > 0
      ) {
        this.tosterservice.Error(
          'Pharmacy Alredy Exist.Please Enter Different Pharmacy Details'
        );
      } else {
        this.authService
          .CreatePharmacy({
            ...this.formVar.value,
          })
          .pipe(first())
          .subscribe(
            (data) => {
              if (data) {
                this.errorMessage = '';
                this.getPharmacyListbyrefid(JSON.parse(data).data.referenceId);
                this.tosterservice.Success(
                  'Pharmacy Details Saved Successfully'
                );
                // this.router.navigate(['/membership']);

                this.getPharmacyListByAuthId();
                // this.router.navigate(['/subscription']);
              } else {
                this.loading = false;
                console.log('Unable to create pharmacy');
                //this.router.navigate(['/code']);
              }
            },
            (error) => {
              console.log('#66Error Message: ', error);
              this.errorMessage = error.text;
            }
          );
      }
    }
  }

  getPharmacyListbyrefid(pharmacyreferenceid): void {
    this.setting
      .getharmacydetailbypharmacyid(pharmacyreferenceid)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            this.pharmaciesid = data?.data?.referenceId;
          }
        },
        (error) => {
          console.log('#66Error Message: ', error);
          this.errorMessage = error.text;
        }
      );
  }

  continue() {
    this.submitted = true;

    if (this.formVar.invalid) {
      return;
    }

    if (this.submitted) {
      if (this.pharmaciesid) {
        this.setting
          .updatepharmacy(this.formVar.value, this.pharmaciesid)
          .pipe(first())
          .subscribe(
            (data) => {
              if (data) {
                this.errorMessage = '';
                // this.tosterservice.Success('Pharmacy Details Saved Successfully')
                // this.router.navigate(['/membership']);
                if (this.pharmacyidd.data.membershipId === null) {
                  this.router.navigate(['/membership']);
                } else {
                  this.router.navigateByUrl('pharmacy/dashboard');
                }
                this.getPharmacyListByAuthId();
                // this.router.navigate(['/subscription']);
              } else {
                this.loading = false;
                // console.log('Unable to create pharmacy');
                //this.router.navigate(['/code']);
              }
            },
            (error) => {
              console.log('#66Error Message: ', error);
              this.errorMessage = error.text;
            }
          );
      } else {
        if (
          this.pharmacylist.filter(
            (item, i) =>
              item.phoneNumber === this.formVar.value.phoneNumber ||
              item.licenseNumber === this.formVar.value.licenseNumber
          ).length > 0
        ) {
          this.tosterservice.Error(
            'Pharmacy Alredy Exist.Please Enter Different Pharmacy Details'
          );
        } else {
          this.authService
            .CreatePharmacy({
              ...this.formVar.value,
            })
            .pipe(first())
            .subscribe(
              (data) => {
                if (data) {
                  this.errorMessage = '';
                  this.tosterservice.Success(
                    'Pharmacy Details Saved Successfully'
                  );
                  // this.router.navigate(['/membership']);
                  if (this.pharmacyidd.data.membershipId === null) {
                    this.router.navigate(['/membership']);
                  } else {
                    this.router.navigateByUrl('pharmacy/dashboard');
                  }
                  this.getPharmacyListByAuthId();
                  // this.router.navigate(['/subscription']);
                } else {
                  this.loading = false;
                  console.log('Unable to create pharmacy');
                  //this.router.navigate(['/code']);
                }
              },
              (error) => {
                console.log('#66Error Message: ', error);
                this.errorMessage = error.text;
              }
            );
        }
      }
    }
  }

  getPharmacyListByAuthId(): void {
    this.authService
      .GetPharmacyListByAuthId()
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            localStorage.setItem('pharmacylist', JSON.stringify(data));
            this.pharmacylist = data.data;
          }
          if (data.length == 0) {
            console.log('to profile');
            this.router.navigateByUrl('profile');
          } else {
            console.log('to subscription');
            // this.router.navigateByUrl('dashboard');
          }
        },
        (error) => {
          console.log('#66Error Message: ', error);
          this.errorMessage = error.text;
        }
      );
  }

  public handleAddressChange(address: any) {
    let city = '';
    console.log(address);
    this.f.phoneNumber.setValue(address.international_phone_number);

    this.f.pharmacyName.setValue(address.name);
    this.f.pharmacyAddress.setValue(address.formatted_address);

    for (const component of address.address_components) {
      const addressType = component.types[0];

      if (this.componentForm[addressType]) {
        const val = component[this.componentForm[addressType]];
        if (addressType === 'postal_code') {
          this.f.postalCode.setValue(val);
        }
        if (addressType === 'country') {
          this.f.country.setValue(val);
        }
        if (addressType === 'administrative_area_level_1') {
          this.f.state.setValue(val);
          this.getCityByState(val);
        }
        if (addressType === 'locality') {
          city = val;
        }
      }
    }

    setTimeout(() => {
      this.f.city.setValue(city);
    }, 200);
  }
}
