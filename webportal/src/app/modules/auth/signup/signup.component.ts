import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { role } from 'src/app/shared/enum/constants';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent implements OnInit {
  formVar: FormGroup;
  submitted = false;
  public errordisplay = [];
public errorMessage : any
public isAgreePrivacy :any
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.formVar = this.fb.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      phone: ['', [Validators.required,Validators.pattern(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/)
]],
      email: ['', [Validators.required, Validators.pattern(/^([a-zA-Z]{1,})+([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/)]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      isAgreePrivacy: ['', Validators.required],
      role: role.PHARMACY,
    });
  }
  onlyNumber(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }  
    return true;
  }
  onlyText(event): boolean {
     const charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode >= 48 && charCode <= 57 )&& (charCode >= 96 || charCode <= 105)) {
      return false;

    }
        return true;

  }
  get f() {
    return this.formVar.controls;
  }
  gologin(){
    this.router.navigate(['/login']);

  }
  
  toggleEditable(e){
    if ( e.target.checked ) {
      this.isAgreePrivacy=this.formVar.value.isAgreePrivacy
    }
    else{
      this.formVar.patchValue({
        isAgreePrivacy: '',
      });
    }
   
}
  onSubmit() {
    this.submitted = true;

    if (this.formVar.invalid) {
      return;
    }
let request={
  firstname:this.formVar.value.firstname,
  lastname:this.formVar.value.lastname,
  phone:this.formVar.value.phone,
  email:this.formVar.value.email,
  password:this.formVar.value.password,
  isAgreePrivacy:this.isAgreePrivacy,
  role: role.PHARMACY,

}
    this.authService
      .signup(request)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            if(data.error===true){
              this.errorMessage = data.message;

            }else{
              localStorage.setItem('fromSignUp', 'yes');
            this.router.navigate(['/code']);
            }
            

          } else {
            console.log('Unable to Login');
            //this.router.navigate(['/code']);
          }
        },
        (error) => {
          console.log('#66Error Message: ', error);
          this.errorMessage = error;
          console.log(error)
          // Swal.fire({
          //   icon: 'error',
          //   title: 'Oops...',
          //   text: error.error.text,
          // });
        }
      );
  }
}
