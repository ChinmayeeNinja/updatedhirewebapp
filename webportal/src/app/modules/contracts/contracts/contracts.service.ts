import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { AppSettings } from '../../../app.settings';
import { Observable, throwError, from, Subject } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Headers, RequestOptions, Request, RequestMethod } from '@angular/http';
@Injectable({
  providedIn: 'root',
})
export class ContractsService {
  constructor(private http: HttpClient) {}

  public getlistcontracts(authonticationid, page): Observable<any> {
    return this.http
      .get(
        AppSettings.apiurls.LISTCONTRACTS +
          authonticationid +
          `?limit=9&offset=${(page - 1) * 9}`
      )
      .pipe(
        map((res) => {
          return res;
        }),
        catchError(this.errorHandler)
      );
  }
  writereview(data) {
    return this.http
      .post<any>(AppSettings.apiurls.WRITEREVIEW, { ...data })
      .pipe(
        map((review) => {
          return JSON.stringify(review);
        }),
        catchError(this.errorHandler)
      );
  }


  errorHandler(error: any) {
    console.log('Error--->', error);
    return throwError(error);
  }
}
