import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from '../../modules/shared/layout/authorized/sidebar/sidebar.component';
import { HeaderComponent } from '../../modules/shared/layout/authorized/header/header.component';
import { from } from 'rxjs';
@NgModule({
  declarations: [SidebarComponent, HeaderComponent],
  imports: [
    // CommonModule
  ],
})
export class AuthModule {}
