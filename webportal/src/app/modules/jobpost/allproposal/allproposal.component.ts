import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs';
import { first } from 'rxjs/operators';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import { Hirecharge } from '../../../shared/config';
import { AngularStripeService } from '@fireflysemantics/angular-stripe-service';
import { AuthService } from '../../auth/auth.service';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { JobpostService } from '../jobpost.service';
import { ToastrService } from '../../../toastr.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { PaymentService } from '../../settings/payment/payment.service';
import Swal from 'sweetalert2';
import * as moment from 'moment';

@Component({
  selector: 'app-allproposal',
  templateUrl: './allproposal.component.html',
  styleUrls: ['./allproposal.component.css'],
})
export class AllproposalComponent implements OnInit {
  stripe;
  formVar: FormGroup;
  submitted = false;
  costhire: number;
  public finalizedDateTime: any;
  public description: any;
  public title: any;
  public modalShow = false;
  public proposalid: any;
  public jobreferenceid: any;
  public jobid: any;
  public errorMessage: any;
  public proposacollectionsize: any;
  page = 1;
  public proposallist = [];
  public pharmacyuser: any;
  public show = false;
  public stripeKey: string;
  public paymentModesList = [];
  public paymentmethodid: any;
  public customerid: any;
  walletamount: number;
  public contractreferanceid: any;
  public amount: any;
  public showless = false;
  public viewjob = true;
  public clickedid = '';
  public softwareskiiill = [];
  public startdate :any;
  public enddate :any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private jobpostService: JobpostService,
    private tosterservice: ToastrService,
    private spinner: NgxSpinnerService,
    private stripeService: AngularStripeService,
    private authService: AuthService,
    private paymentService: PaymentService,

    config: NgbPaginationConfig
  ) {
    config.size = 'sm';
    config.boundaryLinks = true;
  }
  ngOnInit(): void {
    var sub = this.route.queryParams.subscribe((params) => {
      this.jobreferenceid = atob(params?.id);
      this.jobid = JSON.parse(atob(params?.jobId));
    });
    this.allproposal(this.page);
    this.formVar = this.fb.group({
      finalizedAmount: ['', Validators.required],
    });
    this.pharmacyuser = JSON.parse(localStorage.getItem('pharmacyUserId'));
    if (
      this.pharmacyuser.data.membershipId === null ||
      localStorage.getItem('membershipname') === 'Pay as you go'
    ) {
      this.costhire = Hirecharge.Value;
    } else {
      this.costhire = 0;
    }
    this.paymentModes();
  }
  get f() {
    return this.formVar.controls;
  }
  gettime(time){
    let date =moment(new Date(time)).utc().format("hh:mm A");
      return date
  }
  modalHideShow(proposalid, des, jobname, startDateTime,endDateTime) {
    // alert(startDateTime);
    this.formVar.reset();
    this.submitted = false;
    this.modalShow = !this.modalShow;
    this.proposalid = proposalid;
    this.title = jobname;
    this.description = des;
    this.finalizedDateTime = startDateTime;
    this.allproposal(this.page);
  }
  modalHideShoww() {
    this.modalShow = false;
  }
  reloadDataa() {
    this.allproposal(this.page);
  }

  viewalljobpostt(pharmacistreferenceid) {
    this.clickedid = pharmacistreferenceid;
    // alert(this.clickedid)
    this.viewjob = false;
    this.showless = true;
    this.spinner.show();
    var request = {
      jobpostId: this.jobreferenceid,
      filter: 'all',
    };
    this.jobpostService
      .Listproposal(request, this.page)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.spinner.hide();

            var dataaa = JSON.parse(data);
            for (var i = 0; i < dataaa?.data?.listResult.length; i++) {
              this.softwareskiiill =
                dataaa?.data?.listResult[i]?.softwareSkills;
            }
          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }
  viewlessjobpost(pharmacistreferenceid) {
    this.clickedid = '';

    this.allproposal(this.page);
  }

  allproposal(page) {
    this.spinner.show();
    var request = {
      jobpostId: this.jobreferenceid,
      filter: 'all',
    };
    this.jobpostService.Listproposal(request, page).subscribe(
      (data) => {
        if (data) {
          this.errorMessage = '';
          var proposal = JSON.parse(data);
          this.proposacollectionsize = proposal?.data?.pagination?.totalCount;
          // this.jobListing.push(dataaa[0]);
          this.proposallist = proposal.data.listResult;
          // var proposalvalue = proposal.data.listResult;
        
          this.spinner.hide();
        } else {
          this.errorMessage = 'list proposal';
        }
      },
      (error) => {
        this.errorMessage = error.error;
      }
    );
  }

  onarchived(proposalReferenceId) {
    var requestarchived = {
      shortlisted: false,
      archived: true,
    };
    this.jobpostService
      .Updateproposal(requestarchived, proposalReferenceId)
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            this.tosterservice.Success('Your Proposal Archived Successfully');

            this.allproposal(this.page);
          } else {
            this.errorMessage = 'Unable to Save Screening Questions';
          }
        },
        (error) => {
          this.errorMessage = error.error;
        }
      );
  }

  onshortlisted(proposalReferenceId) {
    // alert("sdf")
    var requestarchived = {
      shortlisted: true,
      archived: false,
    };
    this.jobpostService
      .Updateproposal(requestarchived, proposalReferenceId)
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            this.tosterservice.Success(
              'Your Proposal Shortlisted Successfully'
            );

            this.allproposal(this.page);
          } else {
            this.errorMessage = 'Unable to Save Screening Questions';
          }
        },
        (error) => {
          this.errorMessage = error.error;
        }
      );
  }

  viewproposal(proposalreferenceid, proposalIddd) {
    localStorage.setItem('jobreferenceid', this.jobreferenceid);
    localStorage.setItem('jobid', this.jobid);

    var proposalidvalue = proposalIddd;
    var proposalid = proposalreferenceid;
    this.router.navigate(['pharmacy/jobpost/viewproposal'], {
      queryParams: {
        proposalreferanseid: btoa(proposalid),
        proposalId: btoa(proposalidvalue),
      },
    });
  }

  GetStripeKey() {
    this.authService
      .GetStripeKey()
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            this.stripeKey = data.public_key;
            this.initStripe();
          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }

  initStripe() {
    this.stripeService.setPublishableKey(this.stripeKey).then((stripe) => {
      this.stripe = stripe;
    });
  }

  paymentModes() {
    this.paymentService
      .GetPaymentModes()
      .pipe(first())
      .subscribe(
        (resp) => {
          let paymentModes = JSON.parse(resp);
          this.paymentModesList = paymentModes;
          for (var i = 0; i < paymentModes.length; i++) {
            if (paymentModes[i].isdefault === 1) {
              this.paymentmethodid = paymentModes[i].customer_info.id;
              this.customerid = paymentModes[i].customer_info.customer;
            }
          }
        },
        (error) => {}
      );
  }

  handleServerResponse(response) {
    if (response.error) {
      // Show error from server on payment form
    } else if (response.requires_action) {
      // Use Stripe.js to handle required card action
      this.stripe
        .handleCardAction(response.payment_intent_client_secret)
        .then((result) => {
          if (result.error) {
            // Show error in payment form
          } else {
            // The card action has been handled
            this.paymentService
              .MakePayment({
                payment_intent_id: result.paymentIntent.id,
                idempotencyKey: response.idempotencyKey,
              })
              .pipe(first())
              .subscribe(
                (resp) => {
                  // HANDLE RESPONSE HERE
                  // SHOW MESSAGE TO USER
                },
                (error) => {
                  console.log(error);
                }
              );
          }
        });
    } else {
      // Show success message
    }
  }

  handleStripeJsResult(result) {}

  onSubmit() {
    this.submitted = true;
    if (this.formVar.invalid) {
      return;
    }
    var request = {
      title: this.title,
      description: this.description,
      finalizedDateTime: this.finalizedDateTime,
      finalizedAmount: this.formVar.value.finalizedAmount,
      proposalId: this.proposalid,
      status: 'pending',
      contractCharges: this.costhire,
    };
    this.jobpostService.Createcontract(request).subscribe(
      (data) => {
        if (data) {
          this.errorMessage = '';
          this.modalShow = false;
          // this.formVar.markAsPristine();
          this.show = true;
          this.contractreferanceid = JSON.parse(data)?.data?.referenceId;
          this.amount = JSON.parse(data)?.data?.contractCharges;
          if (JSON.parse(data)?.data?.contractCharges !== 0) {
            if (this.paymentmethodid === undefined) {
              // alert('please create a card details')
              Swal.fire({
                title: 'Please Select One Payment Details In Profile Setting',
                showClass: {
                  popup: 'animate__animated animate__fadeInDown',
                },
                hideClass: {
                  popup: 'animate__animated animate__fadeOutUp',
                },
              });
            } else if (this.customerid === undefined) {
              Swal.fire({
                title: 'Please Enter Your Card Details In Profile Setting Page',
                showClass: {
                  popup: 'animate__animated animate__fadeInDown',
                },
                hideClass: {
                  popup: 'animate__animated animate__fadeOutUp',
                },
              });
            } else {
              this.jobpostService
                .Calculatecontractcharges(JSON.parse(data)?.data?.referenceId)
                .pipe(first())
                .subscribe(
                  (data) => {
                    if (data) {
                      this.walletamount = JSON.parse(data)?.data?._WALLETPOINTS;
                      this.errorMessage = '';
                      let paymentdetails = {
                        //payment_method_id : 'pm_1HwV8nDnHwZE8EB8r2eIHp4w',
                        pharmacyUserId: this.pharmacyuser?.data?.referenceId,
                        payment_method_id: this.paymentmethodid,
                        amount: this.amount * 100,
                        currency: 'USD',
                        transactionType: 'hirePharmacist',
                        description: '{}',
                        customer: this.customerid,
                        walletAmount: this.walletamount,
                      };
                      Swal.fire({
                        title: 'Please Confirm Your Payment?',
                        text: 'You Want to Proceed With This Payment!',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Proced It!',
                      }).then((result) => {
                        if (result.isConfirmed) {
                          this.paymentService
                            .MakePayment(paymentdetails)
                            .pipe(first())
                            .subscribe(
                              (resp) => {
                                this.handleServerResponse(JSON.parse(resp));
                                Swal.fire(
                                  'Payment Successfull',
                                  'Your Payment Details Has Been Successfull.',
                                  'success'
                                );
                                // let contractreferenceid = JSON.parse(data)?.data?.referenceId
                                let request = {
                                  title: JSON.parse(data)?.data?.title,
                                  description: JSON.parse(data)?.data
                                    ?.description,
                                  finalizedDateTime: JSON.parse(data)?.data
                                    ?.finalizedDateTime,
                                  finalizedAmount: JSON.parse(data)?.data
                                    ?.finalizedAmount,
                                  proposalId: this.proposalid,
                                  status: 'waitingforpharmacist',
                                  contractCharges: JSON.parse(data)?.data
                                    ?.contractCharges,
                                };
                                this.jobpostService
                                  .Updatecontract(
                                    request,
                                    this.contractreferanceid
                                  )
                                  .subscribe(
                                    (data) => {
                                      if (data) {
                                        this.errorMessage = '';
                                        this.tosterservice.Success(
                                          'Your Contract Created Successfully'
                                        );

                                        this.allproposal(this.page);
                                      } else {
                                        this.errorMessage =
                                          'Unable to Create Contract ';
                                      }
                                    },
                                    (error) => {
                                   
                                      this.errorMessage = error.error;
                                    }
                                  );
                              },
                              (error) => {
                              }
                            );
                        }
                      });
                    }
                  },
                  (error) => {
                    this.errorMessage = error.text;
                  }
                );
            }
          } else {
            this.jobpostService
              .Calculatecontractcharges(JSON.parse(data)?.data?.referenceId)
              .pipe(first())
              .subscribe(
                (data) => {
                  if (data) {
                    this.walletamount = JSON.parse(data)?.data?._WALLETPOINTS;
                    this.errorMessage = '';
                    let bodyrequest = {
                      pharmacyUserId: this.pharmacyuser?.data?.referenceId,
                      transactionType: 'hirePharmacist',
                      stripePaymentId: '',
                      description: '{}',
                      amount: this.amount * 100,
                      status: 'processing',
                      walletAmount: this.walletamount,
                    };
                    this.jobpostService
                      .Createpaymenttransation(bodyrequest)
                      .subscribe(
                        (data) => {
                          if (data) {
                            this.errorMessage = '';
                            // let contractreferenceid = JSON.parse(data)?.data?.referenceId
                            let request = {
                              proposalId: this.proposalid,
                              status: 'waitingforpharmacist',
                            };
                            this.jobpostService
                              .Updatecontract(request, this.contractreferanceid)
                              .subscribe(
                                (data) => {
                                  if (data) {
                                    this.errorMessage = '';
                                    this.tosterservice.Success(
                                      'Your Contract Created Successfully'
                                    );

                                    this.allproposal(this.page);
                                  } else {
                                    this.errorMessage =
                                      'Unable to Create Contract ';
                                  }
                                },
                                (error) => {
                                  this.errorMessage = error.error;
                                }
                              );
                          } else {
                            this.errorMessage = 'Unable to Save transation';
                          }
                        },
                        (error) => {
                          this.errorMessage = error.error;
                        }
                      );
                  }
                },
                (error) => {
                  this.errorMessage = error.text;
                }
              );
          }
        } else {
          this.errorMessage = 'Unable to Save Jobpost';
        }
      },
      (error) => {
        this.errorMessage = error.error;
      }
    );
  }
}
