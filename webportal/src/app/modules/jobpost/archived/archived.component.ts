import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs';
import { first } from 'rxjs/operators';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { ToastrService } from '../../../toastr.service';
import { NgxSpinnerService } from 'ngx-spinner';

import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { JobpostService } from '../jobpost.service';
import * as moment from 'moment';

@Component({
  selector: 'app-archived',
  templateUrl: './archived.component.html',
  styleUrls: ['./archived.component.css'],
})
export class ArchivedComponent implements OnInit {
  public page = 1;
  public jobreferenceid: any;
  public jobid: any;
  public errorMessage: any;
  public archivedcollectionsizeee: any;
  public archivedproposallist = [];
  public showless = false;
  public viewjob = true;
  public clickedid = '';
  public softwareskiiill = [];
  public startdate :any;
  public enddate :any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private jobpostService: JobpostService,
    config: NgbPaginationConfig,
    private tosterservice: ToastrService,
    private spinner: NgxSpinnerService
  ) {
    config.size = 'sm';
    config.boundaryLinks = true;
  }
  ngOnInit(): void {
    var sub = this.route.queryParams.subscribe((params) => {
      this.jobreferenceid = atob(params.id);
      this.jobid = JSON.parse(atob(params.jobId));
    });
    this.archivedproposals(this.page);
  }
  reloadData() {
    this.archivedproposals(this.page);
  }
  remove(id) {
    var request = { isDeleted: 1 };

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, remove it!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.jobpostService.Updateproposal(request, id).subscribe(
          (data) => {
            if (data) {
              this.errorMessage = '';
              this.archivedproposals(this.page);
            } else {
              this.errorMessage = 'Unable to Save Jobpost';
            }
          },
          (error) => {
            this.errorMessage = error.error;
          }
        );
        Swal.fire('Removed!', 'Your archived has been Removed.', 'success');
      }
    });
  }
  gettime(time){
    let date =moment(new Date(time)).utc().format("hh:mm A");
      return date
  }

  viewalljobpostt(pharmacistreferenceid) {
    this.clickedid = pharmacistreferenceid;
    // alert(this.clickedid)
    this.viewjob = false;
    this.showless = true;
    this.spinner.show();
    var request = {
      jobpostId: this.jobreferenceid,
      filter: 'archived',
    };
    this.jobpostService
      .Listproposal(request, this.page)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.spinner.hide();
            var dataaa = JSON.parse(data);
            for (var i = 0; i < dataaa?.data?.listResult.length; i++) {
              this.softwareskiiill =
                dataaa?.data?.listResult[i]?.softwareSkills;
            }
          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }
  viewlessjobpost(pharmacistreferenceid) {
    this.clickedid = '';

    this.archivedproposals(this.page);
  }

  archivedproposals(archivedpage) {
    this.spinner.show();

    var request = {
      jobpostId: this.jobreferenceid,
      filter: 'archived',
    };
    this.jobpostService.Listproposal(request, archivedpage).subscribe(
      (data) => {
        if (data) {
          this.errorMessage = '';
          var proposal = JSON.parse(data);
          this.archivedcollectionsizeee =
            proposal?.data?.pagination?.totalCount;
          this.archivedproposallist = proposal.data.listResult;
          this.spinner.hide();
        } else {
          this.errorMessage = 'list proposal';
        }
      },
      (error) => {
        this.errorMessage = error.error;
      }
    );
  }
}
