import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { JobpostService } from '../jobpost.service';
import { PaymentType } from '../../../shared/enum/constants';
import { first } from 'rxjs/operators';
import * as moment from 'moment';
import { ToastrService } from '../../../toastr.service';
import { Hirecharge } from '../../../shared/config';
import { join } from '@angular/compiler-cli/src/ngtsc/file_system';
@Component({
  selector: 'app-budget-timing',
  templateUrl: './budget-timing.component.html',
  styleUrls: ['./budget-timing.component.css'],
})
export class BudgetTimingComponent implements OnInit {
  formVar: FormGroup;
  submitted = false;
  public Edit = 'false';
  public Editt = 'false';
  autohide = true;
  // show = false;
  label: any;
  public errorMessage: any;
  // public paymentType: any;
  public paymentType: any;
  public currentJobpostId: any;
  public dataa: any;
  public jobdetailsbudget: any;
  public pharmacyneed: any;
  public pharmacydetailschargeshow = false;
  public pharmacychargesdetails = [];
  public sidebarvalue: any;
  public sidebarstringifyobj: any;
  public sideBarObject: any;
  public sideBarObjectt: any;
  public nextclicksidebarobj: any;
  public joblistrefernceid: any;
  public referenceid: any;
  public endDateTime: any;
  public month: any;
  public topprogressbarline: any;
  public gobacktopprogressbarline: any;
  public perheadpharmaycost: any;
  public hirecharges: any;
  public ppharmacyid: any;
  value = 0;
  public starttime: any;
  public endtime: any;
  public totalCost: number;
  public monthstart: any;
  public todaysdate: any;
  public paymentvalue: any;
  public pharmacyuser: any;
  public hirechargevalue: number;
  public todaysdateee: any;
  public startTimeee: any;
  public finalmin: any;
  public mins: any;
  public mintime: any;
  public minhr: any;
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private JobpostService: JobpostService,
    private tosterservice: ToastrService
  ) {
    this.JobpostService.sideBarObject.subscribe((sideBarObjectt) => {
      this.sideBarObject = sideBarObjectt;
    });

    this.formVar = this.fb.group({
      paymentType: ['', Validators.required],
      fixedPrice: ['', Validators.required],
      startTime: ['', Validators.required],
      startDate: ['', Validators.required],
      // endDate: ['', Validators.required],
      endTime: ['', Validators.required],
      status: ['draft'],
    });

    this.paymentType = PaymentType;
    this.formVar.controls.paymentType.patchValue(
      PaymentType[
        PaymentType.findIndex(
          (x) => x.Value === Number(localStorage.getItem('paymentvalue'))
        )
      ].Value
    );

    if (Number(localStorage.getItem('paymentvalue')) === 100) {
      this.label = 'Enter the Fixed Price';
    } else {
      this.label = 'Enter The Hourly Rate';
    }
  }
  changetime() {
    this.starttime = this.formVar.value.startTime;
    this.endtime = this.formVar.value.endTime;

    var changetime = moment
      .utc(moment(this.endtime, 'HH:mm').diff(moment(this.starttime, 'HH:mm')))
      .format('HH:mm')
      .split(':');
    this.minhr = changetime[0];
    this.mintime = changetime[1];
    this.mins = this.minhr + '.' + this.mintime;
    this.finalmin = Number(this.minhr) + Number(this.mintime / 60);
  }
  changeendtime() {
    this.starttime = this.formVar.value.startTime;
    this.endtime = this.formVar.value.endTime;
    var changeendtime = moment
      .utc(moment(this.endtime, 'HH:mm').diff(moment(this.starttime, 'HH:mm')))
      .format('HH:mm')
      .split(':');
    this.minhr = changeendtime[0];
    this.mintime = changeendtime[1];
    this.mins = this.minhr + '.' + this.mintime;
    this.finalmin = Number(this.minhr) + Number(this.mintime / 60);
  }

  ngOnInit(): void {
    var sub = this.route.queryParams.subscribe((params) => {
      this.pharmacyneed = params.pharmacyneed;
    });
    var subb = this.route.queryParams.subscribe((params) => {
      this.Edit = params.edit;
    });
    var subb = this.route.queryParams.subscribe((params) => {
      this.Editt = params.editt;
      this.referenceid = params.id;
    });
    this.topprogressbarline = {
      title: true,
      expertise: true,
      location: true,
      budget: true,
      review: true,
    };
    this.pharmacyuser = JSON.parse(localStorage.getItem('pharmacyUserId'));
    if (
      this.pharmacyuser.data.membershipId === null ||
      localStorage.getItem('membershipname') === 'Pay as you go'
    ) {
      this.hirechargevalue = Hirecharge.Value;
    } else {
      this.hirechargevalue = 0;
    }
    this.sideBarObjectt = JSON.parse(
      localStorage.getItem('sidebarstringifyobj')
    );
    this.todaysdate = moment(new Date()).format('YYYY-MM-DD');

    this.sideBarObjectt = { ...this.sideBarObjectt, step4: true };
    this.currentJobpostId = localStorage.getItem('currentJobpost');
    var joblistobj = localStorage.getItem('joblistvalue');
    this.jobdetailsbudget = JSON.parse(joblistobj);
    this.joblistrefernceid = this.jobdetailsbudget?.data?.referenceId;
    if (this.currentJobpostId) {
      this.getjobpost(this.currentJobpostId);
    }

    // this.totalpharmacy = this.pharmacyneed;
    this.hirecharges = this.formVar.value.fixedPrice;
  }
  get f() {
    return this.formVar.controls;
  }

  clickContinue() {
    this.submitted = true;
    if (this.formVar.invalid) {
      return;
    }
    var startdateTime = moment(
      `${this.formVar.value.startDate} ${this.formVar.value.startTime}`,
      'YYYY-MM-DD HH:mm'
    )
      .format()
      .split('+')[0];
    var enddateTime = moment(
      `${this.formVar.value.startDate} ${this.formVar.value.endTime}`,
      'YYYY-MM-DD HH:mm'
    )
      .format()
      .split('+')[0];
    var request = {
      paymentType: this.formVar.value.paymentType,
      fixedPrice: this.formVar.value.fixedPrice,
      endDateTime: enddateTime,
      startDateTime: startdateTime,
      status: 'draft',
    };
    if (this.Edit === 'true') {
      localStorage.setItem(
        'topprogressbarline',
        JSON.stringify(this.topprogressbarline)
      );

      this.JobpostService.updateJobPost(
        request,
        this.currentJobpostId
      ).subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            // console.log('Job Post Updated', data);
            // console.log(this.formVar.value.fixedPrice);
            localStorage.setItem(
              'paymentvalue',
              JSON.parse(data)?.data?.paymentType
            );

            var dd = {
              perheadpharmaycost: '',
              totalpharmacy: '',
              hirecharges: '',
            };
            dd.perheadpharmaycost = this.formVar.value.fixedPrice;
            dd.totalpharmacy = this.pharmacyneed;
            dd.hirecharges = this.formVar.value.fixedPrice;
            this.pharmacychargesdetails.push(dd);
            this.pharmacydetailschargeshow = !this.pharmacydetailschargeshow;
            // localStorage.setItem('joblistvalue', data);
            this.JobpostService.sideBarObject.next(this.sideBarObjectt);
            this.nextclicksidebarobj = this.sideBarObject;
            //  this.tosterservice.Success('Your Budget And Timing Saved Successfully Click Continue For Next Page')
            this.router.navigate(['pharmacy/jobpost/review']);
          } else {
            this.errorMessage = 'Unable to Save Jobpost';
          }
        },
        (error) => {
          this.errorMessage = error.error;
        }
      );
      if (this.nextclicksidebarobj === undefined) {
        var sidebardata = localStorage.getItem('sidebarstringifyobj');

        localStorage.setItem('sidebarstringifyobj', sidebardata);
      } else {
        this.sidebarstringifyobj = JSON.stringify(this.nextclicksidebarobj);
        localStorage.setItem('sidebarstringifyobj', this.sidebarstringifyobj);
      }
    } else if (this.Editt === 'true') {
      localStorage.setItem(
        'topprogressbarline',
        JSON.stringify(this.topprogressbarline)
      );
      this.JobpostService.updateJobPost(
        request,
        this.currentJobpostId
      ).subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            localStorage.setItem(
              'paymentvalue',
              JSON.parse(data)?.data?.paymentType
            );

            var dd = {
              perheadpharmaycost: '',
              totalpharmacy: '',
              hirecharges: '',
            };
            dd.perheadpharmaycost = this.formVar.value.fixedPrice;
            dd.totalpharmacy = this.pharmacyneed;
            dd.hirecharges = this.formVar.value.fixedPrice;
            this.pharmacychargesdetails.push(dd);
            this.pharmacydetailschargeshow = !this.pharmacydetailschargeshow;
            // localStorage.setItem('joblistvalue', data);
            this.JobpostService.sideBarObject.next(this.sideBarObjectt);
            this.nextclicksidebarobj = this.sideBarObject;
            //  this.tosterservice.Success('Your Budget And Timing Saved Successfully Click Continue For Next Page')
            this.router.navigate(['pharmacy/jobpost/edit'], {
              queryParams: { id: this.referenceid },
            });
          } else {
            this.errorMessage = 'Unable to Save Jobpost';
          }
        },
        (error) => {
          this.errorMessage = error.error;
        }
      );
      if (this.nextclicksidebarobj === undefined) {
        var sidebardata = localStorage.getItem('sidebarstringifyobj');

        localStorage.setItem('sidebarstringifyobj', sidebardata);
      } else {
        this.sidebarstringifyobj = JSON.stringify(this.nextclicksidebarobj);
        localStorage.setItem('sidebarstringifyobj', this.sidebarstringifyobj);
      }
    } else {
      localStorage.setItem(
        'topprogressbarline',
        JSON.stringify(this.topprogressbarline)
      );
      this.JobpostService.updateJobPost(
        request,
        this.currentJobpostId
      ).subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            localStorage.setItem(
              'paymentvalue',
              JSON.parse(data)?.data?.paymentType
            );

            var dd = {
              perheadpharmaycost: '',
              totalpharmacy: '',
              hirecharges: '',
            };
            dd.perheadpharmaycost = this.formVar.value.fixedPrice;
            dd.totalpharmacy = this.pharmacyneed;
            dd.hirecharges = this.formVar.value.fixedPrice;
            this.pharmacychargesdetails.push(dd);
            this.pharmacydetailschargeshow = !this.pharmacydetailschargeshow;
            // localStorage.setItem('joblistvalue', data);
            this.totalCost = this.value * this.pharmacyneed * this.value;
            localStorage.setItem('totalCost', JSON.stringify(this.totalCost));

            this.JobpostService.sideBarObject.next(this.sideBarObjectt);
            this.nextclicksidebarobj = this.sideBarObject;
            localStorage.setItem(
              'sidebarstringifyobj',
              JSON.stringify(this.sideBarObject)
            );

            //  this.tosterservice.Success('Your Budget And Timing Saved Successfully Click Continue For Next Page')
            this.router.navigate(['pharmacy/jobpost/review']);
          } else {
            this.errorMessage = 'Unable to Save Jobpost';
          }
        },
        (error) => {
          this.errorMessage = error.error;
        }
      );
      if (this.nextclicksidebarobj === undefined) {
        var sidebardata = localStorage.getItem('sidebarstringifyobj');

        localStorage.setItem('sidebarstringifyobj', sidebardata);
      } else {
        this.sidebarstringifyobj = JSON.stringify(this.nextclicksidebarobj);
        localStorage.setItem('sidebarstringifyobj', this.sidebarstringifyobj);
      }
    }
  }

  onOptionsSelected(value) {
    if (value === 100) {
      this.label = 'Enter the Fixed Price';
    } else if (value === 10) {
      this.label = 'Enter The Hourly Rate';
    }
  }

  goback() {
    if (this.Editt === 'true') {
      this.router.navigate(['pharmacy/jobpost/edit']);
    } else {
      this.router.navigate(['pharmacy/jobpost/location-visibility']);
      this.sideBarObject = {
        step1: true,
        step2: true,
        step3: true,
        step4: true,
        step5: false,
      };
      this.sidebarstringifyobj = JSON.stringify(this.sideBarObject);
      localStorage.setItem('sidebarstringifyobj', this.sidebarstringifyobj);
      this.gobacktopprogressbarline = {
        title: true,
        expertise: true,
        location: true,
        budget: false,
        review: false,
      };
      localStorage.setItem(
        'topprogressbarline',
        JSON.stringify(this.gobacktopprogressbarline)
      );
      localStorage.setItem('ppharmacyid', this.ppharmacyid);
    }
  }

  getjobpost(pharmacyRefrenceId): void {
    this.JobpostService.getJobPost(pharmacyRefrenceId)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            var dataa = JSON.parse(data);
            this.ppharmacyid = dataa?.data?.pharmacyId;
            localStorage.setItem('ppharmacyid', this.ppharmacyid);
            var dd = dataa.data.endDateTime;

            var ddstart = dataa.data.startDateTime;
            //  this.month = moment(new Date(dd)).format('YYYY-MM-DD');
            //  console.log(this.month)

            var endtime = moment(dd).utc().format('HH:mm');

            if (ddstart !== null) {
              this.monthstart = moment(new Date(ddstart)).format('YYYY-MM-DD');
              var starttime = moment(ddstart).utc().format('HH:mm');
            }

            var timemins = moment
              .utc(moment(endtime, 'HH:mm').diff(moment(starttime, 'HH:mm')))
              .format('HH:mm')
              .split(':');
            this.minhr = timemins[0];
            this.mintime = timemins[1];
            this.mins = this.minhr + '.' + this.mintime;
            this.finalmin = Number(this.minhr) + Number(this.mintime / 60);

            if (this.currentJobpostId === null) {
              this.formVar.patchValue({
                // paymentType: this.formVar.value.paymentType,
                fixedPrice: this.formVar.value.fixedPrice,
                endDate: this.formVar.value.endDate,
                startDate: this.formVar.value.startDate,
                startTime: this.formVar.value.startTime,
                endTime: this.formVar.value.endTime,
              });
            } else {
              this.formVar.patchValue({
                // paymentType: dataa?.data?.paymentType,
                fixedPrice: dataa?.data?.fixedPrice,
                endDate: this.month,
                startDate: this.monthstart,
                startTime: starttime,
                endTime: endtime,
              });
            }
          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }

  onSubmit() {
    this.submitted = true;
    if (this.formVar.invalid) {
      return;
    }

    var startdateTime = moment(
      `${this.formVar.value.startDate} ${this.formVar.value.startTime}`,
      'YYYY-MM-DD HH:mm'
    ).format('YYYY-MM-DD HH:mm');
    var enddateTime = moment(
      `${this.formVar.value.startDate} ${this.formVar.value.endTime}`,
      'YYYY-MM-DD HH:mm'
    ).format('YYYY-MM-DD HH:mm');
    //   var date = new Date(this.formVar.value.startDate + ' ' + this.formVar.value.startTime);
    // console.log(date)
    var request = {
      paymentType: this.formVar.value.paymentType,
      fixedPrice: this.formVar.value.fixedPrice,
      endDateTime: enddateTime,
      startDateTime: startdateTime,
      status: 'draft',
    };
    if (this.Edit === 'true') {
      this.JobpostService.updateJobPost(
        request,
        this.currentJobpostId
      ).subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            this.JobpostService.sideBarObject.next(this.sideBarObjectt);
            this.nextclicksidebarobj = this.sideBarObject;
            this.tosterservice.Success(
              'Your Budget And Timing Saved Successfully Click Continue For Next Page'
            );
            localStorage.setItem(
              'paymentvalue',
              JSON.parse(data)?.data?.paymentType
            );
          } else {
            this.errorMessage = 'Unable to Save Jobpost';
          }
        },
        (error) => {
          this.errorMessage = error.error;
        }
      );
    } else if (this.Editt === 'true') {
      this.JobpostService.updateJobPost(
        request,
        this.currentJobpostId
      ).subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            var dd = {
              perheadpharmaycost: '',
              totalpharmacy: '',
              hirecharges: '',
            };
            dd.perheadpharmaycost = this.formVar.value.fixedPrice;
            dd.totalpharmacy = this.pharmacyneed;
            dd.hirecharges = this.formVar.value.fixedPrice;
            this.pharmacychargesdetails.push(dd);
            this.pharmacydetailschargeshow = !this.pharmacydetailschargeshow;
            // localStorage.setItem('joblistvalue', data);
            this.JobpostService.sideBarObject.next(this.sideBarObjectt);
            this.nextclicksidebarobj = this.sideBarObject;
            localStorage.setItem(
              'paymentvalue',
              JSON.parse(data)?.data?.paymentType
            );

            this.tosterservice.Success(
              'Your Budget And Timing Saved Successfully Click Continue For Next Page'
            );
          } else {
            this.errorMessage = 'Unable to Save Jobpost';
          }
        },
        (error) => {
          this.errorMessage = error.error;
        }
      );
    } else {
      this.JobpostService.updateJobPost(
        request,
        this.currentJobpostId
      ).subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            var dd = {
              perheadpharmaycost: '',
              totalpharmacy: '',
              hirecharges: '',
            };
            dd.perheadpharmaycost = this.formVar.value.fixedPrice;
            dd.totalpharmacy = this.pharmacyneed;
            dd.hirecharges = this.formVar.value.fixedPrice;
            this.pharmacychargesdetails.push(dd);
            this.pharmacydetailschargeshow = !this.pharmacydetailschargeshow;
            // localStorage.setItem('joblistvalue', data);
            localStorage.setItem(
              'paymentvalue',
              JSON.parse(data)?.data?.paymentType
            );

            this.JobpostService.sideBarObject.next(this.sideBarObjectt);
            this.nextclicksidebarobj = this.sideBarObject;
            this.tosterservice.Success(
              'Your Budget And Timing Saved Successfully Click Continue For Next Page'
            );
            localStorage.setItem(
              'sidebarstringifyobj',
              JSON.stringify(this.sideBarObject)
            );
          } else {
            this.errorMessage = 'Unable to Save Jobpost';
          }
        },
        (error) => {
          this.errorMessage = error.error;
        }
      );
    }
  }
}
