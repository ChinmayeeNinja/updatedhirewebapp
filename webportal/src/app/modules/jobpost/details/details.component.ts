import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { JobpostService } from '../jobpost.service';
import { first } from 'rxjs/operators';
import { ToastrService } from '../../../toastr.service';
// import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css'],
})
export class DetailsComponent implements OnInit {
  formVar: FormGroup;
  submitted = false;
  // show = false;
  autohide = true;
  goBackk = 'false';
  disable = true;
  public Edit = 'false';
  public Editt = 'false';
  public errorMessage: any;
  public jobpostId: any;
  // public jobdetails: any;
  public sideBarObject: any;
  public sideBarObjectt: any;
  public sidebarvalue: any;
  public sidebarstringifyobj: any;
  public nextclicksidebarobj: any;
  public sidebarobjj: {};
  public topprogressbarline: any;
  public gobacktopprogressbarline: any;
  // public joblistrefernceid:any
  public referenceid: any;
  public currentJobpostId: any;
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private JobpostService: JobpostService,
    private tosterservice: ToastrService
  ) {
    this.JobpostService.sideBarObject.subscribe((sideBarObjectt) => {
      this.sideBarObject = sideBarObjectt;
    });
  }

  ngOnInit(): void {
    var sub = this.route.queryParams.subscribe((params) => {
      this.Edit = params.edit;
      this.goBackk = params.goBack;
      this.Editt = params.editt;
      this.referenceid = params.id;
    });

    this.sideBarObjectt = JSON.parse(
      localStorage.getItem('sidebarstringifyobj')
    );
    this.sideBarObjectt = { ...this.sideBarObjectt, step1: true };
    this.currentJobpostId = localStorage.getItem('currentJobpost');
    this.topprogressbarline = {
      title: true,
      expertise: true,
      location: false,
      budget: false,
      review: false,
    };
    this.formVar = this.fb.group({
      jobName: ['', [Validators.required]],
      jobDescription: ['', [Validators.required]],
      status: ['draft'],
    });
    if (this.currentJobpostId) {
      this.getjobpost(this.currentJobpostId);
      this.disable = false;
    }
    // this.showSuccess()
  }
  get f() {
    return this.formVar.controls;
  }

  goback() {
    if (this.Editt === 'true') {
      this.router.navigate(['pharmacy/jobpost/edit']);
    } else {
      this.router.navigate(['pharmacy/jobpost/list']);

      this.gobacktopprogressbarline = {
        title: false,
        expertise: false,
        location: false,
        budget: false,
        review: false,
      };
      localStorage.setItem(
        'topprogressbarline',
        JSON.stringify(this.gobacktopprogressbarline)
      );
    }
  }

  getjobpost(pharmacyRefrenceId): void {
    this.JobpostService.getJobPost(pharmacyRefrenceId)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            var dataa = JSON.parse(data);
            if (this.currentJobpostId === null) {
              this.formVar.patchValue({
                jobName: this.formVar.value.jobName,
                jobDescription: this.formVar.value.jobDescription,
              });
            } else {
              this.formVar.patchValue({
                jobName: dataa?.data?.jobName,
                jobDescription: dataa?.data?.jobDescription,
              });
            }
          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }

  onSubmit() {
    this.submitted = true;
    if (this.formVar.invalid) {
      return;
    }
    if (this.Edit === 'true') {
      this.JobpostService.updateJobPost(
        this.formVar.value,
        this.currentJobpostId
      ).subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            this.jobpostId = JSON.parse(data);
            localStorage.setItem(
              'currentJobpost',
              this.jobpostId.data.referenceId
            );
            // alert('Job Name And Description Saved Click Continue For Next Page');
            this.JobpostService.sideBarObject.next(this.sideBarObjectt);
            this.nextclicksidebarobj = this.sideBarObject;
            this.tosterservice.Success(
              'Your Title And Description Saved Successfully '
            );
            localStorage.setItem(
              'sidebarstringifyobj',
              JSON.stringify(this.sideBarObject)
            );
            this.router.navigate(['pharmacy/jobpost/review']);
          } else {
            this.errorMessage = 'Unable to Save Jobpost';
          }
        },
        (error) => {
          this.errorMessage = error.error;
        }
      );
    } else if (this.Editt === 'true') {
      this.JobpostService.updateJobPost(
        this.formVar.value,
        this.currentJobpostId
      ).subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            this.jobpostId = JSON.parse(data);
            localStorage.setItem(
              'currentJobpost',
              this.jobpostId.data.referenceId
            );
            // alert('Job Name And Description Saved Click Continue For Next Page');
            this.JobpostService.sideBarObject.next(this.sideBarObjectt);
            this.nextclicksidebarobj = this.sideBarObject;
            this.tosterservice.Success(
              'Your Title And Description Saved Successfully '
            );
            localStorage.setItem(
              'sidebarstringifyobj',
              JSON.stringify(this.sideBarObject)
            );
            this.router.navigate(['pharmacy/jobpost/edit'], {
              queryParams: { id: this.referenceid },
            });
          } else {
            this.errorMessage = 'Unable to Save Jobpost';
          }
        },
        (error) => {
          this.errorMessage = error.error;
        }
      );
    } else if (this.goBackk === 'true') {
      localStorage.setItem(
        'topprogressbarline',
        JSON.stringify(this.topprogressbarline)
      );

      this.JobpostService.updateJobPost(
        this.formVar.value,
        this.currentJobpostId
      ).subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            this.jobpostId = JSON.parse(data);
            localStorage.setItem(
              'currentJobpost',
              this.jobpostId.data.referenceId
            );
            // alert('Job Name And Description Saved Click Continue For Next Page');
            this.JobpostService.sideBarObject.next(this.sideBarObjectt);
            this.nextclicksidebarobj = this.sideBarObject;
            this.tosterservice.Success(
              'Your Title And Description Saved Successfully '
            );
            localStorage.setItem(
              'sidebarstringifyobj',
              JSON.stringify(this.sideBarObject)
            );
            this.router.navigate(['pharmacy/jobpost/expertise']);
          } else {
            this.errorMessage = 'Unable to Save Jobpost';
          }
        },
        (error) => {
          this.errorMessage = error.error;
        }
      );
    } else {
      this.JobpostService.CreateJobPost(this.formVar.value).subscribe(
        (dataa) => {
          if (dataa) {
            this.errorMessage = '';
            this.jobpostId = JSON.parse(dataa);
            localStorage.setItem(
              'currentJobpost',
              this.jobpostId.data.referenceId
            );
            this.tosterservice.Success(
              'Your Title And Description Saved Successfully '
            );
            this.disable = false;
            // alert('Job Name And Description Saved Click Continue For Next Page');
            this.JobpostService.sideBarObject.next(this.sideBarObjectt);
            localStorage.setItem(
              'sidebarstringifyobj',
              JSON.stringify(this.sideBarObject)
            );

            this.nextclicksidebarobj = this.sideBarObject;
            this.router.navigate(['pharmacy/jobpost/expertise']);
          } else {
            this.errorMessage = 'Unable to Save Jobpost';
          }
        },
        (error) => {
          this.errorMessage = error.error;
        }
      );
      localStorage.setItem(
        'topprogressbarline',
        JSON.stringify(this.topprogressbarline)
      );

      if (this.nextclicksidebarobj === undefined) {
        var sidebardata = localStorage.getItem('sidebarstringifyobj');

        localStorage.setItem('sidebarstringifyobj', sidebardata);
      } else {
        this.sidebarstringifyobj = JSON.stringify(this.nextclicksidebarobj);
        localStorage.setItem('sidebarstringifyobj', this.sidebarstringifyobj);
      }
    }
  }
}
