import { Component, OnInit } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';
import { first } from 'rxjs/operators';

import {
  expertise,
  pharmacyOperation,
  additionalSkills,
  softwareSkills,
} from '../../../shared/enum/constants';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { JobpostService } from '../jobpost.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from '../../../toastr.service';

@Component({
  selector: 'app-expertise',
  templateUrl: 'expertise.component.html',
  styleUrls: ['expertise.component.css'],
})
export class ExpertiseComponent implements OnInit {
  display = 'none'; //default Variable
  public Edit = 'false';
  public Editt = 'false';
  autohide = true;
  // show=false;
  public modalShoww = false;
  public referenceid: any;
  public constExpertise: any;
  public constPharmacyOperations: any;
  public constAdditionalSkills: any;
  public constSoftwareSkills: any;
  public errorMessage: any;
  public expertiseForm: FormGroup;
  public formVar: FormGroup;
  public skillformVar: FormGroup;
  public submitted = false;
  public submitedd = false;
  public submittedd = false;
  public currentJobpost: any;
  public modalShow = false;
  public softskill: any;
  public skilllevel: any;
  public jobdetailsskill: any;
  public sidebarvalue: any;
  public sideBarObject: any;
  public sidebarstringifyobj: any;
  public softwareSkills = [];
  public questionarr = [];
  public topprogressbarline: any;
  public gobacktopprogressbarline: any;
  // public jobdetails: any;
  public sideBarObjectt: any;
  public nextclicksidebarobj: any;
  // public joblistrefernceid:any
  constructor(
    private formBuilder: FormBuilder,
    private jobpostService: JobpostService,
    private router: Router,
    private route: ActivatedRoute,
    private tosterservice: ToastrService
  ) {
    this.jobpostService.sideBarObject.subscribe((sideBarObjectt) => {
      this.sideBarObject = sideBarObjectt;
    });
  }

  ngOnInit(): void {
    this.currentJobpost = localStorage.getItem('currentJobpost');
    this.sideBarObjectt = JSON.parse(
      localStorage.getItem('sidebarstringifyobj')
    );
    // this.joblistrefernceid = this.jobdetails?.data?.referenceId
    // var joblistobj = localStorage.getItem('joblistvalue');
    // this.jobdetails = JSON.parse(joblistobj);
    this.sideBarObjectt = { ...this.sideBarObjectt, step2: true };
    var sub = this.route.queryParams.subscribe((params) => {
      this.Edit = params.edit;
    });
    // var sub = this.route.queryParams.subscribe((params) => {
    //   this.sidebarvalue = JSON.parse(params.sidebarobj);
    //   console.log(this.sidebarvalue);
    // });
    var subb = this.route.queryParams.subscribe((params) => {
      this.Editt = params.editt;
      this.referenceid = params.id;
    });
    this.topprogressbarline = {
      title: true,
      expertise: true,
      location: true,
      budget: false,
      review: false,
    };
    this.formVar = this.formBuilder.group({
      screeningQuestions: ['', [Validators.required]],
    });
    this.expertiseForm = this.formBuilder.group({
      additionalSkills: ['', Validators.required],
      pharmacyOperations: ['', Validators.required],
      expertise: ['', Validators.required],
      // softwareSkills: ['', Validators.required],
      // software: ['', Validators.required],
      // skillLevel: ['', Validators.required],
    });
    this.skillformVar = this.formBuilder.group({
      softwareSkills: ['', Validators.required],
      skillLevel: ['', Validators.required],
    });

    this.constAdditionalSkills = additionalSkills;
    this.constPharmacyOperations = pharmacyOperation;
    this.constExpertise = expertise;
    this.constSoftwareSkills = softwareSkills;
    if (this.currentJobpost) {
      this.getjobpost(this.currentJobpost);
    }
  }

  goback() {
    if (this.Editt === 'true') {
      this.router.navigate(['pharmacy/jobpost/edit']);
    } else {
      this.router.navigate(['pharmacy/jobpost/details'], {
        queryParams: { goBack: true },
      });
      this.sideBarObject = {
        step1: true,
        step2: false,
        step3: false,
        step4: false,
        step5: false,
      };
      this.sidebarstringifyobj = JSON.stringify(this.sideBarObject);
      localStorage.setItem('sidebarstringifyobj', this.sidebarstringifyobj);
      this.gobacktopprogressbarline = {
        title: true,
        expertise: false,
        location: false,
        budget: false,
        review: false,
      };
      localStorage.setItem(
        'topprogressbarline',
        JSON.stringify(this.gobacktopprogressbarline)
      );
    }
  }
  delet(skillname) {
    let newArr = this.softwareSkills.filter((item) => item.skill !== skillname);
    // this.softwareSkills.splice(skillname,1);
    this.softwareSkills = newArr;
  }
  delett(ques) {
    let newqueArr = this.questionarr.filter((item) => item.question !== ques);
    this.questionarr = newqueArr;
  }

  // onloaddata() {
  getjobpost(pharmacyRefrenceId): void {
    this.jobpostService
      .getJobPost(pharmacyRefrenceId)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            var dataa = JSON.parse(data);
            if (this.currentJobpost === null) {
              this.expertiseForm.patchValue({
                additionalSkills: this.expertiseForm.value.additionalSkills,
                pharmacyOperations: this.expertiseForm.value.pharmacyOperations,
                expertise: this.expertiseForm.value.expertise,
                softwareSkills: this.expertiseForm.value.softwareSkills,
                software: this.expertiseForm.value.software,
                skillLevel: this.expertiseForm.value.skillLevel,
              });
            } else {
              this.expertiseForm.patchValue({
                additionalSkills: dataa?.data?.additionalSkills,
                pharmacyOperations: dataa?.data?.pharmacyOperations,
                expertise: dataa?.data?.expertise,
                softwareSkills: dataa?.data?.softwareSkills,
                software: dataa?.data?.software,
                skillLevel: dataa?.data?.skillLevel,
              });
              if (dataa?.data?.softwareSkills) {
                for (var i = 0; i < dataa?.data?.softwareSkills.length; i++) {
                  this.softwareSkills.push(dataa?.data?.softwareSkills[i]);
                }
                for (
                  var i = 0;
                  i < dataa?.data?.screeningQuestions.length;
                  i++
                ) {
                  this.questionarr.push(dataa?.data?.screeningQuestions[i]);
                }
              }
            }
          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }

  modalHideShow() {
    this.skillformVar.reset();
    this.submittedd = false;
    this.modalShow = !this.modalShow;
    // this.selectedQuantity = null;
    // this.selectedQuantitylevel = null;
  }

  modalHideShoww() {
    this.submitedd = false;
    this.formVar.reset();
    this.modalShoww = !this.modalShoww;
  }
  // selectedQuantity = 'html';
  // selectedQuantitylevel = 'bigner';
  // onOptionsSelected(value: string) {
  //   this.softskill = this.selectedQuantity;
  // }

  // onOptionsSelecteddd(value: string) {
  //   this.skilllevel = this.selectedQuantitylevel;
  // }

  saveskills() {
    this.submittedd = true;
    if (this.skillformVar.invalid) {
      return;
    }
    const softwareSkills = { skill: '', level: '' };
    softwareSkills.skill = this.skillformVar.value.softwareSkills;
    softwareSkills.level = this.skillformVar.value.skillLevel;
    if (this.softwareSkills.length < 10) {
      this.softwareSkills.push(softwareSkills);
    } else {
      this.tosterservice.Warning('Maximum 10 Software Skill You Can Add ');
    }

    this.softwareSkills = this.softwareSkills;
    this.softwareSkills = this.softwareSkills.filter(
      (v, i, a) => a.findIndex((t) => t.skill === v.skill) === i
    );
    this.modalShow = false;
  }

  submit() {
    this.submitedd = true;
    if (this.formVar.invalid) {
      return;
    }
    const screenquestion = { question: '' };
    screenquestion.question = this.formVar.value.screeningQuestions;
    if (this.questionarr.length < 10) {
      this.questionarr.push(screenquestion);
    } else if (this.questionarr.length > 10) {
      this.tosterservice.Warning('Maximum 10 Screening Question You Can Add ');
    }
    this.questionarr = this.questionarr.filter(
      (v, i, a) => a.findIndex((t) => t.question === v.question) === i
    );
    this.modalShoww = false;
    this.formVar.patchValue({
      screeningQuestions: null,
    });
  }
  get f() {
    return this.expertiseForm.controls;
  }
  get ff() {
    return this.formVar.controls;
  }
  get fff() {
    return this.skillformVar.controls;
  }

  clickContinue() {
    this.submitted = true;
    if (this.expertiseForm.invalid) {
      return;
    }
    var request = {
      additionalSkills: this.expertiseForm.value.additionalSkills,
      pharmacyOperations: this.expertiseForm.value.pharmacyOperations,
      expertise: this.expertiseForm.value.expertise,
      softwareSkills: this.softwareSkills,
      screeningQuestions: this.questionarr,
      status: 'draft',
    };
    if (this.Edit === 'true') {
      this.submitted = true;

      localStorage.setItem(
        'topprogressbarline',
        JSON.stringify(this.topprogressbarline)
      );

      console.log('Updating Jobpost', this.expertiseForm.value);

      console.log('this.currentJobpost===>', this.currentJobpost);
      this.jobpostService.updateJobPost(request, this.currentJobpost).subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            //this.pharmacydata.length == 0 ||
            this.jobpostService.sideBarObject.next(this.sideBarObjectt);
            this.nextclicksidebarobj = this.sideBarObject;
            // this.tosterservice.Success(' Your Skillset And Level Saved Successfully Click Continue For Next Page ')
            this.router.navigate(['pharmacy/jobpost/review']);
          } else {
            this.errorMessage = 'Unable to Save Jobpost';
          }
        },
        (error) => {
          this.errorMessage = error.error;
        }
      );

      if (this.nextclicksidebarobj === undefined) {
        var sidebardata = localStorage.getItem('sidebarstringifyobj');

        localStorage.setItem('sidebarstringifyobj', sidebardata);
      } else {
        this.sidebarstringifyobj = JSON.stringify(this.nextclicksidebarobj);
        localStorage.setItem('sidebarstringifyobj', this.sidebarstringifyobj);
      }
    } else if (this.Editt === 'true') {
      this.submitted = true;

      localStorage.setItem(
        'topprogressbarline',
        JSON.stringify(this.topprogressbarline)
      );

      // this.expertiseForm.patchValue({
      //   softwareSkills: this.softwareSkills,
      // });

      this.jobpostService.updateJobPost(request, this.currentJobpost).subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            //this.pharmacydata.length == 0 ||
            this.jobpostService.sideBarObject.next(this.sideBarObjectt);
            this.nextclicksidebarobj = this.sideBarObject;
            // this.tosterservice.Success(' Your Skillset And Level Saved Successfully Click Continue For Next Page ')
            this.router.navigate(['pharmacy/jobpost/edit'], {
              queryParams: { id: this.referenceid },
            });
          } else {
            this.errorMessage = 'Unable to Save Jobpost';
          }
        },
        (error) => {
          this.errorMessage = error.error;
        }
      );

      if (this.nextclicksidebarobj === undefined) {
        var sidebardata = localStorage.getItem('sidebarstringifyobj');

        localStorage.setItem('sidebarstringifyobj', sidebardata);
      } else {
        this.sidebarstringifyobj = JSON.stringify(this.nextclicksidebarobj);
        localStorage.setItem('sidebarstringifyobj', this.sidebarstringifyobj);
      }
    } else {
      this.submitted = true;

      localStorage.setItem(
        'topprogressbarline',
        JSON.stringify(this.topprogressbarline)
      );

      // this.expertiseForm.patchValue({
      //   softwareSkills: this.softwareSkills,
      // });
      this.jobpostService.updateJobPost(request, this.currentJobpost).subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            this.jobpostService.sideBarObject.next(this.sideBarObjectt);
            this.nextclicksidebarobj = this.sideBarObject;
            // this.tosterservice.Success(' Your Skillset And Level Saved Successfully Click Continue For Next Page ')
            localStorage.setItem(
              'sidebarstringifyobj',
              JSON.stringify(this.sideBarObject)
            );

            this.router.navigate(['pharmacy/jobpost/location-visibility']);
          } else {
            this.errorMessage = 'Unable to Save Jobpost';
          }
        },
        (error) => {
          this.errorMessage = error.error;
        }
      );

      if (this.nextclicksidebarobj === undefined) {
        var sidebardata = localStorage.getItem('sidebarstringifyobj');

        localStorage.setItem('sidebarstringifyobj', sidebardata);
      } else {
        this.sidebarstringifyobj = JSON.stringify(this.nextclicksidebarobj);
        localStorage.setItem('sidebarstringifyobj', this.sidebarstringifyobj);
      }
    }
  }

  onSubmit() {
    this.submitted = true;
    if (this.expertiseForm.invalid) {
      return;
    }
    var request = {
      additionalSkills: this.expertiseForm.value.additionalSkills,
      pharmacyOperations: this.expertiseForm.value.pharmacyOperations,
      expertise: this.expertiseForm.value.expertise,
      softwareSkills: this.softwareSkills,
      screeningQuestions: this.questionarr,
      status: 'draft',
    };
    if (this.Edit === 'true') {
      this.expertiseForm.patchValue({
        softwareSkills: this.softwareSkills,
      });
      this.submitted = true;

      this.jobpostService.updateJobPost(request, this.currentJobpost).subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            //this.pharmacydata.length == 0 ||
            this.jobpostService.sideBarObject.next(this.sideBarObjectt);
            this.nextclicksidebarobj = this.sideBarObject;
            this.tosterservice.Success(
              ' Your Skillset And Level Saved Successfully Click Continue For Next Page '
            );
            localStorage.setItem(
              'topprogressbarline',
              JSON.stringify(this.topprogressbarline)
            );
          } else {
            this.errorMessage = 'Unable to Save Jobpost';
          }
        },
        (error) => {
          this.errorMessage = error.error;
        }
      );
    } else if (this.Editt === 'true') {
      this.expertiseForm.patchValue({
        softwareSkills: this.softwareSkills,
      });
      this.submitted = true;

      this.jobpostService.updateJobPost(request, this.currentJobpost).subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            //this.pharmacydata.length == 0 ||
            this.jobpostService.sideBarObject.next(this.sideBarObjectt);
            this.nextclicksidebarobj = this.sideBarObject;
            this.tosterservice.Success(
              ' Your Skillset And Level Saved Successfully Click Continue For Next Page '
            );
            localStorage.setItem(
              'topprogressbarline',
              JSON.stringify(this.topprogressbarline)
            );
          } else {
            this.errorMessage = 'Unable to Save Jobpost';
          }
        },
        (error) => {
          this.errorMessage = error.error;
        }
      );
    } else {
      // this.submitted = true;
      // console.log('Updating Jobpost', this.expertiseForm.value);
      // var request = {
      //   additionalSkills: this.expertiseForm.value.additionalSkills,
      //   pharmacyOperations: this.expertiseForm.value.pharmacyOperations,
      //   expertise: this.expertiseForm.value.expertise,
      //   softwareSkills: this.expertiseForm.value.softwareSkills,
      //   screeningQuestions: this.questionarr,

      // };
      this.jobpostService.updateJobPost(request, this.currentJobpost).subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            //this.pharmacydata.length == 0 ||
            this.jobpostService.sideBarObject.next(this.sideBarObjectt);
            this.nextclicksidebarobj = this.sideBarObject;
            this.tosterservice.Success(
              ' Your Skillset And Level Saved Successfully Click Continue For Next Page '
            );
            localStorage.setItem(
              'topprogressbarline',
              JSON.stringify(this.topprogressbarline)
            );
          } else {
            this.errorMessage = 'Unable to Save Jobpost';
          }
        },
        (error) => {
          this.errorMessage = error.error;
        }
      );
    }
  }
}
