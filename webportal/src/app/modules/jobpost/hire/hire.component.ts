import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs';
import { first } from 'rxjs/operators';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { JobpostService } from '../jobpost.service';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-hire',
  templateUrl: './hire.component.html',
  styleUrls: ['./hire.component.css'],
})
export class HireComponent implements OnInit {
  page = 1;
  public errorMessage: any;
  public jobreferenceid: any;
  public jobid: any;
  public collectionsizeee: any;
  public hireData = [];
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private jobpostService: JobpostService,
    config: NgbPaginationConfig,
    private spinner: NgxSpinnerService,

  ) {
    config.size = 'sm';
    config.boundaryLinks = true;
  }
  ngOnInit(): void {
    var sub = this.route.queryParams.subscribe((params) => {
      this.jobreferenceid = atob(params.id);
      this.jobid = JSON.parse(atob(params.jobId));
    });

    this.myhirepharmacist(this.jobreferenceid, this.page);
  }
  reloadDataa() {
    this.myhirepharmacist(this.jobreferenceid, this.page);
  }
  myhirepharmacist(jobreferenceid, page) {
                  this.spinner.show();

    this.jobpostService
      .Myhiredpharmacist(jobreferenceid, page)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            var dataaa = JSON.parse(data);
            this.collectionsizeee = dataaa?.data?.pagination?.totalCount;
            this.hireData = dataaa?.data?.listResult;
                          this.spinner.hide();

          }
        },
        (error) => {
          console.log('#66Error Message: ', error);
          this.errorMessage = error.text;
        }
      );
  }
}
