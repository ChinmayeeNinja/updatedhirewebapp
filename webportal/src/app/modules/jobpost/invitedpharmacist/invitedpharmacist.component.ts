import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs';
import { first } from 'rxjs/operators';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { JobpostService } from '../jobpost.service';
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from '../../../toastr.service';
import {
  pharmacyOperation,
  additionalSkills,
} from '../../../shared/enum/constants';
@Component({
  selector: 'app-invitedpharmacist',
  templateUrl: './invitedpharmacist.component.html',
  styleUrls: ['./invitedpharmacist.component.css']
})
export class InvitedpharmacistComponent implements OnInit {
  page = 1
  public jobreferenceid :any
  public jobid :any
  public errorMessage :any
  public collectionsizeee :any;
  public jobListinggg=[];
  public softwareskill = []
   public showless = false;
  public viewjob =true;
  public constAdditionalSkills :any;
  public constPharmacyOperations :any;
  public pharmacyoperationname :any;
  public additionalkeyname :any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private jobpostService: JobpostService,
    config: NgbPaginationConfig,
    private spinner: NgxSpinnerService,
    private tosterservice: ToastrService,

  ) {
    config.size = 'sm';
    config.boundaryLinks = true;
  }
  ngOnInit(): void {
    var sub = this.route.queryParams.subscribe((params) => {
      this.jobreferenceid = atob(params.id);
      this.jobid = JSON.parse(atob(params.jobId));
    });
    this.constAdditionalSkills = additionalSkills;
    this.constPharmacyOperations = pharmacyOperation;
    this.invitepharmacistList(this.jobreferenceid, this.page);

  }
  reloadDataa() {
    this.invitepharmacistList(this.jobreferenceid, this.page);
  }
  viewalljobpost() {
   this.viewjob = false;
    this.showless = true;
    this.jobpostService
      .InvitedPharmacistList(this.jobreferenceid, this.page)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            var dataaa = JSON.parse(data);
        
              for (var i = 0; i < dataaa?.data?.listResult.length; i++) {
              this.softwareskill =
                dataaa?.data?.listResult[i]?.softwareProficiency;
            }
          }
        },
        (error) => {
          console.log('#66Error Message: ', error);
          this.errorMessage = error.text;
        }
      );
  }
    viewlessjobpost() {
    this.invitepharmacistList(this.jobreferenceid, this.page);
  

  }

  counter(i){
    return new Array(i);
  }
  toNumber(n){
    return Number(n)
      }

  invitepharmacistList(jobreferenceid, page) {
              this.spinner.show();

    this.jobpostService
      .InvitedPharmacistList(jobreferenceid, page)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
                this.viewjob = true;
                  this.showless = false;
            var dataaa = JSON.parse(data);
            this.collectionsizeee = dataaa?.data?.pagination?.totalCount;
            // this.jobListing.push(dataaa[0]);
            this.jobListinggg = dataaa?.data?.listResult;
                for (var i = 0; i < dataaa?.data?.listResult.length; i++) {
              if (dataaa?.data?.listResult[i]?.softwareProficiency.length > 2) {
                this.softwareskill = dataaa?.data?.listResult[
                  i
                ]?.softwareProficiency.slice(0, 2);
              }
            
            }
          this.spinner.hide();

          }
        },
        (error) => {
          console.log('#66Error Message: ', error);
          this.errorMessage = error.text;
        }
      );
  }

  savepharmacist(pharmacistId) {

// alert(pharmacistId);
var request = {
  jobpostId: this.jobid,
  pharmacistId: pharmacistId,
  status: 'sent',
};
this.jobpostService.SavedPharmacist(request).subscribe(
  (data) => {
    if (data) {
      this.errorMessage = '';
      this.tosterservice.Success('You Saved The Pharmacist Successfully');
      this.invitepharmacistList(this.jobreferenceid, this.page);
    } else {
      this.errorMessage = 'Unable to Save Jobpost';
    }
  },
  (error) => {
    console.log('Error Message: ', error.error);
    this.errorMessage = error.error;
  }
);
}

  updatepharmacist(pharmacistReferenceId) {
    var pharmacistrefid = pharmacistReferenceId
    var request ={
          "isDeleted": 1
    }
     this.jobpostService.Updatepharmacist(request, pharmacistrefid).subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';

  this.tosterservice.Success('You Unsaved The Pharmacist Successfully');
  this.invitepharmacistList(this.jobreferenceid, this.page);
       } else {
            this.errorMessage = 'Unable to update pharmacist';
          }
        },
        (error) => {
          console.log('Error Message: ', error.error);
          this.errorMessage = error.error;
        }
      );
  }

  viewpharmacistdetails(refid){
    localStorage.setItem('jobreferenceid', this.jobreferenceid)
    localStorage.setItem('jobid',this.jobid)

    this.router.navigate(['pharmacy/jobpost/viewpharmacist'], {
      queryParams: { pharmacistRefid: btoa(refid),invitepharmacist: true },
    });
  }

}
