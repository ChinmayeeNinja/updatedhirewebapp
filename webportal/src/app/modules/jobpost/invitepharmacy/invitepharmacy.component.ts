import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JobpostService } from '../jobpost.service';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-invitepharmacy',
  templateUrl: './invitepharmacy.component.html',
  styleUrls: ['./invitepharmacy.component.css'],
})
export class InvitepharmacyComponent implements OnInit {
  public searchhh = true;
  public inviteddpharmacyy = false;
  public myhiree = false;
  public savedd = false;
  public jobreferenceid: any;
  public jobid: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private jobpostService: JobpostService,
    config: NgbPaginationConfig
  ) {
    config.size = 'sm';
    config.boundaryLinks = true;
  }
  ngOnInit(): void {
    var sub = this.route.queryParams.subscribe((params) => {
      this.jobreferenceid = atob(params.id);
      this.jobid = JSON.parse(atob(params.jobId));
    });
  }
  Search() {
    this.searchhh = true;
    this.inviteddpharmacyy = false;
    this.myhiree = false;
    this.savedd = false;
    this.router.navigate(['pharmacy/jobpost/view/invitepharmacy'], {
      queryParams: { id: btoa(this.jobreferenceid), jobId: btoa(this.jobid) },
    });
  }
  invitedpharmacist() {
    this.searchhh = false;
    this.inviteddpharmacyy = true;
    this.myhiree = false;
    this.savedd = false;
    this.router.navigate(
      ['pharmacy/jobpost/view/invitepharmacy/invitedpharmacist'],
      {
        queryParams: { id: btoa(this.jobreferenceid), jobId: btoa(this.jobid) },
      }
    );
  }
  myhire() {
    this.searchhh = false;
    this.inviteddpharmacyy = false;
    this.myhiree = true;
    this.savedd = false;
    this.router.navigate(['pharmacy/jobpost/view/invitepharmacy/myhire'], {
      queryParams: { id: btoa(this.jobreferenceid), jobId: btoa(this.jobid) },
    });
  }
  Saved() {
    this.searchhh = false;
    this.inviteddpharmacyy = false;
    this.myhiree = false;
    this.savedd = true;
    this.router.navigate(['pharmacy/jobpost/view/invitepharmacy/saved'], {
      queryParams: { id: btoa(this.jobreferenceid), jobId: btoa(this.jobid) },
    });
  }
  isLinkActive(url): boolean {
    const queryParamsIndex = this.router.url.indexOf('?');
    const baseUrl =
      queryParamsIndex === -1
        ? this.router.url
        : this.router.url.slice(0, queryParamsIndex);
    return baseUrl === url;
  }
}
