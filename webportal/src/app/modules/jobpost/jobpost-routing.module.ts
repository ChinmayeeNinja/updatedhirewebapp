import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailsComponent } from './details/details.component';
import { ExpertiseComponent } from './expertise/expertise.component';
import { LocationVisibilityComponent } from './location-visibility/location-visibility.component';
import { BudgetTimingComponent } from './budget-timing/budget-timing.component';
import { ReviewComponent } from './review/review.component';
// import { ViewComponent } from './view/view.component';
import { ListComponent } from './list/list.component';
import { AuthLayoutComponent } from '../shared/layout/auth-layout/auth-layout.component';
import { UnauthLayoutComponent } from '../shared/layout/unauth-layout/unauth-layout.component';
import { ViewComponent } from './view/view.component';
import { ViewproposalComponent } from './viewproposal/viewproposal.component';
import { EditComponent } from './edit/edit.component';
import { ReusejobpostComponent } from './reusejobpost/reusejobpost.component';
import { from } from 'rxjs';
import { ViewjobpostComponent } from './viewjobpost/viewjobpost.component';
import { InvitepharmacyComponent } from './invitepharmacy/invitepharmacy.component';
import { ReviewproposalComponent } from './reviewproposal/reviewproposal.component';
import { ProfileComponent } from './profile/profile.component'
import {CvComponent} from './cv/cv.component'
import { HireComponent } from './hire/hire.component';
import { SearchComponent } from './search/search.component';
import { InvitedpharmacistComponent } from './invitedpharmacist/invitedpharmacist.component';
import { MyhireComponent } from './myhire/myhire.component';
import { SavedComponent } from './saved/saved.component';
import { MessagedComponent } from './messaged/messaged.component';
import { AllproposalComponent } from './allproposal/allproposal.component';
import { ArchivedComponent } from './archived/archived.component';
import { ShortlistedComponent } from './shortlisted/shortlisted.component';
import { ViewpharmacistComponent } from './viewpharmacist/viewpharmacist.component';

const routes: Routes = [
  {
    path: '',
    component: ListComponent,
  },
  {
    path: 'list',
    component: ListComponent,
  },
  {
    path: 'details',
    component: DetailsComponent,
  },
  {
    path: 'expertise',
    component: ExpertiseComponent,
  },
  {
    path: 'budget-timing',
    component: BudgetTimingComponent,
  },
  {
    path: 'review',
    component: ReviewComponent,
  },
  {
    path: 'view',
    component: ViewComponent,
    children: [
      { path: '', component: ViewjobpostComponent },
      {
        path: 'invitepharmacy',
        component: InvitepharmacyComponent,
        children: [
          { path: '', component: SearchComponent },
          {
            path: 'invitedpharmacist',
            component: InvitedpharmacistComponent,
          },
          { path: 'myhire', component: MyhireComponent },
          { path: 'saved', component: SavedComponent },
        ],
      },
      {
        path: 'reviewproposal',
        component: ReviewproposalComponent,
        children: [
          { path: '', component: AllproposalComponent },
          {
            path: 'Shortlisted',
            component: ShortlistedComponent,
          },
          { path: 'Messaged', component: MessagedComponent },
          { path: 'archived', component: ArchivedComponent },
        ],
      },
     
      { path: 'hire', component: HireComponent },
    ],
  },
  {
    path: 'edit',
    component: EditComponent,
  },
  
  {
    path: 'location-visibility',
    component: LocationVisibilityComponent,
  },
  {
    path: 'resusejobpost',
    component: ReusejobpostComponent,
  },
  {
    path: 'viewpharmacist',
    component: ViewpharmacistComponent,
  },
   {
        path: 'viewproposal',
        component: ViewproposalComponent,
        children: [
          { path: '', component: ProfileComponent },
          { path: 'cv', component: CvComponent },
        ],
      },
      
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JobpostRoutingModule {}
