import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { JobpostRoutingModule } from './jobpost-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DetailsComponent } from './details/details.component';
import { ExpertiseComponent } from './expertise/expertise.component';
import { LocationVisibilityComponent } from './location-visibility/location-visibility.component';
import { ReviewComponent } from './review/review.component';
import { BudgetTimingComponent } from './budget-timing/budget-timing.component';
import { JobpostProgressbarComponent } from '../shared/layout/authorized/jobroutingsidebar/jobpost-progressbar/jobpost-progressbar.component';
import { from } from 'rxjs';
import { ViewComponent } from './view/view.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EditComponent } from './edit/edit.component';
import { ReusejobpostComponent } from './reusejobpost/reusejobpost.component';
import { ViewproposalComponent } from './viewproposal/viewproposal.component';
import { SearchComponent } from './search/search.component';
import { HireComponent } from './hire/hire.component';
import { SavedComponent } from './saved/saved.component';
import { InvitedpharmacistComponent } from './invitedpharmacist/invitedpharmacist.component';
import { InvitepharmacyComponent } from './invitepharmacy/invitepharmacy.component';
import { ReviewproposalComponent } from './reviewproposal/reviewproposal.component';
import { MyhireComponent } from './myhire/myhire.component';
import { ViewjobpostComponent } from './viewjobpost/viewjobpost.component';
import { AllproposalComponent } from './allproposal/allproposal.component';
import { MessagedComponent } from './messaged/messaged.component';
import { ShortlistedComponent } from './shortlisted/shortlisted.component';
import { ArchivedComponent } from './archived/archived.component';
import { ToastrModule, ToastNoAnimation, ToastNoAnimationModule } from 'ngx-toastr';
import { ArchwizardModule } from 'ng2-archwizard';
import { ProfileComponent } from './profile/profile.component';
import { CvComponent } from './cv/cv.component';
import { ViewpharmacistComponent } from './viewpharmacist/viewpharmacist.component';
import { StarComponent } from './stars.component';
import { StarItemComponent } from './star.component';
@NgModule({
  declarations: [
    ListComponent,
    DetailsComponent,
    ExpertiseComponent,
    LocationVisibilityComponent,
    ReviewComponent,
    BudgetTimingComponent,
    JobpostProgressbarComponent,
    ViewComponent,
    EditComponent,
    ReusejobpostComponent,
    ViewproposalComponent,
    SearchComponent,
    HireComponent,
    SavedComponent,
    InvitedpharmacistComponent,
    InvitepharmacyComponent,
    ReviewproposalComponent,
    MyhireComponent,
    ViewjobpostComponent,
    AllproposalComponent,
    MessagedComponent,
    ShortlistedComponent,
    ArchivedComponent,
    ProfileComponent,
    CvComponent,
    ViewpharmacistComponent,
    StarComponent,StarItemComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    JobpostRoutingModule,
    HttpClientModule,
    NgbModule,
    ToastNoAnimationModule.forRoot(),


   
  ],
})
export class JobpostModule {}
