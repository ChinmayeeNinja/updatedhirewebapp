import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../../dashboard/dashboard.service';
import { first } from 'rxjs/operators';
import { Router, CanActivate, ActivatedRoute } from '@angular/router';
import { JobpostService } from '../jobpost.service';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from "ngx-spinner";
import { PaymentType } from '../../../shared/enum/constants';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent implements OnInit {
  Object = Object;
  showw = true;
  open = false;
  close =true
  page = 1;
  public collectionsize: number;
  public errorMessage: any;
  public pharmacyUser: any;
  public authenticationId: any;
  public sideBarObjectt: any;
  public sidebarstringifyobj: any;
  public topprogressbarline : any
  public jobListing = {};
    public paymentType: any;
    public pharmacylistid :any
  public clickedid = ""
  constructor(
    // private dashboardService: DashboardService,
    private route: ActivatedRoute,
    private router: Router,
    // private dashboardService: DashboardService,
    private jobpostService: JobpostService,
        private spinner: NgxSpinnerService

  ) {
      this.pharmacylistid = JSON.parse(
      localStorage.getItem('pharmacylist'))?.data[0]?.id
  }

  ngOnInit(): void {
              this.paymentType = PaymentType;

    this.pharmacyUser = JSON.parse(localStorage.getItem('pharmacyUserId'));
    // this.authenticationId = localStorage.getItem('signupUserReferenceId');
    this.getjoblisting(this.pharmacyUser.data.referenceId, this.page);
    this.sideBarObjectt = {
      step1: false,
      step2: false,
      step3: false,
      step4: false,
      step5: false,
    };
    this.sidebarstringifyobj = JSON.stringify(this.sideBarObjectt);
    this.topprogressbarline={
      title:true,
      expertise:false,
      location:false,
      budget:false,
      review:false
    }
    localStorage.setItem('topprogressbarline', JSON.stringify(this.topprogressbarline));
    localStorage.setItem('sidebarstringifyobj', JSON.stringify(this.sideBarObjectt));
 
  }

  openclose(id) {
    this.open = !this.open
    this.clickedid = id
  }
  closeopen(id) {
      this.close =true
    this.clickedid = ""

  }
  reloadData() {
    this.getjoblisting(this.pharmacyUser.data.referenceId, this.page);
  }
  viewjobdetals(id, jobId) {
    this.router.navigate(['pharmacy/jobpost/view'], {
      queryParams: { id: btoa(id), jobId: btoa(jobId) },
    });
  }

  editjobdetails(id) {
    this.router.navigate(['pharmacy/jobpost/edit']);
    localStorage.setItem(
      'currentJobpost', id);
     localStorage.setItem('ppharmacyid',JSON.parse(
      localStorage.getItem('pharmacylist')).data[0].id
    )
        localStorage.setItem('paymentvalue',this.paymentType[0].Value)
  }
  toNumber(n){
return Number(n)
  }
  deletejobdetalis(jobReferenceId){
    var request = {
      isDeleted: 1

    }
    Swal.fire({
  title: 'Are you sure?',
  text: "You want to delete this jobpost!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire(
      'Deleted!',
      'Your file has been deleted.',
      'success'
    )
      this.jobpostService
    .updateJobPost(request, jobReferenceId)
    .subscribe(
      (data) => {
        if (data) {
          this.errorMessage = '';
          this.getjoblisting(this.pharmacyUser.data.referenceId, this.page);

        } else {
          this.errorMessage = 'Unable to Save Jobpost';
          console.log('Unable to Save Jobpost');
        }
      },
      (error) => {
        console.log('Error Message: ', error.error);
        this.errorMessage = error.error;
      }
    );
  }
})
  
  }
  postNewJob() {
    this.router.navigate(['pharmacy/jobpost/details']);
    localStorage.removeItem('currentJobpost')
    // localStorage.removeItem('sidebarstringifyobj')
    localStorage.removeItem('sideBarObject')
// localStorage.removeItem('ppharmacyid')
      localStorage.setItem('ppharmacyid', this.pharmacylistid
    )
        localStorage.setItem('paymentvalue',this.paymentType[0].Value)

  }
  resusejobpost() {
    this.router.navigate(['pharmacy/jobpost/resusejobpost']);
    localStorage.setItem('sidebarstringifyobj', this.sidebarstringifyobj);
    localStorage.setItem('topprogressbarline', JSON.stringify(this.topprogressbarline));
localStorage.removeItem('ppharmacyid')
    localStorage.removeItem('currentJobpost')

  }

  getjoblisting(pharmacyRefrenceId, page): void {
                this.spinner.show();

    this.jobpostService
      .getPharmacyJoblisting(pharmacyRefrenceId, page)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            var dataaa = data;
            this.collectionsize = dataaa?.data?.pagination?.totalCount;

            this.jobListing = dataaa?.data?.listResult;
            this.errorMessage = '';
                            this.spinner.hide();

          }
        },
        (error) => {
          console.log('#66Error Message: ', error);
          this.errorMessage = error.text;
        }
      );
  }
}
