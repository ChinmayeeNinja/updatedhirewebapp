import { Component, OnInit } from '@angular/core';
import { Router, CanActivate, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../auth/auth.service';
import { jobVisibility } from '../../../shared/enum/constants';
import { JobpostService } from '../jobpost.service';
import {
  FormBuilder,
  FormGroup,
  Validators,
  ValidatorFn,
  FormArray,
  FormControl,
} from '@angular/forms';
import { first } from 'rxjs/operators';
import {ToastrService} from '../../../toastr.service'

@Component({
  selector: 'app-location-visibility',
  templateUrl: './location-visibility.component.html',
  styleUrls: ['./location-visibility.component.css'],
})
export class LocationVisibilityComponent implements OnInit {
  formVar;
  submitted = false;
  disable = true;
  autohide = true;
  // show = false;
  public Edit = 'false';
  public Editt = 'false';
  public next = 'false';
  public errorMessage: any;
  public pharmacyUserDetailsLocalStorage = [];
  public currentJobpostId: any;
  public constJobvisibility: any;
  public pharmacyid: any;
  public request: any;
  public jobdetailslocation: any;
  public sidebarstringifyobj: any;
  public sidebarvalue: any;
  public sideBarObject: any;
  public sideBarObjectt = {};
  public nextclicksidebarobj: any;
  public pharmaceneed: any;
  public referenceid: any;
  public joblistrefernceid: any;
  public ppharmacyid: any;
  public topprogressbarline: any;
  public gobacktopprogressbarline: any;
  public radioSel: any;
  public radioSelectedString: any;
 myCategory = [];
  pharmacyuserid;
  constructor(
    private fb: FormBuilder,
    private jobpostService: JobpostService,
    private router: Router,
    private route: ActivatedRoute,
    private tosterservice:ToastrService,

    private authService: AuthService
  ) {
    this.jobpostService.sideBarObject.subscribe((sideBarObjectt) => {
      this.sideBarObject = sideBarObjectt;
    });
    this.pharmacyUserDetailsLocalStorage = JSON.parse(
      localStorage.getItem('pharmacylist')).data;
    this.pharmacyuserid = Number(localStorage.getItem('ppharmacyid'));
    this.currentJobpostId = localStorage.getItem('currentJobpost');
    if (this.currentJobpostId) {
      this.getjobpost(this.currentJobpostId);
    }
    this.formVar = this.fb.group({
      pharmacistNeeded: ['', [Validators.required]],
      JobVisibility: ['', [Validators.required]],
        myCategory: ['', [Validators.required]],
         });
    this.myCategory = this.pharmacyUserDetailsLocalStorage;
      this.formVar.controls.myCategory.patchValue(
                  this.pharmacyUserDetailsLocalStorage[
                    this.pharmacyUserDetailsLocalStorage.findIndex(
                    (x) => x.id === Number(this.pharmacyuserid)
                  )
                  ].id
                );
  }
   
  ngOnInit(): void {
    var sub = this.route.queryParams.subscribe((params) => {
      this.Edit = params.edit;
      this.next = params.next;
    });
    var subb = this.route.queryParams.subscribe((params) => {
      this.Editt = params.editt;
      this.referenceid = params.id;
    });
    this.topprogressbarline = {
      title: true,
      expertise: true,
      location: true,
      budget: true,
      review: false,
    };
    this.constJobvisibility = jobVisibility;

    this.sideBarObjectt = JSON.parse(
      localStorage.getItem('sidebarstringifyobj')
    );
    this.sideBarObjectt = { ...this.sideBarObjectt, step3: true };
  }
  getjobpost(pharmacyRefrenceId): void {
    this.jobpostService
      .getJobPost(pharmacyRefrenceId)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            var dataa = JSON.parse(data);
            this.jobdetailslocation = dataa;
            this.ppharmacyid = dataa?.data?.pharmacyId;
            if (this.currentJobpostId === null) {
              this.formVar.patchValue({
                // jobName: this.formVar.value.jobName,
                JobVisibility: this.formVar.value.JobVisibility,
                pharmacistNeeded: this.formVar.value.pharmacistNeeded,
              });
            } else {
              this.formVar.patchValue({
                // jobName: this.jobdetailslocation?.data?.jobName,
                pharmacistNeeded: dataa?.data?.pharmacistNeeded,
                JobVisibility: dataa?.data?.JobVisibility,
                pharmacyId: dataa?.data?.pharmacyId,               
              });
                  
            }
          }
        },
        (error) => {
          console.log('#66Error Message: ', error);
          this.errorMessage = error.text;
        }
      );
  }

  // checkboxclick(event) {
  //   this.pharmacyid = event.target.value;
  //       console.log(event.target.value);
  // }
  get f() {
    return this.formVar.controls;
  }
  //////////////////  continue  ///////////////////

  clickContinue() {
    this.submitted = true;
    if (this.formVar.invalid) {
      return;
    }
      this.request = {
        pharmacistNeeded: this.formVar.value.pharmacistNeeded,

        JobVisibility: this.formVar.value.JobVisibility,

        pharmacyId: this.formVar.value.myCategory,
                    status: 'draft',

      };
        this.pharmaceneed = this.formVar.value.pharmacistNeeded;

    if (this.Edit === 'true') {
      this.pharmaceneed = this.formVar.value.pharmacistNeeded;

      localStorage.setItem(
        'topprogressbarline',
        JSON.stringify(this.topprogressbarline)
      );
         
      this.jobpostService
        .updateJobPost(this.request, this.currentJobpostId)
        .subscribe(
          (data) => {
            if (data) {
              this.errorMessage = '';
              this.jobpostService.sideBarObject.next(this.sideBarObjectt);
              this.nextclicksidebarobj = this.sideBarObject;
              this.disable = false;
                // this.tosterservice.Success('Your Location And Jobvisibility Saved Successfully ')

              this.jobpostService.sideBarObject.next(this.sideBarObjectt);
              this.nextclicksidebarobj = this.sideBarObject;
              localStorage.setItem(
                'sidebarstringifyobj',
                JSON.stringify(this.sideBarObject)
              );
              this.router.navigate(['pharmacy/jobpost/review']);
            } else {
              this.errorMessage = 'Unable to Save Jobpost';
            }
          },
          (error) => {
            console.log('Error Message: ', error.error);
            this.errorMessage = error.error;
          }
        );
      if (this.nextclicksidebarobj === undefined) {
        var sidebardata = localStorage.getItem('sidebarstringifyobj');

        localStorage.setItem('sidebarstringifyobj', sidebardata);
      } else {
        this.sidebarstringifyobj = JSON.stringify(this.nextclicksidebarobj);
        localStorage.setItem('sidebarstringifyobj', this.sidebarstringifyobj);
      }
    } else if (this.Editt === 'true') {
      this.pharmaceneed = this.formVar.value.pharmacistNeeded;
      localStorage.setItem(
        'topprogressbarline',
        JSON.stringify(this.topprogressbarline)
      );
          this.jobpostService
        .updateJobPost(this.request, this.currentJobpostId)
        .subscribe(
          (data) => {
            if (data) {
              this.errorMessage = '';
              this.jobpostService.sideBarObject.next(this.sideBarObjectt);
              this.nextclicksidebarobj = this.sideBarObject;
              this.disable = false;
  // this.tosterservice.Success('Your Location And Jobvisibility Saved Successfully ')
              this.jobpostService.sideBarObject.next(this.sideBarObjectt);
              this.nextclicksidebarobj = this.sideBarObject;
              localStorage.setItem(
                'sidebarstringifyobj',
                JSON.stringify(this.sideBarObject)
              );
              this.router.navigate(['pharmacy/jobpost/edit'], {
                queryParams: { id: this.referenceid },
              });
            } else {
              this.errorMessage = 'Unable to Save Jobpost';
            }
          },
          (error) => {
            console.log('Error Message: ', error.error);
            this.errorMessage = error.error;
          }
        );
      if (this.nextclicksidebarobj === undefined) {
        var sidebardata = localStorage.getItem('sidebarstringifyobj');

        localStorage.setItem('sidebarstringifyobj', sidebardata);
      } else {
        this.sidebarstringifyobj = JSON.stringify(this.nextclicksidebarobj);
        localStorage.setItem('sidebarstringifyobj', this.sidebarstringifyobj);
      }
    } else {
      this.pharmaceneed = this.formVar.value.pharmacistNeeded;
      localStorage.setItem(
        'topprogressbarline',
        JSON.stringify(this.topprogressbarline)
      );
     
      this.jobpostService
        .updateJobPost(this.request, this.currentJobpostId)
        .subscribe(
          (data) => {
            if (data) {
              this.errorMessage = '';
              this.jobpostService.sideBarObject.next(this.sideBarObjectt);
              this.nextclicksidebarobj = this.sideBarObject;
              this.disable = false;
  // this.tosterservice.Success('Your Location And Jobvisibility Saved Successfully ')
              this.jobpostService.sideBarObject.next(this.sideBarObjectt);
              this.nextclicksidebarobj = this.sideBarObject;
              localStorage.setItem(
                'sidebarstringifyobj',
                JSON.stringify(this.sideBarObject)
              );
              this.router.navigate(['pharmacy/jobpost/budget-timing'], {
                queryParams: {
                  pharmacyneed: this.formVar.value.pharmacistNeeded,
                },
              });
            } else {
              this.errorMessage = 'Unable to Save Jobpost';
            }
          },
          (error) => {
            console.log('Error Message: ', error.error);
            this.errorMessage = error.error;
          }
        );
      if (this.nextclicksidebarobj === undefined) {
        var sidebardata = localStorage.getItem('sidebarstringifyobj');

        localStorage.setItem('sidebarstringifyobj', sidebardata);
      } else {
        this.sidebarstringifyobj = JSON.stringify(this.nextclicksidebarobj);
        localStorage.setItem('sidebarstringifyobj', this.sidebarstringifyobj);
      }
    }
  }

  ////////////////// Go back ///////////////////

  goback() {
    if(this.Editt === 'true'){
      this.router.navigate(['pharmacy/jobpost/edit']);

    }
  else  if (this.next === 'true') {
      this.router.navigate(['pharmacy/jobpost/resusejobpost']);
    }
    else{
      this.router.navigate(['pharmacy/jobpost/expertise']);
      this.sideBarObject = {
        step1: true,
        step2: true,
        step3: false,
        step4: false,
        step5: false,
      };
      this.sidebarstringifyobj = JSON.stringify(this.sideBarObject);
      localStorage.setItem('sidebarstringifyobj', this.sidebarstringifyobj);
      this.gobacktopprogressbarline = {
        title: true,
        expertise: true,
        location: false,
        budget: false,
        review: false,
      };
      localStorage.setItem(
        'topprogressbarline',
        JSON.stringify(this.gobacktopprogressbarline)
      );
    }
  
  }

  // ////////////save///////////////
  onSubmit() {
    this.submitted = true;
    if (this.formVar.invalid) {
      return;
    }
        this.request = {
        // jobName: this.formVar.value.jobName,
        pharmacistNeeded: this.formVar.value.pharmacistNeeded,

        JobVisibility: this.formVar.value.JobVisibility,

        pharmacyId: this.formVar.value.myCategory,
                            status: 'draft',

      };
    this.pharmaceneed = this.formVar.value.pharmacistNeeded;

    if (this.Edit === 'true') {
  
      this.jobpostService
        .updateJobPost(this.request, this.currentJobpostId)
        .subscribe(
          (data) => {
            if (data) {
              this.errorMessage = '';
              this.jobpostService.sideBarObject.next(this.sideBarObjectt);
              this.nextclicksidebarobj = this.sideBarObject;
              this.disable = false;
              var dataa = JSON.parse(data)
              this.ppharmacyid = dataa.data.pharmacyId
               localStorage.setItem('ppharmacyid', this.ppharmacyid);

               this.tosterservice.Success('Your Location And Jobvisibility Saved Successfully ')

              //this.pharmacydata.length == 0 ||
            } else {
              this.errorMessage = 'Unable to Save Jobpost';
            }
          },
          (error) => {
            console.log('Error Message: ', error.error);
            this.errorMessage = error.error;
          }
        );
    } else if (this.Editt === 'true') {
      
      this.jobpostService
        .updateJobPost(this.request, this.currentJobpostId)
        .subscribe(
          (data) => {
            if (data) {
              this.errorMessage = '';
              this.jobpostService.sideBarObject.next(this.sideBarObjectt);
              this.nextclicksidebarobj = this.sideBarObject;
              this.disable = false;
            this.tosterservice.Success('Your Location And Jobvisibility Saved Successfully ')
            var dataa = JSON.parse(data)
              this.ppharmacyid = dataa.data.pharmacyId
               localStorage.setItem('ppharmacyid', this.ppharmacyid);
              //this.pharmacydata.length == 0 ||
            } else {
              this.errorMessage = 'Unable to Save Jobpost';
            }
          },
          (error) => {
            console.log('Error Message: ', error.error);
            this.errorMessage = error.error;
          }
        );
    } else {
      
      this.jobpostService
        .updateJobPost(this.request, this.currentJobpostId)
        .subscribe(
          (data) => {
            if (data) {
              this.errorMessage = '';
              this.jobpostService.sideBarObject.next(this.sideBarObjectt);
              this.nextclicksidebarobj = this.sideBarObject;
  this.tosterservice.Success('Your Location And Jobvisibility Saved Successfully ')
              localStorage.setItem(
                'sidebarstringifyobj',
                JSON.stringify(this.sideBarObject)
              );
               var dataa = JSON.parse(data)
              this.ppharmacyid = dataa.data.pharmacyId
               localStorage.setItem('ppharmacyid', this.ppharmacyid);
              //this.pharmacydata.length == 0 ||
            } else {
              this.errorMessage = 'Unable to Save Jobpost';
            }
          },
          (error) => {
            console.log('Error Message: ', error.error);
            this.errorMessage = error.error;
          }
        );
    }
  }
}
