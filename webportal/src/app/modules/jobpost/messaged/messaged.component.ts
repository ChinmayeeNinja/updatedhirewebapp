import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs';
import { first } from 'rxjs/operators';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import { AngularStripeService } from '@fireflysemantics/angular-stripe-service';
import { AuthService } from '../../auth/auth.service';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { JobpostService } from '../jobpost.service';
import { ToastrService } from '../../../toastr.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { PaymentService } from '../../settings/payment/payment.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-messaged',
  templateUrl: './messaged.component.html',
  styleUrls: ['./messaged.component.css']
})
export class MessagedComponent implements OnInit {
  formVar: FormGroup;
  public jobreferenceid: any;
  public jobid: any;
  public errorMessage: any;
  page = 1;
public pharmacistpoposalname =[];
public pharmacistid :any;
public pharmacistjobid :any;
public pharmacistrefid :any;
public pharmacistjobrefid :any;
public listmessage =[]
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private jobpostService: JobpostService,
    private tosterservice: ToastrService,
    private spinner: NgxSpinnerService,
    private stripeService: AngularStripeService,
    private authService: AuthService,
    private paymentService: PaymentService,

    config: NgbPaginationConfig
  ) { }

  ngOnInit(): void {
    var sub = this.route.queryParams.subscribe((params) => {
      this.jobreferenceid = atob(params?.id);
      this.jobid = JSON.parse(atob(params?.jobId));
    });
    this.formVar = this.fb.group({
      message: ['', [Validators.required]],

    });
    this.allproposal(this.page);
  }

  allproposal(page) {
    this.spinner.show();
    var request = {
      jobpostId: this.jobreferenceid,
      filter: 'all',
    };
    this.jobpostService.Listproposal(request, page).subscribe(
      (data) => {
        if (data) {
          this.pharmacistpoposalname =JSON.parse(data)?.data?.listResult
          this.errorMessage = '';
          this.spinner.hide();
        } else {
          this.errorMessage = 'list proposal';
          console.log('list proposal');
        }
      },
      (error) => {
        console.log('Error Message: ', error.error);
        this.errorMessage = error.error;
      }
    );
  }

  startconvertion(jobId,pharmacistid,jobrefid,pharmacistrefid){
    this.pharmacistjobid = jobId,
    this.pharmacistid =pharmacistid,
    this.pharmacistjobrefid = jobrefid,
    this.pharmacistrefid = pharmacistrefid
    this.getmessage()
  }
  onSubmit(){
    var request = {
      pharmacyUserId: JSON.parse(localStorage.getItem('pharmacyUserId')).data.id,
    pharmacistId: this.pharmacistid,
    jobpostId: this.pharmacistjobid,
    message: this.formVar.value.message,
    sender:2
    };
      this.jobpostService.Createmessage(
        request
      ).subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            this.getmessage()
            this.formVar.reset();
          } else {
            this.errorMessage = 'Unable to save message';
          }
        },
        (error) => {
          this.errorMessage = error.error;
        }
      );
    }

    getmessage() {
      this.spinner.show();
      var request = {
        pharmacyUserId: JSON.parse(localStorage.getItem('pharmacyUserId')).data.referenceId,
        pharmacistId: this.pharmacistrefid,
        jobpostId: this.pharmacistjobrefid
      };

      this.jobpostService.Getmessagehistory(request).subscribe(
        (data) => {
          if (data) {
            this.listmessage =JSON.parse(data).data
            this.errorMessage = '';
            this.spinner.hide();
          } else {
            this.errorMessage = 'list message';
          }
        },
        (error) => {
          console.log('Error Message: ', error.error);
          this.errorMessage = error.error;
        }
      );
     
     
    }

  }


