import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs';
import { first } from 'rxjs/operators';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { JobpostService } from '../jobpost.service';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
 public proposareferenceid: any;
  public errorMessage: any;
  public proposaldetails: any;
  public ratingreview =[]
  constructor(  private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private jobpostService: JobpostService,
    config: NgbPaginationConfig) {}

  ngOnInit(): void {
     this.route.queryParams.subscribe((params) => {
      this.proposareferenceid = atob(params.proposalreferanseid);
    });
    this.viewproposal(this.proposareferenceid);
  }

    viewproposal(proposareferenceid): void {
    this.jobpostService
      .Viewproposal(proposareferenceid)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.proposaldetails = JSON.parse(data).data.listResult;
            this.ratingreview = JSON.parse(data).data.ratingReviews
          }
        },
        (error) => {
          console.log('#66Error Message: ', error);
          this.errorMessage = error.text;
        }
      );
  }

}
