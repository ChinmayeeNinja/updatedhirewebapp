import { Component, OnInit } from '@angular/core';
import { Router, CanActivate, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DashboardService } from '../../dashboard/dashboard.service';
import { first } from 'rxjs/operators';
import { JobpostService } from '../jobpost.service';

@Component({
  selector: 'app-reusejobpost',
  templateUrl: './reusejobpost.component.html',
  styleUrls: ['./reusejobpost.component.css'],
})
export class ReusejobpostComponent implements OnInit {
  Object = Object;
    show = false;
  submitted = false;

  formVar: FormGroup;
  public topprogressbarline :any
  public sideBarObject: any;
  public sidebarstringifyobj: any;
  public pharmacyUser: any;
  public errorMessage: any;
  public jobpostId: any
  public currentJobpostId: any
  public joblistarrayobj: any;
  public jobreferenceid: any;
  public jobnamee: any
  public sideBarObjectt: any;
  public jobnamelist =[];
  page=1;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private JobpostService: JobpostService
  ) {}

  ngOnInit(): void {
        this.currentJobpostId = localStorage.getItem('currentJobpost');
    this.pharmacyUser = JSON.parse(localStorage.getItem('pharmacyUserId'));
       this.topprogressbarline={
      title:true,
      expertise:true,
      location:true,
      budget:true,
      review:true
       }
        this.sideBarObjectt = {
      step1: true,
      step2: true,
      step3: true,
      step4: true,
      step5: false,
    };
    this.getjoblisting(this.pharmacyUser.data.referenceId);
    this.formVar = this.fb.group({
      jobName: ['', [Validators.required]],
    });

  }

  get f() {
    return this.formVar.controls;
  }
   onOptionsSelected(id) {
    // alert(id)
     var pharmacyid = id;
     var splitvalue = pharmacyid.split("_")
     this.jobnamee = splitvalue[1]
     this.jobreferenceid = splitvalue[0]
         if(this.jobreferenceid){
       this.getjobpost(this.jobreferenceid);

}
}

   getjobpost(jobreferenceid): void {
      this.JobpostService
        .getJobPost(jobreferenceid)
        .pipe(first())
        .subscribe(
          (data) => {
            if (data) {
              this.joblistarrayobj = data;                 
           }
          },
          (error) => {
            console.log('#66Error Message: ', error);
            this.errorMessage = error.text;
          }
        )
    }
 


  getjoblisting(pharmacyRefrenceId): void {
    this.JobpostService
      .getjobpostbasedonpharmacyuserid(pharmacyRefrenceId)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.jobnamelist = JSON.parse(data).data;

            this.errorMessage = '';
          }
        },
        (error) => {
          console.log('#66Error Message: ', error);
          this.errorMessage = error.text;
        }
      );
  }

  goback() {
        this.router.navigate(['pharmacy/jobpost/list']);

  }
  onSubmit() {
        this.submitted = true;
    if (this.formVar.invalid) {
      return;
    }
    var request = {
  JobVisibility: JSON.parse(this.joblistarrayobj)?.data.JobVisibility,
      additionalSkills: JSON.parse(this.joblistarrayobj)?.data?.additionalSkills,
      additionalSkillsOthers: JSON.parse(this.joblistarrayobj)?.data?.additionalSkillsOthers,
      endDateTime: JSON.parse(this.joblistarrayobj)?.data?.endDateTime,
      expertise: JSON.parse(this.joblistarrayobj)?.data?.expertise,
      expertiseOther: JSON.parse(this.joblistarrayobj)?.data?.expertiseOther,
      expertiseRetail: JSON.parse(this.joblistarrayobj)?.data?.expertiseRetail,
      fixedPrice: JSON.parse(this.joblistarrayobj)?.data?.fixedPrice,
      jobDescription: JSON.parse(this.joblistarrayobj)?.data?.jobDescription,
      jobName: JSON.parse(this.joblistarrayobj)?.data?.jobName,
      pharmacistNeeded: JSON.parse(this.joblistarrayobj)?.data?.pharmacistNeeded,
      pharmacyId: JSON.parse(this.joblistarrayobj)?.data?.pharmacyId,
      pharmacyOperations: JSON.parse(this.joblistarrayobj)?.data?.pharmacyOperations,
      screeningQuestions: JSON.parse(this.joblistarrayobj)?.data?.screeningQuestions,
      softwareSkills: JSON.parse(this.joblistarrayobj)?.data?.softwareSkills,
      startDateTime: JSON.parse(this.joblistarrayobj)?.data?.startDateTime,
      paymentType:JSON.parse(this.joblistarrayobj)?.data?.paymentType,
            status: 'draft',      
    }
    this.JobpostService.CreateJobPost(request).subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            this.jobpostId = JSON.parse(data);
            localStorage.setItem(
              'currentJobpost',
              this.jobpostId.data.referenceId
            );
             this.router.navigate(['pharmacy/jobpost/review']);
            localStorage.setItem('topprogressbarline', JSON.stringify(this.topprogressbarline));
    localStorage.setItem('sidebarstringifyobj', JSON.stringify(this.sideBarObjectt));

          } else {
            this.errorMessage = 'Unable to Save Jobpost';
            console.log('Unable to Save Jobpost');
          }
        },
        (error) => {
          console.log('Error Message: ', error.error);
          this.errorMessage = error.error;
        }
      );  }
}
