import { Component, OnInit } from '@angular/core';
import { Router, CanActivate, ActivatedRoute } from '@angular/router';
import { JobpostService } from '../jobpost.service';
import { first } from 'rxjs/operators';
import * as _ from 'lodash';
import {ToastrService} from '../../../toastr.service'
import * as moment from 'moment';
import {Hirecharge} from '../../../shared/config'

import {
  expertise,
  pharmacyOperation,
  additionalSkills,
  softwareSkills,
  jobVisibility,
  PaymentType,
} from '../../../shared/enum/constants';
@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css'],
})
export class ReviewComponent implements OnInit {
  autohide = true;
  show =false
  public joblist: any;
  public softwareSkillsss: any;
  public screeningQuestionsss: any;
  public pharmacyid: any;
  public pharmacydetails = [];
  public currentJobpostId: any;
  public errorMessage: any;
  public joblistvalue: any;
  public request: any;
  public sideBarObject: any;
  public sidebarstringifyobj: any;
  public sideBarObjectt: any;
  public nextclicksidebarobj: any;
  public joblistarrayobj: any;
  public ppharmacyid: any;
  public paymentType :any
  public constPharmacyOperations: any;
  public pharmacyneed : any
  public pharmacyyy:{};
  public jobDetails;
  public totalcost :any;
  public startdate :any;
  public enddate :any;
  public pharmacyuser :any;
  public hirechargevalue :number;
  public mintime : any
  public mins :any
  private pharmacyOperationValue;
  constructor(private router: Router,     private tosterservice:ToastrService,
private jobpostService: JobpostService) {
    this.jobpostService.sideBarObject.subscribe((sideBarObjectt) => {
      this.sideBarObject = sideBarObjectt;
      
    });
  }

  ngOnInit(): void {
    this.currentJobpostId = localStorage.getItem('currentJobpost');
    this.sideBarObjectt = JSON.parse(
      localStorage.getItem('sidebarstringifyobj')
    );
    this.sideBarObjectt = { ...this.sideBarObjectt, step5: true };
    if(this.currentJobpostId){
      this.getjobpost(this.currentJobpostId);
    }
    this.constPharmacyOperations = pharmacyOperation;
this.totalcost = JSON.parse(localStorage.getItem('totalCost'))

this.pharmacyuser = JSON.parse(localStorage.getItem('pharmacyUserId'))
if (this.pharmacyuser.data.membershipId === null || localStorage.getItem('membershipname')=== 'Pay as you go') {
  this.hirechargevalue =Hirecharge.Value
}
else {
        this.hirechargevalue =0

}

  }

  titlepage() {
    this.router.navigate(['pharmacy/jobpost/details'], {
      queryParams: { edit: true },
    });
    // localStorage.setItem('joblistvalue', this.joblistarrayobj);
  }
  detailsandexpertise() {
    this.router.navigate(['pharmacy/jobpost/expertise'], {
      queryParams: { edit: true },
    });
    // localStorage.setItem('joblistvalue', this.joblistarrayobj);
  }
  locationandvisibility() {
    this.router.navigate(['pharmacy/jobpost/location-visibility'], {
      queryParams: { edit: true },
    });
            localStorage.setItem('ppharmacyid', this.ppharmacyid);
  }
  budgetandtiming() {
    this.router.navigate(['pharmacy/jobpost/budget-timing'], {
      queryParams: { edit: true,pharmacyneed:  this.pharmacyneed },
    });
      
          localStorage.setItem('paymentvalue',this.paymentType)
  }
  postnewjob() {
    this.router.navigate(['pharmacy/jobpost']);
  }
  goback() {
    this.router.navigate(['pharmacy/jobpost/budget-timing']);
    this.sideBarObject = {
      title: true,
      details: true,
      location: true,
      budget: true,
      review: false,
    };
    this.sidebarstringifyobj = JSON.stringify(this.sideBarObject);
    localStorage.setItem('sidebarstringifyobj', this.sidebarstringifyobj);
  }

  greet():string { //the function returns a string 
    return "Hello World" 
 } 

  public getPharmacyOperations(intPharmacyOperations: any): string  {
    let valuePharmacyOperation="";
    _.find(pharmacyOperation, function(o) { 
      if(o.Value === intPharmacyOperations) {
        valuePharmacyOperation = o.key;
      }
    });
    return valuePharmacyOperation;
 }

 public getSoftwareSkill(intSoftwareSkill: any): string  {
  let valueSoftwareSkill="";
  _.find(softwareSkills, function(o) { 
    if(o.Value === intSoftwareSkill) {
      valueSoftwareSkill = o.key;
    }
  });
  return valueSoftwareSkill;
}

public getexpertise(intexpertise: any): string  {
  let valueexpertise="";
  _.find(expertise, function(o) { 
    if(o.Value === intexpertise) {
      valueexpertise = o.key;
    }
  });
  return valueexpertise;
}

public getadditionalSkills(intadditionalSkills: any): string  {
  let valueadditionalSkills="";
  _.find(additionalSkills, function(o) { 
    if(o.Value === intadditionalSkills) {
      valueadditionalSkills = o.key;
    }
  });
  return valueadditionalSkills;
}

public getjobVisibility(intjobVisibility: any): string  {
  let valuejobVisibility="";
  _.find(jobVisibility, function(o) { 
    if(o.Value === intjobVisibility) {
      valuejobVisibility = o.key;
    }
  });
  return valuejobVisibility;
}

public getPaymentType(intPaymentType: any): string  {
  let valuePaymentType="";
  _.find(PaymentType, function(o) { 
    if(o.Value === intPaymentType) {
      valuePaymentType = o.key;
    }
  });
  return valuePaymentType;
}

  getjobpost(pharmacyRefrenceId): void {
    this.jobpostService
      .getJobPost(pharmacyRefrenceId)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            var joblistarr = [];
            // this.joblistvalue = data;
            this.joblistarrayobj = data;
            this.jobDetails = JSON.parse(data).data;
            this.startdate =moment(new Date(JSON.parse(data).data.startDateTime)).utc().format("hh:mm A");
            this.enddate = moment(new Date(JSON.parse(data).data.endDateTime)).utc().format("hh:mm A")
            let starttime = moment(JSON.parse(data).data.startDateTime).utc().format('HH:mm')
            let endtime = moment(JSON.parse(data).data.endDateTime).utc().format('HH:mm')
            let timemins = moment.utc(moment(endtime, "HH:mm").diff(moment(starttime, "HH:mm"))).format("HH:mm").split(':')
           let hh=timemins[0]
            this.mintime = timemins[1] 
           this.mins =Number(hh)+ Number(this.mintime/60)
            this.ppharmacyid = JSON.parse(data)?.data?.pharmacyId;
            this.paymentType = JSON.parse(data)?.data?.paymentType
          localStorage.setItem('paymentvalue',this.paymentType)
            localStorage.setItem('ppharmacyid', this.ppharmacyid);
            this.jobDetails.jobVisibilityText = this.getjobVisibility(this.jobDetails.JobVisibility);
            this.jobDetails.pharmacyOperationsText = this.getPharmacyOperations(this.jobDetails.pharmacyOperations);
            this.jobDetails.expertiseText = this.getexpertise(this.jobDetails.expertise);
            this.jobDetails.additionalSkillsText = this.getadditionalSkills(this.jobDetails.additionalSkills);
            this.jobDetails.paymentTypeText = this.getPaymentType(this.jobDetails.paymentType);
            joblistarr.push(this.jobDetails);
            this.pharmacyneed = JSON.parse(data).data.pharmacistNeeded
            if(JSON.parse(data).data.paymentType === 10){
              this.totalcost =  JSON.parse(data).data.fixedPrice*
              this.mins +
              this.hirechargevalue            }
            else {
              this.totalcost =  JSON.parse(data).data.fixedPrice +
              this.hirechargevalue 
            }
                        
            this.pharmacyyy = JSON.parse(data).data
            this.joblist = joblistarr;
            
            this.pharmacyid = JSON.parse(data).data.pharmacyId;
            let pharmacylist = JSON.parse(localStorage.getItem('pharmacylist')).data;
            for (var i = 0; i < pharmacylist.length; i++) {
              if (this.pharmacyid === pharmacylist[i].id) {
                let pharmaydetailobjj = {
                  pharmacynaame: '',
                  pharmacyaddress: '',
                  state: '',
                  country: '',
                  city: '',
                };
                // var arr = [];
                pharmaydetailobjj.pharmacyaddress =
                  pharmacylist[i].pharmacyAddress;
                pharmaydetailobjj.pharmacynaame = pharmacylist[i].pharmacyName;
                pharmaydetailobjj.state = pharmacylist[i].state;
                pharmaydetailobjj.country = pharmacylist[i].country;
                pharmaydetailobjj.city = pharmacylist[i].city;
                this.pharmacydetails.push(pharmaydetailobjj);
                // this.pharmacydetails = arr;
              }
            }
          }
        },
        (error) => {
          console.log('#66Error Message: ', error);
          this.errorMessage = error.text;
        }
      );
  }

  savejobpost() {
    this.request = {
      JobVisibility: JSON.parse(this.joblistarrayobj)?.data.JobVisibility,
      additionalSkills: JSON.parse(this.joblistarrayobj)?.data?.additionalSkills,
      additionalSkillsOthers: JSON.parse(this.joblistarrayobj)?.data?.additionalSkillsOthers,
      endDateTime: JSON.parse(this.joblistarrayobj)?.data?.endDateTime,
      expertise: JSON.parse(this.joblistarrayobj)?.data?.expertise,
      expertiseOther: JSON.parse(this.joblistarrayobj)?.data?.expertiseOther,
      expertiseRetail: JSON.parse(this.joblistarrayobj)?.data?.expertiseRetail,
      fixedPrice: JSON.parse(this.joblistarrayobj)?.data?.fixedPrice,
      jobDescription: JSON.parse(this.joblistarrayobj)?.data?.jobDescription,
      jobName: JSON.parse(this.joblistarrayobj)?.data?.jobName,
      pharmacistNeeded: JSON.parse(this.joblistarrayobj)?.data?.pharmacistNeeded,
      pharmacyId: JSON.parse(this.joblistarrayobj)?.data?.pharmacyId,
      pharmacyOperations: JSON.parse(this.joblistarrayobj)?.data?.pharmacyOperations,
      screeningQuestions: JSON.parse(this.joblistarrayobj)?.data?.screeningQuestions,
      softwareSkills: JSON.parse(this.joblistarrayobj)?.data?.softwareSkills,
      startDateTime: JSON.parse(this.joblistarrayobj)?.data?.startDateTime,
            status: 'active',

    };
    this.jobpostService
      .updateJobPost(this.request, this.currentJobpostId)
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            var dataa = JSON.parse(data)
            // alert('Job Details Saved Successfully');
            this.jobpostService.sideBarObject.next(this.sideBarObjectt);
            this.nextclicksidebarobj = this.sideBarObject;
            // localStorage.removeItem('currentJobpost')
            this.show =true
            localStorage.setItem('sidebarstringifyobj',  JSON.stringify(this.sideBarObject));

            this.router.navigate(['pharmacy/jobpost/view/invitepharmacy'], {
              queryParams: { id: btoa(dataa.data.referenceId), jobId: btoa(dataa.data.id) },
            });
              this.tosterservice.Success('Your Jobpost Saved Successfully  ')
            // localStorage.removeItem('ppharmacyid')
            // localStorage.removeItem('totalCost')
            // localStorage.removeItem('topprogressbarline')
            // localStorage.removeItem('sidebarstringifyobj')

            //this.pharmacydata.length == 0 ||
          } else {
            this.errorMessage = 'Unable to Save Jobpost';
          }
        },
        (error) => {
          console.log('Error Message: ', error.error);
          this.errorMessage = error.error;
        }
      );
  }
}
