import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JobpostService } from '../jobpost.service';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-reviewproposal',
  templateUrl: './reviewproposal.component.html',
  styleUrls: ['./reviewproposal.component.css'],
})
export class ReviewproposalComponent implements OnInit {
  public allproposals = true;
  public shortlisted = false;
  public messege = false;
  public archive = false;
  public jobreferenceid: any;
  public jobid: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private jobpostService: JobpostService,
    config: NgbPaginationConfig
  ) {}

  ngOnInit(): void {
    var sub = this.route.queryParams.subscribe((params) => {
      this.jobreferenceid = atob(params.id);
      this.jobid = JSON.parse(atob(params.jobId));
    });
  }
  AllProposals() {
    this.allproposals = true;
    this.shortlisted = false;
    this.messege = false;
    this.archive = false;
    this.router.navigate(['pharmacy/jobpost/view/reviewproposal'], {
      queryParams: { id: btoa(this.jobreferenceid), jobId: btoa(this.jobid) },
    });
  }
  Shortlisted() {
    this.allproposals = false;
    this.shortlisted = true;
    this.messege = false;
    this.archive = false;
    this.router.navigate(
      ['pharmacy/jobpost/view/reviewproposal/Shortlisted'],
      {
        queryParams: { id: btoa(this.jobreferenceid), jobId: btoa(this.jobid) },
      }
    );
  }
  Messaged() {
    this.allproposals = false;
    this.shortlisted = false;
    this.messege = true;
    this.archive = false;
    this.router.navigate(['pharmacy/jobpost/view/reviewproposal/Messaged'], {
      queryParams: { id: btoa(this.jobreferenceid), jobId: btoa(this.jobid) },
    });
  }
  Archived() {
    this.allproposals = false;
    this.shortlisted = false;
    this.messege = false;
    this.archive = true;
    this.router.navigate(['pharmacy/jobpost/view/reviewproposal/archived'], {
      queryParams: { id: btoa(this.jobreferenceid), jobId: btoa(this.jobid) },
    });
  }
  isLinkActive(url): boolean {
    const queryParamsIndex = this.router.url.indexOf('?');
    const baseUrl =
      queryParamsIndex === -1
        ? this.router.url
        : this.router.url.slice(0, queryParamsIndex);
    return baseUrl === url;
  }
}
