import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs';
import { first } from 'rxjs/operators';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { JobpostService } from '../jobpost.service';
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from '../../../toastr.service';
import {
  pharmacyOperation,
  additionalSkills,
} from '../../../shared/enum/constants';
@Component({
  selector: 'app-saved',
  templateUrl: './saved.component.html',
  styleUrls: ['./saved.component.css']
})
export class SavedComponent implements OnInit {
page = 1;
public jobreferenceid :any
  public jobid :any
  public errorMessage :any
  public collectionsizee :any
  public jobListingg = []
  public softwareeskill = []
   public showless = false;
  public viewjob =true;
  public constAdditionalSkills :any;
  public constPharmacyOperations :any;
  public pharmacyoperationname :any;
  public additionalkeyname :any;
  public review =[]

    constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private jobpostService: JobpostService,
    
      config: NgbPaginationConfig,
       private spinner: NgxSpinnerService,
       private tosterservice: ToastrService,

  ) {
    config.size = 'sm';
    config.boundaryLinks = true;
  }

  ngOnInit(): void {
    var sub = this.route.queryParams.subscribe((params) => {
      this.jobreferenceid = atob(params.id);
      this.jobid = JSON.parse(atob(params.jobId));
    });
    this.constAdditionalSkills = additionalSkills;
    this.constPharmacyOperations = pharmacyOperation;
    this.savepharmacistList(this.jobreferenceid, this.page);
  }
  reloadDataa() {
    this.savepharmacistList(this.jobreferenceid, this.page);
  }
   viewalljobpost() {
     this.jobpostService
      .InvitedPharmacistList(this.jobreferenceid, this.page)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
             this.showless = true;
                this.viewjob = false;
            var dataaa = JSON.parse(data);
                 for (var i = 0; i < dataaa?.data?.listResult.length; i++) {
              this.softwareeskill =
                dataaa?.data?.listResult[i]?.softwareProficiency;
            }

          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
   }
    viewlessjobpost() {
    this.savepharmacistList(this.jobreferenceid, this.page);
  }
  savepharmacistList(jobreferenceid, page) {
          this.spinner.show();

    this.jobpostService
      .SavedPharmacistList(jobreferenceid, page)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.viewjob = true;
                  this.showless = false;
            var dataaa = JSON.parse(data);
            this.collectionsizee = dataaa?.data?.pagination?.totalCount;
            // this.jobListing.push(dataaa[0]);
            this.jobListingg = dataaa?.data?.listResult;
                for (var i = 0; i < dataaa?.data?.listResult.length; i++) {
                         if (dataaa?.data?.listResult[i]?.softwareProficiency.length > 2) {
                this.softwareeskill = dataaa?.data?.listResult[
                  i
                ]?.softwareProficiency.slice(0, 2);
              }
           
                }
                  this.spinner.hide();

          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }
  invitedpharmacistt(pharmacistId) {
    var request = {
      jobpostId: this.jobid,
      pharmacistId: pharmacistId,
      status: 'sent',
    };
    this.jobpostService.InvitedPharmacist(request).subscribe(
      (data) => {
        if (data) {
          this.errorMessage = '';
          // this.tosterservice.Success('You Invited The Pharmacist Successfully');
          this.savepharmacistList(this.jobreferenceid, this.page);

        } else {
          this.errorMessage = 'Unable to Save Jobpost';
        }
      },
      (error) => {
        this.errorMessage = error.error;
      }
    );
  }
  savepharmacist(pharmacistId) {
var request = {
  jobpostId: this.jobid,
  pharmacistId: pharmacistId,
  status: 'sent',
};
this.jobpostService.SavedPharmacist(request).subscribe(
  (data) => {
    if (data) {
      this.errorMessage = '';
      this.tosterservice.Success('You Saved The Pharmacist Successfully');
      this.savepharmacistList(this.jobreferenceid, this.page);
    } else {
      this.errorMessage = 'Unable to Save Jobpost';
    }
  },
  (error) => {
    this.errorMessage = error.error;
  }
);
}

updatepharmacist(pharmacistReferenceId) {
  var pharmacistrefid = pharmacistReferenceId
  var request ={
        "isDeleted": 1
  }
   this.jobpostService.Updatepharmacist(request, pharmacistrefid).subscribe(
      (data) => {
        if (data) {
          this.errorMessage = '';

this.tosterservice.Success('You Unsaved The Pharmacist Successfully');
        this.savepharmacistList(this.jobreferenceid, this.page);
        } else {
          this.errorMessage = 'Unable to update pharmacist';
        }
      },
      (error) => {
        this.errorMessage = error.error;
      }
    );
}

counter(i){
  return new Array(i);
}
toNumber(n){
  return Number(n)
    }

viewpharmacistdetails(refid){
  localStorage.setItem('jobreferenceid', this.jobreferenceid)
  localStorage.setItem('jobid',this.jobid)

  this.router.navigate(['pharmacy/jobpost/viewpharmacist'], {
    queryParams: { pharmacistRefid: btoa(refid) , savepharmacist: true } ,
  });
}
 
}
