import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs';
import { first } from 'rxjs/operators';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import {
  pharmacyOperation,
  additionalSkills,
} from '../../../shared/enum/constants';
import { JobpostService } from '../jobpost.service';
import { ToastrService } from '../../../toastr.service';
import { NgxSpinnerService } from "ngx-spinner";
import { from } from 'rxjs';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
export class SearchComponent implements OnInit {
  page = 1;
  // public show = false;
  // public showw = false;
  public collectionsize: number;
  public jobListing = [];
  public softwareskiiill = [];
  public errorMessage: any;
  public jobreferenceid: any;
  public jobid: any;
  public showless = false;
  public viewjob = true;
  public clickedid='';
  public constAdditionalSkills :any;
  public constPharmacyOperations :any;
  public pharmacyoperationname :any;
  public additionalkeyname :any;
public review =[]
public ratings:any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private jobpostService: JobpostService,
    private tosterservice: ToastrService,
            private spinner: NgxSpinnerService,
    config: NgbPaginationConfig
  ) {
    config.size = 'sm';
    config.boundaryLinks = true;
  }
  ngOnInit(): void {
    var sub = this.route.queryParams.subscribe((params) => {
      this.jobreferenceid = atob(params.id);
      this.jobid = JSON.parse(atob(params.jobId));
      // console.log(this.jobreferenceid);
    });
    this.constAdditionalSkills = additionalSkills;
    this.constPharmacyOperations = pharmacyOperation;
    this.searchPharmacist(this.jobreferenceid, this.page);
    // this.ratings = [{ name: 'Service', number: '4.5' }, { name: 'Food', number: '3.25987' }, { name: 'Speed', number: '1.25987' }];

  }


//  an other process of showing rating 
  // getStars(rating) {
  //   var val = parseFloat(rating);
  //   var size = val/5*100;
  //   return size + '%';
  // }

  reloadData() {
    console.log(this.page);
    this.searchPharmacist(this.jobreferenceid, this.page);
  }
  viewalljobpostt(pharmacistreferenceid) {
    this.clickedid = pharmacistreferenceid
    // alert(this.clickedid)
    this.viewjob = false;
    this.showless = true;
    this.jobpostService
      .SearchPharmacist(this.jobreferenceid, this.page)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            var dataaa = JSON.parse(data);
            for (var i = 0; i < dataaa?.data?.listResult.length; i++) {
              this.softwareskiiill =
                dataaa?.data?.listResult[i]?.softwareProficiency;
            }
          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }
  viewlessjobpost(pharmacistreferenceid) {
    this.clickedid = ""

    this.searchPharmacist(this.jobreferenceid, this.page);
  }
  searchPharmacist(jobreferenceidd, page): void {
                this.spinner.show();

    this.jobpostService
      .SearchPharmacist(jobreferenceidd, page)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            var dataaa = JSON.parse(data);
            this.viewjob = true;
            this.showless = false;
            this.collectionsize = dataaa?.data?.pagination?.totalCount;
            // this.jobListing.push(dataaa[0]);

            this.jobListing = dataaa?.data?.listResult;
            for (var i = 0; i < dataaa?.data?.listResult.length; i++) {
                //  alert(this.rating)
                if(dataaa?.data?.listResult[i]?.reviews !== null){
                  this.review = dataaa?.data?.listResult[i]?.reviews.split(',')

                }
                // else {
                //   this.review = dataaa?.data?.listResult[i]?.reviews.split(',')
                //   console.log(this.review)

                // }
                
              if (dataaa?.data?.listResult[i]?.softwareProficiency.length > 2) {
                this.softwareskiiill = dataaa?.data?.listResult[
                  i
                ]?.softwareProficiency.slice(0, 2);
              }
            }
        
            // console.log(this.jobListing);
                        this.spinner.hide();

          }
        },
        (error) => {
          console.log('#66Error Message: ', error);
          this.errorMessage = error.text;
        }
      );
  }

  savepharmacist(pharmacistId) {
        this.clickedid = pharmacistId

    var request = {
      jobpostId: this.jobid,
      pharmacistId: pharmacistId,
      status: 'sent',
    };
    this.jobpostService.SavedPharmacist(request).subscribe(
      (data) => {
        if (data) {
          this.errorMessage = '';
          this.tosterservice.Success('You Saved The Pharmacist Successfully');
          this.searchPharmacist(this.jobreferenceid, this.page);
        } else {
          this.errorMessage = 'Unable to Save Jobpost';
        }
      },
      (error) => {
        console.log('Error Message: ', error.error);
        this.errorMessage = error.error;
      }
    );
  }

 

  updatepharmacist(pharmacistReferenceId) {
    var pharmacistrefid = pharmacistReferenceId
    var request ={
          "isDeleted": 1
    }
     this.jobpostService.Updatepharmacist(request, pharmacistrefid).subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';

  this.tosterservice.Success('You Unsaved The Pharmacist Successfully');
          this.searchPharmacist(this.jobreferenceid, this.page);
          } else {
            this.errorMessage = 'Unable to update pharmacist';
          }
        },
        (error) => {
          console.log('Error Message: ', error.error);
          this.errorMessage = error.error;
        }
      );
  }

  invitedpharmacistt(pharmacistId) {
    // alert(pharmacistId);
    var request = {
      jobpostId: this.jobid,
      pharmacistId: pharmacistId,
      status: 'sent',
    };
    this.jobpostService.InvitedPharmacist(request).subscribe(
      (data) => {
        if (data) {
          this.errorMessage = '';
          this.tosterservice.Success('You Invited The Pharmacist Successfully');
          this.searchPharmacist(this.jobreferenceid, this.page);
        } else {
          this.errorMessage = 'Unable to Save Jobpost';
          console.log('Unable to Save Jobpost');
        }
      },
      (error) => {
        console.log('Error Message: ', error.error);
        this.errorMessage = error.error;
      }
    );
  }

  viewpharmacistdetails(refid){
    localStorage.setItem('jobreferenceid', this.jobreferenceid)
    localStorage.setItem('jobid',this.jobid)

    this.router.navigate(['pharmacy/jobpost/viewpharmacist'], {
      queryParams: { pharmacistRefid: btoa(refid),allpharmacist: true },
    });
  }

}

