import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs';
import { first } from 'rxjs/operators';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { JobpostService } from '../jobpost.service';
import { ToastrService } from '../../../toastr.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Hirecharge } from '../../../shared/config';
import { AngularStripeService } from '@fireflysemantics/angular-stripe-service';
import { AuthService } from '../../auth/auth.service';
import { PaymentService } from '../../settings/payment/payment.service';
import Swal from 'sweetalert2';
import * as moment from 'moment';

@Component({
  selector: 'app-shortlisted',
  templateUrl: './shortlisted.component.html',
  styleUrls: ['./shortlisted.component.css'],
})
export class ShortlistedComponent implements OnInit {
  stripe;
  formVar: FormGroup;
  public submitted = false;
  costhire: number;
  public finalizedDateTime: any;
  public description: any;
  public title: any;
  public page = 1;
  public modalShow = false;
  public jobreferenceid: any;
  public jobid: any;
  public proposalid: any;
  public errorMessage: any;
  public shortlistcollectionsizee: any;
  public shortlistproposallist = [];
  public pharmacyuser: any;
  public stripeKey: string;
  public paymentModesList = [];
  public paymentmethodid: any;
  public customerid: any;
  public walletamount: any;
  public contractreferanceid: any;
  public amount: any;
  public showless = false;
  public viewjob = true;
  public clickedid = '';
  public softwareskiiill = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private jobpostService: JobpostService,
    private spinner: NgxSpinnerService,
    config: NgbPaginationConfig,
    private tosterservice: ToastrService,
    private stripeService: AngularStripeService,
    private authService: AuthService,
    private paymentService: PaymentService
  ) {
    config.size = 'sm';
    config.boundaryLinks = true;
  }
  ngOnInit(): void {
    var sub = this.route.queryParams.subscribe((params) => {
      this.jobreferenceid = atob(params.id);
      this.jobid = JSON.parse(atob(params.jobId));
    });
    this.shortlistedproposals(this.page);
    this.paymentModes();
    this.formVar = this.fb.group({
      finalizedAmount: ['', Validators.required],
    });
    this.pharmacyuser = JSON.parse(localStorage.getItem('pharmacyUserId'));
    if (
      this.pharmacyuser.data.membershipId === null ||
      localStorage.getItem('membershipname') === 'Pay as you go'
    ) {
      this.costhire = Hirecharge.Value;
    } else {
      this.costhire = 0;
    }
  }
  get f() {
    return this.formVar.controls;
  }
  reloadData() {
    this.shortlistedproposals(this.page);
  }
  gettime(time){
    let date =moment(new Date(time)).utc().format("hh:mm A");
      return date
  }
  modalHideShow(proposalid, des, jobname, startDateTime) {
    // alert(proposalid);
    this.formVar.reset();
    this.submitted = false;
    this.modalShow = !this.modalShow;
    this.proposalid = proposalid;
    this.title = jobname;
    this.description = des;
    this.finalizedDateTime = startDateTime;
  }
  modalHideShoww() {
    this.modalShow = false;
  }

  onarchived(proposalReferenceId) {
    var requestarchived = {
      shortlisted: false,
      archived: true,
    };

    this.jobpostService
      .Updateproposal(requestarchived, proposalReferenceId)
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            this.shortlistedproposals(this.page);
            this.tosterservice.Success('Your Proposal Archived Successfully ');
          } else {
            this.errorMessage = 'Unable to Save Screening Questions';
          }
        },
        (error) => {
          console.log('Error Message: ', error.error);
          this.errorMessage = error.error;
        }
      );
  }

  viewalljobpostt(pharmacistreferenceid) {
    this.clickedid = pharmacistreferenceid;
    // alert(this.clickedid)
    this.viewjob = false;
    this.showless = true;
    this.spinner.show();
    var request = {
      jobpostId: this.jobreferenceid,
      filter: 'archived',
    };
    this.jobpostService
      .Listproposal(request, this.page)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            var dataaa = JSON.parse(data);
            for (var i = 0; i < dataaa?.data?.listResult.length; i++) {
              this.softwareskiiill =
                dataaa?.data?.listResult[i]?.softwareSkills;
            }
          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }
  viewlessjobpost(pharmacistreferenceid) {
    this.clickedid = '';

    this.shortlistedproposals(this.page);
  }

  shortlistedproposals(shortlistpage) {
    this.spinner.show();

    var request = {
      jobpostId: this.jobreferenceid,
      filter: 'shortlisted',
    };
    this.jobpostService.Listproposal(request, shortlistpage).subscribe(
      (data) => {
        if (data) {
          this.errorMessage = '';

          var proposal = JSON.parse(data);
          this.shortlistcollectionsizee =
            proposal?.data?.pagination?.totalCount;
          this.shortlistproposallist = proposal.data.listResult;
        } else {
          this.errorMessage = 'list proposal';
        }
        this.spinner.hide();
      },
      (error) => {
        console.log('Error Message: ', error.error);
        this.errorMessage = error.error;
      }
    );
  }
  GetStripeKey() {
    this.authService
      .GetStripeKey()
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            this.stripeKey = data.public_key;
            this.initStripe();
          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }

  initStripe() {
    this.stripeService.setPublishableKey(this.stripeKey).then((stripe) => {
      this.stripe = stripe;
    });
  }
  paymentModes() {
    this.paymentService
      .GetPaymentModes()
      .pipe(first())
      .subscribe(
        (resp) => {
          let paymentModes = JSON.parse(resp);
          this.paymentModesList = paymentModes;
          for (var i = 0; i < paymentModes.length; i++) {
            if (paymentModes[i].isdefault === 1) {
              this.paymentmethodid = paymentModes[i].customer_info.id;
              this.customerid = paymentModes[i].customer_info.customer;
            }
          }
        },
        (error) => {}
      );
  }

  handleServerResponse(response) {
    if (response.error) {
      // Show error from server on payment form
    } else if (response.requires_action) {
      // Use Stripe.js to handle required card action
      this.stripe
        .handleCardAction(response.payment_intent_client_secret)
        .then((result) => {
          if (result.error) {
            // Show error in payment form
          } else {
            // The card action has been handled
            this.paymentService
              .MakePayment({
                payment_intent_id: result.paymentIntent.id,
                idempotencyKey: response.idempotencyKey,
              })
              .pipe(first())
              .subscribe(
                (resp) => {
                  // HANDLE RESPONSE HERE
                  // SHOW MESSAGE TO USER
                },
                (error) => {
                  console.log(error);
                }
              );
          }
        });
    } else {
      // Show success message
      console.log('Success in payment processing');
    }
  }

  handleStripeJsResult(result) {}

  onSubmit() {
    this.submitted = true;
    if (this.formVar.invalid) {
      return;
    }
    var request = {
      title: this.title,
      description: this.description,
      finalizedDateTime: this.finalizedDateTime,
      finalizedAmount: this.formVar.value.finalizedAmount,
      proposalId: this.proposalid,
      status: 'pending',
      contractCharges: this.costhire,
    };
    this.jobpostService.Createcontract(request).subscribe(
      (data) => {
        if (data) {
          this.errorMessage = '';
          this.modalShow = false;
          this.contractreferanceid = JSON.parse(data)?.data?.referenceId;
          this.amount = JSON.parse(data)?.data?.contractCharges;
          if (JSON.parse(data)?.data?.contractCharges !== 0) {
            if (this.paymentmethodid === undefined) {
              // alert('please create a card details')
              Swal.fire({
                title: 'Please Select One Payment Details In Profile Setting',
                showClass: {
                  popup: 'animate__animated animate__fadeInDown',
                },
                hideClass: {
                  popup: 'animate__animated animate__fadeOutUp',
                },
              });
            } else if (this.customerid === undefined) {
              Swal.fire({
                title: 'Please Enter Your Card Details In Profile Setting Page',
                showClass: {
                  popup: 'animate__animated animate__fadeInDown',
                },
                hideClass: {
                  popup: 'animate__animated animate__fadeOutUp',
                },
              });
            } else {
              this.jobpostService
                .Calculatecontractcharges(this.contractreferanceid)
                .pipe(first())
                .subscribe(
                  (data) => {
                    if (data) {
                      this.errorMessage = '';
                      this.walletamount = JSON.parse(data)?.data?._WALLETPOINTS;
                      let paymentdetails = {
                        pharmacyUserId: this.pharmacyuser?.data?.referenceId,
                        payment_method_id: this.paymentmethodid,
                        amount: this.amount * 100,
                        transactionType: 'hirePharmacist',
                        description: '{}',
                        currency: 'USD',
                        customer: this.customerid,
                        walletAmount: this.walletamount,
                      };
                      Swal.fire({
                        title: 'Are You Sure?',
                        text: 'You Want to Proceed With This Payment!',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, Proced It!',
                      }).then((result) => {
                        if (result.isConfirmed) {
                          this.paymentService
                            .MakePayment(paymentdetails)
                            .pipe(first())
                            .subscribe(
                              (resp) => {
                                this.handleServerResponse(JSON.parse(resp));
                                Swal.fire(
                                  'Payment Successfull',
                                  'Your Payment Details Has Been Successfull.',
                                  'success'
                                );
                                // let contractreferenceid = JSON.parse(data)?.data?.referenceId
                                let request = {
                                  proposalId: this.proposalid,
                                  status: 'waitingforpharmacist',
                                };
                                this.jobpostService
                                  .Updatecontract(
                                    request,
                                    this.contractreferanceid
                                  )
                                  .subscribe(
                                    (data) => {
                                      if (data) {
                                        this.errorMessage = '';
                                        this.tosterservice.Success(
                                          'Your Contract Created Successfully'
                                        );

                                        this.shortlistedproposals(this.page);
                                      } else {
                                        this.errorMessage =
                                          'Unable to Create Contract ';
                                      }
                                    },
                                    (error) => {
                                    
                                      this.errorMessage = error.error;
                                    }
                                  );
                              },
                              (error) => {
                                console.log('error: ' + error);
                              }
                            );
                        }
                      });
                    }
                  },
                  (error) => {
                    this.errorMessage = error.text;
                  }
                );
            }
          } else {
            this.jobpostService
              .Calculatecontractcharges(this.contractreferanceid)
              .pipe(first())
              .subscribe(
                (data) => {
                  if (data) {
                    this.errorMessage = '';
                    this.walletamount = JSON.parse(data)?.data?._WALLETPOINTS;
                    let bodyrequest = {
                      pharmacyUserId: this.pharmacyuser?.data?.referenceId,
                      transactionType: 'hirePharmacist',
                      stripePaymentId: '',
                      description: '{}',
                      amount: this.amount * 100,
                      status: 'processing',
                      walletAmount: this.walletamount,
                    };
                    this.jobpostService
                      .Createpaymenttransation(bodyrequest)
                      .subscribe(
                        (data) => {
                          if (data) {
                            this.errorMessage = '';
                            // let contractreferenceid = JSON.parse(data)?.data?.referenceId
                            let request = {
                              proposalId: this.proposalid,
                              status: 'waitingforpharmacist',
                            };
                            this.jobpostService
                              .Updatecontract(request, this.contractreferanceid)
                              .subscribe(
                                (data) => {
                                  if (data) {
                                    this.errorMessage = '';
                                    this.tosterservice.Success(
                                      'Your Contract Created Successfully'
                                    );

                                    this.shortlistedproposals(this.page);
                                  } else {
                                    this.errorMessage =
                                      'Unable to Create Contract ';
                                  }
                                },
                                (error) => {
                                  console.log('Error Message: ', error.error);
                                  this.errorMessage = error.error;
                                }
                              );
                          } else {
                            this.errorMessage = 'Unable to Save transation';
                          }
                        },
                        (error) => {
                          console.log('Error Message: ', error.error);
                          this.errorMessage = error.error;
                        }
                      );
                  }
                },
                (error) => {
                  this.errorMessage = error.text;
                }
              );
          }
        } else {
          this.errorMessage = 'Unable to Save Jobpost';
        }
      },
      (error) => {
        console.log('Error Message: ', error.error);
        this.errorMessage = error.error;
      }
    );
  }
}
