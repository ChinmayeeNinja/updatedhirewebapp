import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs';
import { first } from 'rxjs/operators';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { JobpostService } from '../jobpost.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css'],
})
export class ViewComponent implements OnInit {
  Object = Object;
  formVar: FormGroup;

  page = 1;
  pagee = 1;
  pageee = 1;
  proposalpage = 1;
  shortlistpage = 1;
  archivedpage = 1;
  public disable = false;
  public viewjobpostt = true;
  public invitepharmacyy = false;
  public reviewproposall = false;
  public searchhh = false;
  public inviteddpharmacyy = false;
  public myhiree = false;
  public savedd = false;
  public allproposals = false;
  public shortlisted = false;
  public messege = false;
  public archive = false;
  public jobreferenceid: any;
  public errorMessage: any;
  public jobviewlist = [];
  public screeningQuestionsss: any;
  public softwareSkillsss: any;
  public hiree = false;
  public pharmacydetails: any;
  public pharmacyid: any;
  public pharmacylist: any;
  public collectionsize: number;
  public collectionsizee: number;
  public collectionsizeee: number;
  public proposacollectionsize: number;
  public shortlistcollectionsizee: number;
  public archivedcollectionsizeee: number;
  public jobListing = [];
  public jobListingg = [];
  public jobListinggg = [];
  public request: any;
  public jobid: any;
  // ActivatedRoute: any;
  public serchjobdetails: any;
  public proposallist = [];
  public shortlistproposallist = [];
  public archivedproposallist = [];
  public proposalid: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private jobpostService: JobpostService,
    config: NgbPaginationConfig
  ) {
    config.size = 'sm';
    config.boundaryLinks = true;
  }

  ngOnInit(): void {
    this.pharmacylist = JSON.parse(localStorage.getItem('pharmacylist'));
    var sub = this.route.queryParams.subscribe((params) => {
      this.jobreferenceid = atob(params.id);
      this.jobid = JSON.parse(atob(params.jobId));
    });
    // this.getJobPost(this.jobreferenceid);
    // this.searchPharmacist(this.jobreferenceid, this.page);

    // this.myhirepharmacist(this.jobreferenceid, this.page);
  }
  get f() {
    return this.formVar.controls;
  }

  viewjobpost() {
    this.viewjobpostt = true;
    this.invitepharmacyy = false;
    this.hiree = false;
    this.reviewproposall = false;
    this.router.navigate(['/pharmacy/jobpost/view'], {
      queryParams: { id: btoa(this.jobreferenceid), jobId: btoa(this.jobid) },
    });
  }
  invitepharmacy() {
    this.invitepharmacyy = true;
    this.viewjobpostt = false;
    this.hiree = false;
    this.searchhh = true;
    this.reviewproposall = false;
    this.router.navigate(['/pharmacy/jobpost/view/invitepharmacy'], {
      queryParams: { id: btoa(this.jobreferenceid), jobId: btoa(this.jobid) },
    });
  }
  reviewproposal() {
    this.hiree = false;
    this.viewjobpostt = false;
    this.invitepharmacyy = false;
    this.reviewproposall = true;
    this.allproposals = true;
    this.router.navigate(['/pharmacy/jobpost/view/reviewproposal'], {
      queryParams: { id: btoa(this.jobreferenceid), jobId: btoa(this.jobid) },
    });
  }
  hire() {
    this.hiree = true;
    this.viewjobpostt = false;
    this.invitepharmacyy = false;
    this.reviewproposall = false;
    this.router.navigate(['/pharmacy/jobpost/view/hire'], {
      queryParams: { id: btoa(this.jobreferenceid), jobId: btoa(this.jobid) },
    });
  }
  isLinkActive(url): boolean {
    const queryParamsIndex = this.router.url.indexOf('?');
    const baseUrl =
      queryParamsIndex === -1
        ? this.router.url
        : this.router.url.slice(0, queryParamsIndex);
    return baseUrl === url;
  }
}
