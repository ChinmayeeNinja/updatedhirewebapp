import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs';
import { first } from 'rxjs/operators';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { JobpostService } from '../jobpost.service';
import { NgxSpinnerService } from "ngx-spinner";
import * as moment from 'moment';

import {
  expertise,
  pharmacyOperation,
  additionalSkills,
  softwareSkills,
  jobVisibility,
  PaymentType,
} from '../../../shared/enum/constants';
@Component({
  selector: 'app-viewjobpost',
  templateUrl: './viewjobpost.component.html',
  styleUrls: ['./viewjobpost.component.css']
})
export class ViewjobpostComponent implements OnInit {
public errorMessage :any
public jobviewlist = [];
public pharmacyid: any;
public screeningQuestionsss: any;
public pharmacylist: any;
public softwareSkillsss: any;
public pharmacydetails =[];
  public jobreferenceid: any
  public constjobvisibility: any
  public constpharmacyoperation: any
  public constExpertise: any
  public constAdditionalSkills: any
  public constPaymentType: any
  public jobvisibilitykey: any;
  public pharmacyoperationkey :any
  public expertisekey: any
  public additionalSkillskey: any
  public paymentTypekey: any;
  public startdate :any;
  public enddate :any;

public jobid : any
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private jobpostService: JobpostService,
        private spinner: NgxSpinnerService,
      config: NgbPaginationConfig
  ) {
    config.size = 'sm';
    config.boundaryLinks = true;
  }
  ngOnInit(): void {
    this.pharmacylist = JSON.parse(localStorage.getItem('pharmacylist')).data;

    var sub = this.route.queryParams.subscribe((params) => {
        this.jobreferenceid = atob(params.id);
      this.jobid = JSON.parse(atob(params.jobId));

    });
    this.getJobPost(this.jobreferenceid);
  }
  goback() {
          this.router.navigate(['pharmacy/jobpost']);

  }
  Gotoedit(id) {
      this.router.navigate(['pharmacy/jobpost/edit']);
    localStorage.setItem(
      'currentJobpost', id);
  }
  getJobPost(jobreferenceid): void {
                this.spinner.show();

    this.jobpostService
      .getJobPost(jobreferenceid)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            this.jobviewlist.push(JSON.parse(data).data);
            this.startdate =moment(new Date(JSON.parse(data).data.startDateTime)).utc().format("hh:mm A");
            this.enddate = moment(new Date(JSON.parse(data).data.endDateTime)).utc().format("hh:mm A")
            this.pharmacyid = JSON.parse(data).data.pharmacyId,
              this.screeningQuestionsss = JSON.parse(
                data
              )?.data?.screeningQuestions;
            this.softwareSkillsss = JSON.parse(data)?.data?.softwareSkills;
            this.constjobvisibility =jobVisibility
            this.constpharmacyoperation = pharmacyOperation
            this.constExpertise = expertise
            this.constAdditionalSkills = additionalSkills
            this.constPaymentType = PaymentType
            for (var i = 0; i < this.constjobvisibility.length; i++){
              if (this.constjobvisibility[i].Value === JSON.parse(data)?.data?.JobVisibility) {
               this.jobvisibilitykey =this.constjobvisibility[i].key
              }
            }
              for (var i = 0; i < this.constpharmacyoperation.length; i++){
              if (this.constpharmacyoperation[i].Value === JSON.parse(data)?.data?.pharmacyOperations) {
               this.pharmacyoperationkey =this.constpharmacyoperation[i].key
              }
            }
            for (var i = 0; i < this.constExpertise.length; i++){
              if (this.constExpertise[i].Value === JSON.parse(data)?.data?.expertise) {
               this.expertisekey =this.constExpertise[i].key
              }
            }
            for (var i = 0; i < this.constAdditionalSkills.length; i++){
              if (this.constAdditionalSkills[i].Value === JSON.parse(data)?.data?.additionalSkills) {
               this.additionalSkillskey =this.constAdditionalSkills[i].key
              }
            }
            for (var i = 0; i < this.constPaymentType.length; i++){
              if (this.constPaymentType[i].Value === JSON.parse(data)?.data?.paymentType) {
               this.paymentTypekey = this.constPaymentType[i].key
              }
            }

            for (var i = 0; i < this.pharmacylist.length; i++) {
              if (this.pharmacyid === this.pharmacylist[i].id) {
                let pharmaydetailobjj = {
                  pharmacynaame: '',
                  pharmacyaddress: '',
                  state: '',
                  country: '',
                  city: '',
                };
                pharmaydetailobjj.pharmacyaddress = this.pharmacylist[
                  i
                ].pharmacyAddress;
                pharmaydetailobjj.pharmacynaame = this.pharmacylist[
                  i
                ].pharmacyName;
                pharmaydetailobjj.state = this.pharmacylist[i].state;
                pharmaydetailobjj.country = this.pharmacylist[i].country;
                pharmaydetailobjj.city = this.pharmacylist[i].city;
                this.pharmacydetails.push(pharmaydetailobjj);
              }
            }
            this.spinner.hide();
          }
        },
        (error) => {
          console.log('#66Error Message: ', error);
          this.errorMessage = error.text;
        }
      );
  }
}
