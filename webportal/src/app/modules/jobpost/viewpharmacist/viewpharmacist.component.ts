import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs';
import { first } from 'rxjs/operators';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { JobpostService } from '../jobpost.service';
import {ToastrService} from '../../../toastr.service'
import Swal from 'sweetalert2';
import { NgxSpinnerService } from "ngx-spinner";
import {
  pharmacyOperation,
  additionalSkills,
} from '../../../shared/enum/constants';
@Component({
  selector: 'app-viewpharmacist',
  templateUrl: './viewpharmacist.component.html',
  styleUrls: ['./viewpharmacist.component.css']
})
export class ViewpharmacistComponent implements OnInit {
 public errorMessage :any
 public pharmacistRefid :any
 public jobnametitle :any;
public invitepharmacist ='false';
public savepharmacist ='false'
public allpharmacist ='false';
public viewpharmacistdetails :any;
public softwareeskill :any;
public contract ='false'
public id :any;
public jobId :any
public showless = false;
  public viewjob =true;
  public rating =null;
  public constAdditionalSkills :any;
  public constPharmacyOperations :any;
  public pharmacyoperationname :any;
  public additionalkeyname :any;

  constructor( private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private jobpostService: JobpostService,
    config: NgbPaginationConfig,
     private spinner: NgxSpinnerService,
     ) { }

  ngOnInit(): void {
    var sub = this.route.queryParams.subscribe((params) => {
      this.pharmacistRefid = atob(params.pharmacistRefid);
                this.allpharmacist =params.allpharmacist
                this.invitepharmacist =params.invitepharmacist
                this.savepharmacist =params.savepharmacist
                this.contract = params.contract

    });

    this.constAdditionalSkills = additionalSkills;
    this.constPharmacyOperations = pharmacyOperation;

    if(this.invitepharmacist=='true'){
      this.jobnametitle='Invite Pharmacist'
    }
    else if (this.allpharmacist=='true'){
      this.jobnametitle='Listing'
    }
     else if (this.savepharmacist=='true'){
      this.jobnametitle='Save Pharmacist'
    }
    else if (this.contract=='true'){
      this.jobnametitle='Contracts'
    }
    this.viewpharmacist(this.pharmacistRefid)
    this.id = localStorage.getItem('jobreferenceid')
    this.jobId=localStorage.getItem('jobid')
  }

  viewalljobpost() {
    this.spinner.show();

    this.jobpostService
     .viewpharmcistDetails(this.pharmacistRefid)
     .pipe(first())
     .subscribe(
       (data) => {
         if (data) {
            this.showless = true;
               this.viewjob = false;
           var dataaa = JSON.parse(data);
           for (var i = 0; i < dataaa?.data?.length; i++) {
            this.softwareeskill =
              dataaa?.data[0]?.softwareProficiency;
          }
          this.spinner.hide();

         }
       },
       (error) => {
         console.log('#66Error Message: ', error);
         this.errorMessage = error.text;
       }
     );
  }
   viewlessjobpost() {
   this.viewpharmacist(this.pharmacistRefid);
 }

  viewpharmacist(pharmacistRefid) {
          this.spinner.show();
            this.jobpostService
            .viewpharmcistDetails(pharmacistRefid)
            .pipe(first())
            .subscribe(
            (data) => {
            if (data) {
        var dataaa = JSON.parse(data);
        // this.collectionsizeee = dataaa?.data?.pagination?.totalCount;
        this.viewpharmacistdetails = dataaa?.data;
                  this.spinner.hide();
                  for (var i = 0; i < dataaa?.data.length; i++) {
                    this.rating =dataaa?.data[0]?.ratings

                    if (dataaa?.data[0]?.softwareProficiency.length > 2) {
                      this.softwareeskill = dataaa?.data[0]?.softwareProficiency.slice(0, 2);
                    }
                    for(var k=0;k<this.constAdditionalSkills.length;k++){
                      if(this.constAdditionalSkills[k].Value ===dataaa?.data[0]?.additionalSkills){
                        this.additionalkeyname = this.constAdditionalSkills[k].key
                      }               
                    }
                    for(var l=0;l<this.constPharmacyOperations.length;l++){
                      if(this.constPharmacyOperations[l].Value ===dataaa?.data[0]?.pharmacyOperations){
                        this.pharmacyoperationname = this.constPharmacyOperations[l].key
      
                      }               
                    }
                      
                      }
      }
      },
      (error) => {
      console.log('#66Error Message: ', error);
      this.errorMessage = error.text;
      }
      );
      }
      counter(i){
        return new Array(i);
      }
      toNumber(n){
        return Number(parseInt(n))
          }

      backtoreviewproposal() {
        if(this.allpharmacist=='true'){
          this.router.navigate(['pharmacy/jobpost/view/invitepharmacy'], {
            queryParams: { id: btoa(this.id), jobId: btoa(this.jobId)  },
       });
          localStorage.removeItem('jobreferenceid')
              localStorage.removeItem('jobid')    
        }
        else if (this.savepharmacist == 'true') {
          this.router.navigate(['pharmacy/jobpost/view/invitepharmacy/saved'], {
               queryParams: { id: btoa(this.id), jobId: btoa(this.jobId)  },
          });
             localStorage.removeItem('jobreferenceid')
                 localStorage.removeItem('jobid')
        }
    
        else if (this.invitepharmacist == 'true') {
          this.router.navigate(['pharmacy/jobpost/view/invitepharmacy/invitedpharmacist'], {
               queryParams: { id: btoa(this.id), jobId: btoa(this.jobId)  },
          });
             localStorage.removeItem('jobreferenceid')
                 localStorage.removeItem('jobid')
        }

        else if (this.contract == 'true') {
          this.router.navigate(['pharmacy/contracts']);
        }
    
      }

}
