import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs';
import { first } from 'rxjs/operators';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { JobpostService } from '../jobpost.service';
import { AngularStripeService } from '@fireflysemantics/angular-stripe-service';
import { AuthService } from '../../auth/auth.service';
import {PaymentService} from '../../settings/payment/payment.service';
import {ToastrService} from '../../../toastr.service'
import Swal from 'sweetalert2';
import {Hirecharge} from '../../../shared/config'
import * as moment from 'moment';

@Component({
  selector: 'app-viewproposal',
  templateUrl: './viewproposal.component.html',
  styleUrls: ['./viewproposal.component.css'],
})
export class ViewproposalComponent implements OnInit {
  stripe;
  formVar: FormGroup;
      public submitted = false;
  costhire : number;
hire='false'
  public proposareferenceid: any;
  public errorMessage: any;
  public proposaldetails: any;
  public finalizedDateTime :any;
public description : any;
public title:any
  public modalShow = false;
  public proposalid: any
  public id: any;
  public jobId : any;
  public jobnametitle :any;
  public stripeKey: string;
  public paymentModesList =[];
  public paymentmethodid:any;
  public customerid:any;
  public walletamount : any;
  public contractreferanceid :any
  public pharmacyuser :any;
  public amount : any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private jobpostService: JobpostService,
    config: NgbPaginationConfig,
    private stripeService: AngularStripeService,
    private authService: AuthService,
    private paymentService: PaymentService,
    private tosterservice:ToastrService,

  ) {
    config.size = 'sm';
    config.boundaryLinks = true;
  }
  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      this.proposareferenceid = atob(params.proposalreferanseid);
      // this.proposalid = atob(params.proposalId)
      this.proposalid = JSON.parse(atob(params.proposalId));
      this.hire = params.hire
      if(this.hire=='true'){
        this.jobnametitle='Hire'
      }
      else{
        this.jobnametitle='Review Proposal'
      }

      this.pharmacyuser = JSON.parse(localStorage.getItem('pharmacyUserId'))
      if (this.pharmacyuser.data.membershipId === null ||localStorage.getItem('membershipname')=== 'Pay as you go') {
        this.costhire =Hirecharge.Value
      }
      else {
              this.costhire =0
  
      }
      this.paymentModes()

    });
    this.id = localStorage.getItem('jobreferenceid')
    this.jobId=localStorage.getItem('jobid')
    this.viewproposal(this.proposareferenceid);
      this.formVar = this.fb.group({
      finalizedAmount: ['', Validators.required],
    });
  }
   get f() {
    return this.formVar.controls;
  }
  gettime(time){
    let date =moment(new Date(time)).utc().format("hh:mm A");
      return date
  }
    Profile() {

    this.router.navigate(
      ['pharmacy/jobpost/viewproposal'],
      {
        queryParams: { proposalreferanseid: btoa(this.proposareferenceid), }
      }
    );
  }
  ShowCv() {

    this.router.navigate(
      ['pharmacy/jobpost/viewproposal/cv'],
      {
        queryParams: { proposalreferanseid: btoa(this.proposareferenceid) },
      }
    );
  }
   isLinkActive(url): boolean {
    const queryParamsIndex = this.router.url.indexOf('?');
    const baseUrl =
      queryParamsIndex === -1
        ? this.router.url
        : this.router.url.slice(0, queryParamsIndex);
    return baseUrl === url;
   }
  
    modalHideShow(proposalid,des,jobname,startDateTime) {
    // alert(proposalid);
    this.formVar.reset();
    this.submitted = false;
    this.modalShow = !this.modalShow;
     this.proposalid = this.proposalid;
    this.title = jobname 
    this.description = des
   this.finalizedDateTime=startDateTime
    }
  
    modalHideShoww() {
    this.modalShow = false;
  }
  
  viewproposal(proposareferenceid): void {
    this.jobpostService
      .Viewproposal(proposareferenceid)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.proposaldetails = JSON.parse(data).data.listResult;
          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }
  backtoreviewproposal() {
    if(this.hire=='true'){
      this.router.navigate(['pharmacy/hire']);

    }
    else{
      this.router.navigate(['pharmacy/jobpost/view/reviewproposal']);
      this.router.navigate(['pharmacy/jobpost/view/reviewproposal'], {
           queryParams: { id: btoa(this.id), jobId: btoa(this.jobId)  },
      });
         localStorage.removeItem('jobreferenceid')
             localStorage.removeItem('jobid')
    }
   

  }

  GetStripeKey() {
    this.authService
      .GetStripeKey()
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            this.stripeKey = data.public_key;
            this.initStripe();
          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }

  initStripe() {
    this.stripeService.setPublishableKey(this.stripeKey).then((stripe) => {
      this.stripe = stripe;
    });
  }

  paymentModes(){
    this.paymentService
    .GetPaymentModes()
    .pipe(first())
    .subscribe(
      (resp) => {
        let paymentModes = JSON.parse(resp)
        this.paymentModesList = paymentModes
        for(var i = 0;i < paymentModes.length;i++){
          if(paymentModes[i].isdefault===1){
            this.paymentmethodid=paymentModes[i].customer_info.id
            this.customerid = paymentModes[i].customer_info.customer
          }
        }
      },
      (error) => {}
    )

  }

  // Getcalculatecontractcharges(contractrefid) {
  //   this.jobpostService
  //     .Calculatecontractcharges(contractrefid)
  //     .pipe(first())
  //     .subscribe(
  //       (data) => {
  //         if (data) {
  //           this.errorMessage = '';
  //           this.walletamount= JSON.parse(data)?.data?._WALLETPOINTS

  //         }
  //       },
  //       (error) => {
  //         this.errorMessage = error.text;
  //       }
  //     );
  // }

  handleServerResponse(response) {
    if (response.error) {
      // Show error from server on payment form
    } else if (response.requires_action) {
      // Use Stripe.js to handle required card action
      this.stripe.handleCardAction(
        response.payment_intent_client_secret
      ).then( (result) => {
        if (result.error) {
          // Show error in payment form
        } else {
          // The card action has been handled
          this.paymentService.MakePayment({payment_intent_id : result.paymentIntent.id,idempotencyKey : response.idempotencyKey})
          .pipe(first())
          .subscribe(
            (resp) => {
              // HANDLE RESPONSE HERE
              // SHOW MESSAGE TO USER
            },
            (error) => {
              console.log(error)
            }
          )
        }
      });
    } else {
      // Show success message
    }
  }

  handleStripeJsResult(result) {
  }

  onSubmit() {
    this.submitted = true;
    if (this.formVar.invalid) {
      return;
    }
    var request = {
      title: this.title,
      description:this.description,
      finalizedDateTime: this.finalizedDateTime,
      finalizedAmount: this.formVar.value.finalizedAmount,
      proposalId: this.proposalid,
      status: 'pending',
      contractCharges:this.costhire

    };
    this.jobpostService.Createcontract(request).subscribe(
      (data) => {
        if (data) {
          this.errorMessage = '';
          this.modalShow = false;
          // this.formVar.markAsPristine();
          // this.Getcalculatecontractcharges(JSON.parse(data)?.data?.referenceId)
          this.amount = JSON.parse(data)?.data?.contractCharges
          this.contractreferanceid = JSON.parse(data)?.data?.referenceId
        if(JSON.parse(data)?.data?.contractCharges !== 0){
          if(this.paymentmethodid===undefined){
            // alert('please create a card details')
            Swal.fire({
              title: 'Please Select One Payment Details In Profile Setting',
              showClass: {
                popup: 'animate__animated animate__fadeInDown'
              },
              hideClass: {
                popup: 'animate__animated animate__fadeOutUp'
              }
            })
            }
            else if(this.customerid===undefined){
              Swal.fire({
                title: 'Please Enter Your Card Details In Profile Setting Page',
                showClass: {
                  popup: 'animate__animated animate__fadeInDown'
                },
                hideClass: {
                  popup: 'animate__animated animate__fadeOutUp'
                }
              })
            }
            else{
              this.jobpostService
              .Calculatecontractcharges(this.contractreferanceid)
              .pipe(first())
              .subscribe(
                (data) => {
                  if (data) {
                    this.errorMessage = '';
                    this.walletamount= JSON.parse(data)?.data?._WALLETPOINTS
                    let paymentdetails = {
                      pharmacyUserId : this.pharmacyuser?.data?.referenceId,
                      payment_method_id : this.paymentmethodid,
                      amount: this.amount*100, 
                      transactionType: 'hirePharmacist',
                      description: '{}',
                      currency: 'USD',
                      customer: this.customerid,
                      walletAmount:this.walletamount
                     // walletAmount: Add wallet amount you get from calculate contract charge API.
                    }
                    Swal.fire({
                      title: 'Are You Sure?',
                      text: "You Want to Proceed With This Payment!",
                      icon: 'warning',
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: 'Yes, Proceed It!'
                    }).then((result) => {
                      if (result.isConfirmed) {
                     
                        this.paymentService.MakePayment(paymentdetails)
                    .pipe(first())
                    .subscribe(
                      (resp) => {
                         this.handleServerResponse(JSON.parse(resp))
                            Swal.fire(
                          'Payment Successfull',
                          'Your Payment Details Has Been Successfull.',
                          'success'
                        )
                        // let contractreferenceid = JSON.parse(data)?.data?.referenceId
                        let request = {
                         
                          proposalId: this.proposalid,
                          status: 'waitingforpharmacist',
                        };
                        this.jobpostService
                        .Updatecontract(request, this.contractreferanceid)
                        .subscribe(
                          (data) => {
                            if (data) {
                              this.errorMessage = '';
                                this.tosterservice.Success('Your Contract Created Successfully')
                  
                                this.viewproposal(this.proposareferenceid);
                  
                            } else {
                              this.errorMessage = 'Unable to Create Contract ';
                            }
                          },
                          (error) => {
                            console.log('Error Message: ', error.error);
                            this.errorMessage = error.error;
                          }
                        );
                      },
                      (error) => {
                         console.log("error: " + error)
                      }
                    )
                      }
                    })
                  }
                },
                (error) => {
                  this.errorMessage = error.text;
                }
              );
             
            } 
        }
        else{
              
          this.jobpostService
          .Calculatecontractcharges(this.contractreferanceid)
          .pipe(first())
          .subscribe(
            (data) => {
              if (data) {
                this.errorMessage = '';
                this.walletamount= JSON.parse(data)?.data?._WALLETPOINTS
                let bodyrequest = {
                  pharmacyUserId: this.pharmacyuser?.data?.referenceId,
                  transactionType: "hirePharmacist",
                  stripePaymentId: "",
                  description: "{}",
                  amount: this.amount*100,
                  status: "processing",
                  walletAmount: this.walletamount
                };
                this.jobpostService.Createpaymenttransation(bodyrequest).subscribe(
                  (data) => {
                    if (data) {
                      this.errorMessage = '';
                      // let contractreferenceid = JSON.parse(data)?.data?.referenceId
                      let request = {
                     
                        proposalId: this.proposalid,
                        status: 'waitingforpharmacist',
                      };
                      this.jobpostService
                      .Updatecontract(request, this.contractreferanceid)
                      .subscribe(
                        (data) => {
                          if (data) {
                            this.errorMessage = '';
                              this.tosterservice.Success('Your Contract Created Successfully')
                
                              this.viewproposal(this.proposareferenceid);
                
                          } else {
                            this.errorMessage = 'Unable to Create Contract ';
                          }
                        },
                        (error) => {
                          console.log('Error Message: ', error.error);
                          this.errorMessage = error.error;
                        }
                      );
                    } else {
                      this.errorMessage = 'Unable to Save transation';
                    }
                  },
                  (error) => {
                    console.log('Error Message: ', error.error);
                    this.errorMessage = error.error;
                  }
                );
              }
            },
            (error) => {
              this.errorMessage = error.text;
            }
          );

       
       
        }

        } else {
          this.errorMessage = 'Unable to Save Jobpost';
          console.log('Unable to Save Jobpost');
        }
      },
      (error) => {
        console.log('Error Message: ', error.error);
        this.errorMessage = error.error;
      }
    );
  }

}
