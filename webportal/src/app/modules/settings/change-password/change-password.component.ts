import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { AuthService } from '../../auth/auth.service';
import { first } from 'rxjs/operators';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from '../../../toastr.service'

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
     formVar: FormGroup;
       show = false;

  public submitted = false
  public authid: any
  public errorMessage :any
   constructor(
    private authService: AuthService,
  private route: ActivatedRoute,
    private router: Router,    private fb: FormBuilder,
    config: NgbPaginationConfig,
    private tosterservice:ToastrService,

  ) { }

  ngOnInit(): void {
   this.authid = localStorage.getItem('signupUserReferenceId');
       this.formVar = this.fb.group({
      email: ['', [Validators.required, Validators.pattern(/^([a-zA-Z]{1,})+([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/)]],
         oldPassword: ['', Validators.required],
            newPassword: ['', Validators.required],

    });
  }
   get f() {
    return this.formVar.controls;
   }
  
  onSubmit() {
    this.submitted = true
     if (this.formVar.invalid) {
      return;
     }
    this.authService
      .changepassword(this.formVar.value, this.authid)
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = ''
              this.show = true;
                  this.router.navigate(['/pharmacy/settings/UserProfile']);

                 

          } else {
            this.errorMessage = 'Unable to  change password';
            // console.log('Unable to  change password');
          }
        },
        (error) => {
          this.tosterservice.Error(error)

          this.errorMessage = error;

        }
      );
    
  }
  
  goback(){
    this.router.navigate(['/pharmacy/settings/UserProfile']);

  }

}
