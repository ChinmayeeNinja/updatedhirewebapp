import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../auth/auth.service';
import { first } from 'rxjs/operators';
// import { OwlOptions } from 'ngx-owl-carousel-o';
import { ToastrService } from '../../../toastr.service'
import { NgxSpinnerService } from "ngx-spinner";

declare var $: any;
@Component({
  selector: 'app-membership',
  templateUrl: './membership.component.html',
  styleUrls: ['./membership.component.css'],
})
export class MembershipComponent implements OnInit {
  public flag: number;
  number = 5
  public membeshipid = 'false';
  public errorMessage: any;
  public pharmacylistt: any;
  public state: any;
  public card = [];
  public request: any;
  public membershipidvalue: any;
  public pharmacyuserdetails: any
  public payasyougomembeshipid: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private tosterservice: ToastrService,
    private spinner: NgxSpinnerService

  ) { }

  ngOnInit(): void {
    var sub = this.route.queryParams.subscribe((params) => {
      this.membeshipid = params.membershipid;
      this.pharmacyuserdetails = JSON.parse(localStorage.getItem('pharmacyUserId'))
    });


    this.pharmacylistt = JSON.parse(localStorage.getItem('pharmacylist'))?.data;
    for (var i = 0; i < this.pharmacylistt.length; i++) {
      this.state = this.pharmacylistt[i].state;
    }
    this.Getmembershipbystate(this.state);
  }


  customOptions: any = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: false,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 4
      }
    },
    nav: true
  }

  Getmembershipbystate(state): void {
    this.spinner.show()
    this.authService
      .Getmembershipbystate(state)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            this.card = data.data;
            this.payasyougomembeshipid = data?.data[0]?.membershipId
            this.spinner.hide()
            if (this.pharmacyuserdetails.data.membershipId === null) {
              var dd = this.pharmacyuserdetails.data.referenceId;
              let requestbody = {
                membershipId: this.payasyougomembeshipid,
              };
              this.authService.updateMembershipid(requestbody, dd).subscribe(
                (data) => {
                  if (data) {
                    this.errorMessage = '';
                    var dataa = JSON.parse(data)
                    this.tosterservice.Success('You Selected a Subscription Card Successfully')
                    this.checkPharmacyUserExists();
                  } else {
                    this.errorMessage = 'Unable to Save Subscription';
                  }
                },
                (error) => {
                  this.errorMessage = error.error;
                }
              );
            }
          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }

  skip() {
    this.router.navigateByUrl('profile');
  }
  Continue() {
    this.router.navigateByUrl('payment');
  }
  logout() {
    localStorage.removeItem('pharmacyUser');
    localStorage.removeItem('currentUser');
    localStorage.removeItem('signupUserReferenceId');
    localStorage.removeItem('accessToken');
    localStorage.removeItem('pharmacyUserId');
    localStorage.removeItem('pharmacylist');

    this.router.navigate(['/login']);
  }
  gotonextpage() {
    this.router.navigate(['/pharmacy/settings/UserProfile']);

  }

  checkPharmacyUserExists(): void {
    this.authService
      .GetPharmacyUserByAuthId()
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            this.pharmacyuserdetails = data
            localStorage.setItem('pharmacyUserId', JSON.stringify(data));
            this.authService
              .Getmembershipbystate(this.state)
              .pipe(first())
              .subscribe(
                (data) => {
                  if (data) {
                    this.errorMessage = '';
                    this.card = data.data;
                    this.payasyougomembeshipid = data?.data[0]?.membershipId
                    // console.log(this.card) 
                    for (var i = 0; i < this.card.length; i++) {
                      if (this.card[i].membershipId === this.pharmacyuserdetails.data.membershipId) {
                        localStorage.setItem("membershipname", this.card[i].name)
                      }
                    }
                    // console.log(this.pharmacyuserdetails)  
                  }
                },
                (error) => {
                  this.errorMessage = error.text;
                }
              );
          }
          return data;
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }

  openpaymentcard(membershipid) {
    // alert(membershipid);
    // alert(referenceIdd);
    var dd = this.pharmacyuserdetails.data.referenceId;
    this.request = {
      membershipId: membershipid,
    };
    if (this.membeshipid === 'true') {
      this.authService.updateMembershipid(this.request, dd).subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            this.tosterservice.Success('You Selected a Subscription Card Successfully')
            this.checkPharmacyUserExists();

            //this.pharmacydata.length == 0 ||
          } else {
            this.errorMessage = 'Unable to Save Subscription';
          }
        },
        (error) => {
          this.errorMessage = error.error;
        }
      );
    }
    else {
      this.authService.updateMembershipid(this.request, dd).subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            var dataa = JSON.parse(data)
            this.checkPharmacyUserExists();

            // this.router.navigate(['/payment']);
            this.tosterservice.Success('You Selected a Subscription Card Successfully')

            //this.pharmacydata.length == 0 ||
          } else {
            this.errorMessage = 'Unable to Save Subscription';
          }
        },
        (error) => {
          this.errorMessage = error.error;
        }
      );
    }
  }
}
