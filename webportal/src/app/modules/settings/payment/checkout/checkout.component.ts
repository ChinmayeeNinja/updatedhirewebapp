import {
  Component,
  OnInit,
  AfterViewInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../../auth/auth.service';
import { first } from 'rxjs/operators';
import { NgForm } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {PaymentService} from '../payment.service';
import {ToastrService} from '../../../../toastr.service'

import { AngularStripeService } from '@fireflysemantics/angular-stripe-service';
@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css'],
})
export class CheckoutComponent implements OnInit {
  @ViewChild('cardInfo', { static: false }) cardInfo: ElementRef;

  stripe;
  loading = false;
  confirmation;
    formVar: FormGroup;    

  public errorMessage: any;
  public stiprkey: string;
  card: any;
  public request: any;
  public tokenn: any;
  cardHandler = this.onChange.bind(this);
  error: string;
  public userdetail :any
public selectpaymentmethod :any
  modalShow=false
  public paymentModess :any
public paymentModelist = [];
public button =true;
public loadingbutton = false;
  constructor(
    private cd: ChangeDetectorRef,
    private stripeService: AngularStripeService,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,
    private paymentService: PaymentService,
    private tosterservice:ToastrService,

  ) {}
  ngOnInit(): void {
    this.userdetail = JSON.parse(localStorage.getItem('currentUser'))
    this.GetStripeKeyy();
    this.paymentModes()

      this.formVar = this.fb.group({
      paymentmethod: [''],
    });
    this.modalHideShoww()

  }
  modalHideShoww() {
    this.modalShow = !this.modalShow;

  }
  modalHide() {
        this.modalShow=false

  }

  GetStripeKeyy() {
    this.authService
      .GetStripeKey()
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            this.stiprkey = data.public_key;
            this.init();
          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }

  init() {
    this.stripeService.setPublishableKey(this.stiprkey).then((stripe) => {
      this.stripe = stripe;
      const elements = stripe.elements();
      this.card = elements.create('card', { hidePostalCode: true },{ name:true });
      this.card.mount(this.cardInfo.nativeElement);
      this.card.addEventListener('change', this.cardHandler);
    });
    this.getCustomer()
  }

  ngOnDestroy() {
    this.card.removeEventListener('change', this.cardHandler);
    this.card.destroy();
  }

  onChange({ error }) {
    if (error) {
      this.error = error.message;
    } else {
      this.error = null;
    }
    this.cd.detectChanges();
  }

  async onSubmit(form: NgForm) {
    this.button = false;
    this.loadingbutton=true;
    const { token, error } = await this.stripe.createToken(this.card);
    if (error) {
      console.log('Error:', error);
    } else {
      console.log('Success!', token);
       if(this.paymentModelist.length > 9){
        this.button = true;
        this.loadingbutton=false;
        this.tosterservice.Error("Maximum You Can Use 10 Card Details.If You Want Some Other Card Details Then Fast You Remove Previous Card Details From Your Card List")
      }
        else if(this.paymentModelist.filter((item,i)=>item.customer_info?.card?.last4 === token.card.last4 && item.customer_info?.card?.exp_month === token.card.exp_month && item.customer_info?.card?.exp_year === token.card.exp_year ).length>0){
            this.button = true;
            this.loadingbutton=false;
            this.tosterservice.Error("Card Alredy Exist.Please Enter Different Card Details")
          }
          
          else{
            // this.button = true;
            this.createcustomerinfo(token.id);
          }
    }
  }

  createcustomerinfo(token) {
    var name=this.userdetail.data.firstname +' ' + this.userdetail.data.lastname
    this.request = {
      tokenid: token,
      email: this.userdetail.data.email,
      name: name,
            paymentType:'CC',

      metadata: { refid: 'sdsddsds' },
    };
    this.authService
      .Createcustomer(this.request)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.stripe.createPaymentMethod({
              type: 'card',
              card: this.card,
            })
            .then(
               (result) => {
                
                this.getCustomer()
                  this.attachPaymentMethod(result);
               },
               (err) => {
                  //console.log(err);
                  this.errorMessage = "Failed to create payment method."
               }
        
            );
            // this.router.navigate(['pharmacy/dashboard']);
          } else {
            console.log('Unable to create customer');
          }
        },
        (error) => {
          console.log('#66Error Message: ', error);
          // this.errorMessage = error.text;
        }
      );
  }

  attachPaymentMethod(token){
      
    let stripecustomerid = atob(localStorage.getItem("stripecustomerId"))
    this.paymentService
      .PaymentMethodCC({
        paymentMethod: token.paymentMethod.id,
        stripecustomerid: stripecustomerid
      })
      .pipe(first())
      .subscribe(

        (response) => {
            //console.log(response)
             let data = JSON.parse(response)

             if(data.error == false)
             {
              this.button = true;
                this.loadingbutton=false;
              this.paymentModes()

                this.errorMessage = "Payment Method added successfully."
                this.tosterservice.Success('Card Details Added Successfully')
                 this.router.navigate(['/pharmacy/settings/paymentviewcard']);

             }
             else
             {  
              this.errorMessage = "Error in adding Payment Method."
             }


        },
        (error) => {
            //console.log(error) 
            this.errorMessage = "Error in adding Payment Method."  
        }

      );
  
  }

  getCustomer(){
    //console.log(" getcustomer")
    this.paymentService
    .GetCustomer()
    .pipe(first())
    .subscribe(
          (resp) =>{
              
              let paymentCustomerInfo = JSON.parse(resp)
              if(paymentCustomerInfo.data == null)
               {
                  //console.log(atob(localStorage.getItem("email")))
                  // create customer
                  // this.createCustomer()
               }
               else
               {
                  localStorage.setItem("stripecustomerId",btoa(paymentCustomerInfo.data.stripecustomerId))
               }

          },
          (error) => {
              console.log('Error getCustomer: ', error);
          });
  }

  paymentModes(){
    this.paymentService
    .GetPaymentModes()
    .pipe(first())
    .subscribe(
      (resp) => {
        this.paymentModelist = JSON.parse(resp)
     
       
      },
      (error) => {}
    )

  }

}
