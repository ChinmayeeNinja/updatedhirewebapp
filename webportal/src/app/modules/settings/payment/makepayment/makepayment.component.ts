import {
  Component,
  OnInit,
  AfterViewInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../../auth/auth.service';
import { first } from 'rxjs/operators';
import { NgForm } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {PaymentService} from '../payment.service';

import { AngularStripeService } from '@fireflysemantics/angular-stripe-service';
import {Paymentmethod} from '../../../../shared/enum/constants'
@Component({
  selector: 'app-makepayment',
  templateUrl: './makepayment.component.html',
  styleUrls: ['./makepayment.component.css']
})
export class MakepaymentComponent implements OnInit {
  public selectpaymentmethod = Paymentmethod
  public Paymentmethod = Paymentmethod
    public title ='Enter Your Card Details'
    public paymentModesList = []
    formVar: FormGroup;
  constructor(
    private cd: ChangeDetectorRef,
    private stripeService: AngularStripeService,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,
    private paymentService: PaymentService,
  ) {
    localStorage.setItem('Paymentmethodvalue',this.Paymentmethod[0].Value)
    this.formVar = this.fb.group({
      Paymentmethod: ['']})
    this.formVar.controls.Paymentmethod.patchValue(
      Paymentmethod[
        Paymentmethod.findIndex(
                  (x) =>x.Value === localStorage.getItem('Paymentmethodvalue')
                )
                ].Value
              );
   }
  ngOnInit(): void {
  }
  backtoviewcard(){
    this.router.navigate(['/pharmacy/settings/paymentviewcard']);

  }
  opencc(value) {
    if (value == 'Credit/Debit') {
      this.router.navigate(['/pharmacy/settings/payment']);
      this.title = 'Enter Your Card Details'
    }
    if (value =='Connect Bank') {
            this.router.navigate(['/pharmacy/settings/payment/paymentach']);
      this.title = 'Enter Your Account Details'
    }
  }  
}
