import {
  Component,
  OnInit,
  AfterViewInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../../auth/auth.service';
import { first } from 'rxjs/operators';
import { NgForm } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {PaymentService} from '../payment.service';
import Swal from 'sweetalert2';
import {ToastrService} from '../../../../toastr.service'
import { AngularStripeService } from '@fireflysemantics/angular-stripe-service';
import {Paymentmethod} from '../../../../shared/enum/constants'
@Component({
  selector: 'app-paymentview',
  templateUrl: './paymentview.component.html',
  styleUrls: ['./paymentview.component.css']
})
export class PaymentviewComponent implements OnInit {
public selectpaymentmethod = Paymentmethod
public Paymentmethod = Paymentmethod
  public title ='Enter Your Card Details'
  public paymentModesList = []
  public errorMessage :any;
  public custmerid:any
  formVar: FormGroup;
  public radioSelected: any
  constructor(
    private cd: ChangeDetectorRef,
    private stripeService: AngularStripeService,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,
    private paymentService: PaymentService,
    private tosterservice:ToastrService

  ) {
    this.formVar = this.fb.group({
      paymentModesList: ['', [Validators.required]],
         });

   }
  ngOnInit(): void {

    this.paymentModes()

  }
  gotocreatepayment(){
    this.router.navigate(['/pharmacy/settings/payment']);

  }

  

  getCustomer(){
    //console.log(" getcustomer")
    this.paymentService
    .GetCustomer()
    .pipe(first())
    .subscribe(
          (resp) =>{
              
              let paymentCustomerInfo = JSON.parse(resp)
              this.custmerid = paymentCustomerInfo.data.stripecustomerId
              // if(paymentCustomerInfo.data == null)
              //  {
                  
              //     this.createCustomer()
              //  }
              //  else
              //  {
              //     localStorage.setItem("stripecustomerId",btoa(paymentCustomerInfo.data.stripecustomerId))
              //  }

          },
          (error) => {
              console.log('Error getCustomer: ', error);
          });
  }

  // createCustomer(){

  //   this.paymentService
  //   .Createcustomer({email:atob(localStorage.getItem("email")),"name":localStorage.getItem("firstname") + " " + localStorage.getItem("lastname")})
  //   .pipe(first())
  //   .subscribe(
  //         (resp) =>{
              
  //             let paymentCustomerInfo = JSON.parse(resp)
  //             if(paymentCustomerInfo.data == null)
  //              {
                
  //                 localStorage.setItem("stripecustomerId",btoa(paymentCustomerInfo.data.stripecustomerId))
                  
  //              }

  //         },
  //         (error) => { 
  //             console.log('Error create customer: ', error);
  //         });
  // }
  updatecardkey(referenceId){
    var request = {
      customerId:this.custmerid
    }
    this.paymentService.updatatedefault(request, referenceId).subscribe(
      (data) => {
        if (data) {
          this.paymentModes()
          this.tosterservice.Success('Card Is Saved As Default Payment Method For Any Payment Transation')

        } else {
          this.errorMessage = 'Unable to Save Jobpost';
        }
      },
      (error) => {
        console.log('Error Message: ', error.error);
        this.errorMessage = error.error;
      }
    );
  }
  paymentModes(){
    this.paymentService
    .GetPaymentModes()
    .pipe(first())
    .subscribe(
      (resp) => {
        let paymentModes = JSON.parse(resp)
        this.paymentModesList = paymentModes
        if(this.paymentModesList.length>0){
          this.getCustomer()
       for(var i=0;i<this.paymentModesList.length;i++){
         if(this.paymentModesList[i].isdefault === 1){
          this.formVar.controls.paymentModesList.patchValue(
            this.paymentModesList[
              this.paymentModesList.findIndex(
              (x) => x.isdefault === 1
            )
            ].referenceId
          );
         }
       }
        }
    
       
      },
      (error) => {}
    )

  }

  Deletecard(referenceId){
    var request = {
      isDeleted:1
    }
    Swal.fire({
      title: 'Are you sure?',
      text: "You want to delete this Account Details!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Deleted!',
          'Your account details has been deleted.',
          'success'
        )
        this.paymentService.updatepayment(request, referenceId).subscribe(
          (data) => {
            if (data) {
              this.paymentModes()
            } else {
              this.errorMessage = 'Unable to Save account';
            }
          },
          (error) => {
            console.log('Error Message: ', error.error);
            this.errorMessage = error.error;
          }
        );
      }
    })
    
  }

}
