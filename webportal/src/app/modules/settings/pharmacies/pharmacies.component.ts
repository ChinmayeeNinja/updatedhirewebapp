import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { JobpostService } from '../../jobpost/jobpost.service'
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../../auth/auth.service';
import { SettingsService } from '../../settings/settings.service'
import Swal from 'sweetalert2';

@Component({
  selector: 'app-pharmacies',
  templateUrl: './pharmacies.component.html',
  styleUrls: ['./pharmacies.component.css']
})
export class PharmaciesComponent implements OnInit {
public pharmacyUserDetailsLocalStorage = []
public errorMessage :any;
  constructor(
    private jobpost: JobpostService,
    private router: Router,
    private authService: AuthService,
    private setting: SettingsService,

    config: NgbPaginationConfig
  ) {
    config.size = 'sm';
    config.boundaryLinks = true;
  }
  ngOnInit(): void {

 this.getPharmacyListByAuthId()
  }

  edit(referenceId){
    this.router.navigate(['/pharmacy/settings/profile'], {
      queryParams: { id: btoa(referenceId),edit:true },
    });

  }
  delete(referenceId){
    var request = {
      isDeleted: 1
    }
    Swal.fire({
      title: 'Are you sure?',
      text: "You Want To Delete This Pharmacies!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
    
        this.setting
    .updatepharmacy(request,referenceId)
    .pipe(first())
    .subscribe(
      (data) => {
        if (data) {
          this.errorMessage = '';
          Swal.fire(
            'Deleted!',
            'Your Pharmacies Has Been Deleted.',
            'success'
          )
          this.getPharmacyListByAuthId()

        } else {
          console.log('Unable to delete Pharmacies');
        }
      },
      (error) => {
        this.errorMessage = error.text;
      }
    );
      }
    })
 

  }

  getPharmacyListByAuthId(): void {
    this.authService
      .GetPharmacyListByAuthId()
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            this.pharmacyUserDetailsLocalStorage=data.data
            localStorage.setItem('pharmacylist', JSON.stringify(data));
          }
          if (data.length == 0) {
            this.router.navigateByUrl('profile');
          } else {
            // this.router.navigateByUrl('dashboard');
          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }
}
