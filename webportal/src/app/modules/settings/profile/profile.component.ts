import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthService } from '../../auth/auth.service';
import * as _ from 'lodash';
import {ToastrService} from '../../../toastr.service'
import { SettingsService } from '../../settings/settings.service'
import { StatesService } from '../../../shared/states-cities/states.service';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

 public states: any;
  public cities: any;
  // public show = false
  public submitted = false;
  public errorMessage: any;
  public stateCities: any;
  public loading = true;
  public pharmacyUserLocalStorage: any;
  public authenticationId: any;
public pharmacyidd :any;
public Edit = 'false'
public pharmacyreferenceid:any;
public pharmacylist =[];
  options = {
    componentRestrictions: {
      country: ['US'],
    },
  };
  componentForm = {
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'long_name',
    country: 'long_name',
    postal_code: 'short_name',
  };
  formVar: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private stateService: StatesService,
    private router: Router,
        private tosterservice:ToastrService,
        private setting: SettingsService,
        private route: ActivatedRoute,

  ) {
    this.getUSStates();
  }

  ngOnInit(): void {
    this.authenticationId = localStorage.getItem('signupUserReferenceId');
    this.pharmacyUserLocalStorage = localStorage.getItem('pharmacyUserId');
    var dd = this.pharmacyUserLocalStorage;
    this.pharmacyidd = JSON.parse(dd);
    var sub = this.route.queryParams.subscribe((params) => {
      this.Edit = params.edit;
      if (this.Edit  === 'true'){
        this.pharmacyreferenceid = atob(params.id);

      }

  });
    this.formVar = this.fb.group({
      pharmacyName: ['', Validators.required],
      phoneNumber: ['', [Validators.required,Validators.pattern(/^\(?[+. ]?([0-1])?([ ])?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/)]],
      pharmacyAddress: ['', Validators.required],
      state: ['', Validators.required],
      city: ['', Validators.required],
      country: ['', Validators.required],
      postalCode: ['', Validators.required],
      licenseNumber: ['', Validators.required],
      pharmacyUserId: this.pharmacyidd.data.id,
    });
    if(this.pharmacyreferenceid ){
      this.getPharmacyListbyrefid(this.pharmacyreferenceid)

    }
    this.getPharmacyListByAuthId()
  }
  get f() {
    return this.formVar.controls;
  }

    onlyNumber(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;

    }
      
    return true;
  }
  onlyText(event): boolean {
     const charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode >= 48 && charCode <= 57 )&& (charCode >= 96 || charCode <= 105)) {
      return false;

    }
        return true;

  }
  getUSStates() {
    this.stateService.getUSStates().subscribe(
      (data) => {
        this.stateCities = JSON.parse(JSON.stringify(data));
        this.states = _.uniqBy(this.stateCities, 'state');
        this.states = _.orderBy(this.states, ['state'], ['asc']);
      },
      (error) => {
        this.errorMessage = error.message;
      }
    );
  }

  getCityByState(state) {
    // console.log(state);
    this.cities = _.filter(this.stateCities, { state: state });
    // console.log('City', this.cities);
  }
 addpharmacy() {
   if (this.formVar.value.city !== '' && this.formVar.value.country !== '' && this.formVar.value.licenseNumber !== '' && this.formVar.value.pharmacyAddress !== '' &&
    this.formVar.value.pharmacyName !== '' &&this.formVar.value.phoneNumber !== '' &&this.formVar.value.state !== '' &&this.formVar.value.postalCode !== '') {
           window.location.reload()

    }
  }

  // continue() {
  //   this.router.navigate(['/pharmacy/settings/payment']);

  // }
  // goback() {
  //                     this.router.navigate(['pharmacy/settings/UserProfile']);

  // }  
  getPharmacyListbyrefid(pharmacyreferenceid): void {
    this.setting
      .getharmacydetailbypharmacyid(pharmacyreferenceid)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            if(this.pharmacyreferenceid===null){
              this.formVar.patchValue({
                pharmacyName: this.formVar.value.pharmacyName,
                phoneNumber: this.formVar.value.phoneNumber,
                pharmacyAddress:this.formVar.value.pharmacyAddress,
                state: this.formVar.value.state,
                city: this.formVar.value.city,
                country:this.formVar.value.country,
                postalCode: this.formVar.value.postalCode,
                licenseNumber: this.formVar.value.licenseNumber,
                pharmacyUserId: this.pharmacyidd.data.id,
                  });
             }
             else{
              this.formVar.patchValue({
                pharmacyName: data?.data?.pharmacyName,
                phoneNumber: data?.data?.phoneNumber,
                pharmacyAddress: data?.data?.pharmacyAddress,
                state: data?.data?.state,
                // city: data?.data?.city,
                country: data?.data?.country,
                postalCode: data?.data?.postalCode,
                licenseNumber:data?.data?.licenseNumber,
                pharmacyUserId: this.pharmacyidd.data.id,
                
              });
              this.getCityByState(data?.data?.state);

              setTimeout(() => {
                this.formVar.patchValue({
                  city: data?.data?.city,
                })}, 500);
             }
          }
         
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }

  onSubmit() {
    this.submitted = true;

    if (this.formVar.invalid) {
      return;
    }

    if (this.submitted) {
   if(this.Edit  === 'true'){
    this.setting
    .updatepharmacy(this.formVar.value,this.pharmacyreferenceid)
    .pipe(first())
    .subscribe(
      (data) => {
        if (data) {
          this.errorMessage = '';
       this.tosterservice.Success('Pharmacy Details update Successfully')
          this.getPharmacyListByAuthId();
              this.router.navigateByUrl('pharmacy/settings/Pharmacies');         
        } else {
          this.loading = false;
          //this.router.navigate(['/code']);
        }
      },
      (error) => {
        this.errorMessage = error.text;
      }
    );
   }
   else{
     if(this.pharmacylist.filter((item,i)=>item.phoneNumber === this.formVar.value.phoneNumber || item.licenseNumber === this.formVar.value.licenseNumber).length>0){
      this.tosterservice.Error("Pharmacy Alredy Exist.Please Enter Different Pharmacy Details")
    }
    else{
      this.authService
      .CreatePharmacy({
        ...this.formVar.value,
      })
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
              this.tosterservice.Success('Pharmacy Details Saved Successfully')
                        this.getPharmacyListByAuthId();
                            if (this.pharmacyidd.data.membershipId===null) {
                            this.router.navigate(['/pharmacy/settings/membership']);
              }
              else {
                            this.router.navigateByUrl('pharmacy/settings/UserProfile');
  
              }
            
          } else {
            this.loading = false;
            //this.router.navigate(['/code']);
          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
    }
   }
    }
  }

  getPharmacyListByAuthId(): void {
    this.authService
      .GetPharmacyListByAuthId()
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            localStorage.setItem('pharmacylist', JSON.stringify(data));
            this.pharmacylist = data.data
          }
          if (data.length == 0) {
            this.router.navigateByUrl('profile');
          } else {
            // this.router.navigateByUrl('dashboard');
          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }

  public handleAddressChange(address: any) {
    // console.log("lat == >",address.geometry.location.lat())
    // console.log("lan ==>",address.geometry.location.lng())

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        const geolocation = {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        };
        // const circle = new google.maps.Circle({
        //   center: geolocation,
        //   radius: position.coords.accuracy,
        // });
      });
    }
    let city = '';
    this.f.phoneNumber.setValue(address.international_phone_number);

    this.f.pharmacyName.setValue(address.name);
    this.f.pharmacyAddress.setValue(address.name);
    this.f.pharmacyAddress.setValue(address.formatted_address);

    for (const component of address.address_components) {
      const addressType = component.types[0];

      if (this.componentForm[addressType]) {
        const val = component[this.componentForm[addressType]];
        if (addressType === 'postal_code') {
          this.f.postalCode.setValue(val);
        }
        if (addressType === 'country') {
          this.f.country.setValue(val);
        }
        if (addressType === 'administrative_area_level_1') {
          this.f.state.setValue(val);
          this.getCityByState(val);
        }
        if (addressType === 'locality') {
          city = val;
        }
      }
    }
  

    setTimeout(() => {
      this.f.city.setValue(city);
    }, 200);
  }

}
