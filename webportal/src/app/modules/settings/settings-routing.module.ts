import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SettingComponent } from './viewsetting/setting.component'
import { UserprofileComponent } from './userprofile/userprofile.component'
import { PharmaciesComponent } from './pharmacies/pharmacies.component'
import { ProfileComponent } from './profile/profile.component';
import { MembershipComponent } from './membership/membership.component';
import { PaymentviewComponent } from './payment/paymentview/paymentview.component';
import { CheckoutComponent } from './payment/checkout/checkout.component';
import { CheckoutachComponent } from './payment/checkoutach/checkoutach.component';
import { MakepaymentComponent } from './payment/makepayment/makepayment.component';
import {ChangePasswordComponent} from './change-password/change-password.component'

import { from } from 'rxjs';
const routes: Routes = [
  {
    path: '',
    component: SettingComponent,
    children: [
      { path: 'UserProfile', component: UserprofileComponent },
  
      { path: 'Pharmacies', component: PharmaciesComponent },
    ],
    
  },
        {path:'profile',component:ProfileComponent},
  { path: 'membership', component: MembershipComponent },
  {
    path: 'changepassword',
    component :ChangePasswordComponent
  },
  { path: 'paymentviewcard', component: PaymentviewComponent },
          {
    path: 'payment',
    component: MakepaymentComponent,
    children: [
      { path: '', component: CheckoutComponent },
  
      { path: 'paymentach', component: CheckoutachComponent },
    ],
    
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SettingsRoutingModule { }
