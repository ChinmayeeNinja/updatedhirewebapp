import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingComponent } from './viewsetting/setting.component'
import { UserprofileComponent } from './userprofile/userprofile.component'
import {PharmaciesComponent} from './pharmacies/pharmacies.component'
import {SettingsRoutingModule} from './settings-routing.module'
import { from } from 'rxjs';
import { ToastrModule, ToastNoAnimation, ToastNoAnimationModule } from 'ngx-toastr';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProfileComponent } from './profile/profile.component';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { MembershipComponent } from './membership/membership.component';
import { PaymentviewComponent } from './payment/paymentview/paymentview.component';
import { CheckoutComponent } from './payment/checkout/checkout.component';
import { CheckoutachComponent } from './payment/checkoutach/checkoutach.component';
import { MakepaymentComponent } from './payment/makepayment/makepayment.component';
import {ChangePasswordComponent} from './change-password/change-password.component'


@NgModule({
  declarations: [SettingComponent,
    UserprofileComponent,   
     ProfileComponent,
  PharmaciesComponent,
  MembershipComponent,
  PaymentviewComponent,
  CheckoutComponent,
  CheckoutachComponent,
  MakepaymentComponent,
  ChangePasswordComponent
  ],
  imports: [
   CarouselModule,
    CommonModule,
    SettingsRoutingModule,
    ReactiveFormsModule,
     HttpClientModule,
    NgbModule,
    FormsModule,
    GooglePlaceModule,
    ToastNoAnimationModule.forRoot(),

  ]
})
export class SettingsModule { }
