import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { AppSettings } from '../../app.settings';
import { Observable, throwError, from, Subject } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Headers, RequestOptions, Request, RequestMethod } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  constructor(private http: HttpClient) {}

  sideBarObject = new Subject<object>();

  public getuserdetails(authid): Observable<any> {
    return this.http
      .get(
        AppSettings.apiurls.GETAUTHUSER + authid
      )
      .pipe(
        map((res) => {
          return res;
        }),
        catchError(this.errorHandler)
      );
   }

   public getharmacydetailbypharmacyid(pharmacyrefid): Observable<any> {
    return this.http
      .get(
        AppSettings.apiurls.GETPHARMACYDETAILSBYPHARMACYID + pharmacyrefid
      )
      .pipe(
        map((res) => {
          return res;
        }),
        catchError(this.errorHandler)
      );
   }

   updatepharmacy(data, pharmacyrefid) {
    return this.http
      .put<any>(AppSettings.apiurls.UPDATEPHARMACY + pharmacyrefid, data)
      .pipe(
        map((updateauth) => {
      
          return JSON.stringify(updateauth);
        }),
        catchError(this.errorHandler)
      );
    }

    updateauth(data, authid) {
    return this.http
      .put<any>(AppSettings.apiurls.GETAUTHUSER + authid, data)
      .pipe(
        map((updateauth) => {
        
          return JSON.stringify(updateauth);
        }),
        catchError(this.errorHandler)
      );
    }

    public getpresignedurlbyauthid(authid): Observable<any> {
      return this.http
        .get(
          AppSettings.apiurls.PRESIGNEDURL + authid
        )
        .pipe(
          map((res) => {
            return res;
          }),
          catchError(this.errorHandler)
        );
     }

     public getsignedprofileurl(authid): Observable<any> {
      return this.http
        .get(
          AppSettings.apiurls.GETSIGNEDPROFILEURL + authid,{responseType: 'text'}
        )
        .pipe(
          map((res) => {
            console.log(res)
            return res;
          }),
          catchError(this.errorHandler)
        );
     }

    // getPaymentMethodsByStripeCustomerId(data) {
    //   return this.http
    //     .get<any>(AppSettings.apiurls.GETPAYMENTMETHODSBYSTRIPECUSTOMERID, data)
    //     .pipe(
    //       map((stripeCustomerDetails) => {
    //         console.log(
    //           'Pharmacy User Data--------->',
    //           JSON.stringify(stripeCustomerDetails)
    //         );
    //         return JSON.stringify(stripeCustomerDetails);
    //       }),
    //       catchError(this.errorHandler)
    //     );
    //   }


    errorHandler(error: any) {
    console.log('Error--->', error);
    return throwError(error);
  }
}
