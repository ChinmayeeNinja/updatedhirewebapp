import { Component, OnInit } from '@angular/core';
import { from,Observable,Subscriber } from 'rxjs';
import { SettingsService } from '../../settings/settings.service'
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from '../../../toastr.service'

import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.css']
})
export class UserprofileComponent implements OnInit {
  Object = Object;

  public userdatafirstname :any
  public userdatalastname:any

  formVar: FormGroup;
  submitted =false
  public errorMessage: any
  public authenticationId : any;
  public profileimg :any;
  public image :any;
  public urlll :any;
  public imageurl:any
  public modalShow = false;
  public myimage :any;
  constructor(
    private setting: SettingsService,
    private router: Router,
    private fb: FormBuilder,
    private tosterservice: ToastrService,
    config: NgbPaginationConfig
  ) {
    config.size = 'sm';
    config.boundaryLinks = true;
  }
  ngOnInit(): void {
        this.authenticationId = localStorage.getItem('signupUserReferenceId');
    // this.userdata.push(JSON.parse(localStorage.getItem('currentUser')))
  //  var dataa=JSON.parse(localStorage.getItem('currentUser'))
     this.formVar = this.fb.group({
       firstname: ['', [Validators.required]],
             email: ['', [Validators.required]],
      lastname: ['', [Validators.required]],
      phone: ['',  [Validators.required,Validators.pattern(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/)
    ]],
    profileImageUrl:'profileImage_5d8acada-faac-11ea-a443-169943198335_03042021092209',
    status :'active'

     });
     
     this.getuser(this.authenticationId)
  }
  get f() {
    return this.formVar.controls;
  }
   onlyNumber(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;

    }
      
    return true;
  }
  onlyText(event): boolean {
     const charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode >= 48 && charCode <= 57 )&& (charCode >= 96 || charCode <= 105)) {
      return false;

    }
        return true;

  }
  addpharmacy() {
      this.router.navigate(['/pharmacy/settings/profile']);

  }
  changepassword() {
          this.router.navigate(['/pharmacy/settings/changepassword']);
  }
  subscriptionplan() {
              this.router.navigate(['/pharmacy/settings/membership'],{queryParams: { membershipid: true },
              });
    

  }
  payment() {
              this.router.navigate(['/pharmacy/settings/paymentviewcard']);
  }

  getuser(authid): void {
    this.setting
      .getuserdetails(authid)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            var dataaa = data;
            this.imageurl =dataaa?.data.profileImageUrl
            this.userdatalastname = dataaa?.data.lastname;
            this.userdatafirstname = dataaa?.data.firstname;
            this.getpresignedprofileimg(dataaa?.data.profileImageUrl)
            this.errorMessage = '';
            this.formVar.patchValue({
              firstname: dataaa?.data?.firstname,
       email: dataaa?.data?.email,
          lastname: dataaa?.data?.lastname,
              phone: dataaa?.data?.phone,

            });
        this.formVar.get('email').disable();
          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }


  onSubmit() {
          this.submitted = true;
    if (this.formVar.invalid) {
      return;
    }
     this.setting.updateauth(this.formVar.value, this.authenticationId).subscribe(
        (data) => {
          if (data) {
            this.tosterservice.Success('Your Details Update Sucessfully')

            this.getuser(this.authenticationId)

          } else {
            this.errorMessage = 'Unable to Save Jobpost';
          }
        },
        (error) => {
          this.errorMessage = error.error;
        }
      );
  }

  getpresignedurl(authid): void {
    this.setting
      .getpresignedurlbyauthid(authid)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            console.log(data)
            this.profileimg = data.filename
            this.urlll = data.url
            console.log(this.profileimg)
            fetch(this.urlll, {
              method: "put",
              headers: {
                "Content-Type": "binary",
              },
              body: this.myimage
            }).then((result) => {
              result.json().then((resp) => {
                //("Restuent has been added")
                console.log(resp);
              });
            });
          }
          else  {
            this.errorMessage = 'Unable to fetch Url';
          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }


  onChange($event: Event) {
    const file = ($event.target as HTMLInputElement).files[0];
    this.convertToBase64(file);
  }

  convertToBase64(file: File) {
   const observable = new Observable((subscriber: Subscriber<any>) => {
      this.readFile(file, subscriber);
    });
    observable.subscribe((d)=>{
      this.myimage =d
      console.log(this.myimage)
      this.getpresignedurl(this.authenticationId)

    })
  }
 

  readFile(file: File, subscriber: Subscriber<any>) {
    const filereader = new FileReader();
    filereader.readAsDataURL(file);

    filereader.onload = () => {
      subscriber.next(filereader.result);
      subscriber.complete();
    };
    filereader.onerror = (error) => {
      subscriber.error(error);
      subscriber.complete();
    };
  }

  getpresignedprofileimg(profileimg): void {
    this.setting
      .getsignedprofileurl(profileimg)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.image=data
          }
          else  {
            this.errorMessage = 'Unable to fetch img';
          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }
  openimg(){
    this.modalShow = !this.modalShow;
  }
  modalHideShoww() {
    this.modalShow = !this.modalShow;
  }
}
