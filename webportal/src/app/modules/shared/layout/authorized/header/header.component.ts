import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
public waletpoint :any;
  constructor(
    private router: Router,

  ) { }

  ngOnInit(): void {
this.waletpoint=JSON.parse(localStorage.getItem('pharmacyUserId')).data.walletPoints
  }
  gotowallet(){
    this.router.navigate(['pharmacy/wallet']);

  }
}
