import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { JobpostService } from '../../../../../jobpost/jobpost.service';

@Component({
  selector: 'app-jobpost-progressbar',
  templateUrl: './jobpost-progressbar.component.html',
  styleUrls: ['./jobpost-progressbar.component.css'],
})
export class JobpostProgressbarComponent implements OnInit {
  public sideBarObject: any;
  public sidebarvalue: any;
  public sidebarobjj: {};
  public topprogressbarlinee :any
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private JobpostService: JobpostService
  ) {
    this.JobpostService.sideBarObject.subscribe((sideBarObject) => {
      this.sideBarObject = sideBarObject;
    });
    var dd = JSON.stringify(this.sideBarObject);
    localStorage.setItem('sideBarObject', dd);
  }
  ngOnInit(): void {
    this.sideBarObject = JSON.parse(
      localStorage.getItem('sidebarstringifyobj')
    );
    this.topprogressbarlinee = JSON.parse(localStorage.getItem('topprogressbarline'))
    console.log(this.sideBarObject)
  }
}
