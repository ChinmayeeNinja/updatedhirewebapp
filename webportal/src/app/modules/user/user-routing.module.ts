import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import {AddsubuserComponent} from '../addsubuser/addsubuser.component'
import {UserComponent} from '../user/user.component'
import { from } from 'rxjs';

const routes: Routes = [
  {
    path: '',
    component: UserComponent,
    // children: [
    //   { path: 'adduser', component: AddsubuserComponent },
  
    // ],
  },
    {
    path: 'adduser',
    component: AddsubuserComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],

})export class UserRoutingModule { }
