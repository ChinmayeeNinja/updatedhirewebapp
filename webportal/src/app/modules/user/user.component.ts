import { Component, OnInit } from '@angular/core';
import { UserService } from '../user/user.service';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  page=1;
public referenceid :any
public errorMessage :any;
public sabuserlist=[]
public collectionsize :any
  constructor(
    private user: UserService,
    private router: Router,
    private spinner: NgxSpinnerService,

    config: NgbPaginationConfig
  ) {
    config.size = 'sm';
    config.boundaryLinks = true;
  }
  ngOnInit(): void {
    // this.authenticationId = localStorage.getItem('signupUserReferenceId');
   var pharmacyrefernceid = JSON.parse(localStorage.getItem('pharmacyUserId')) 
    this.referenceid =pharmacyrefernceid.data.referenceId
    
    this.getsubuserlist(this.referenceid,this.page)

  }
  adnewuser(){
    this.router.navigate(['pharmacy/user/adduser'],);
      localStorage.removeItem('subuseritem')

  }
  edit(userReferenceID,item){
    this.router.navigate(['pharmacy/user/adduser'],{      queryParams: { edit: true,disabled:true},


  });
  localStorage.setItem('subuseritem',JSON.stringify(item))
}

Delete(userReferenceID){
  var referenceid = userReferenceID
    var requestt = {
      isDeleted:1  
     };
    //console.log(request);
    Swal.fire({
      title: 'Are you sure?',
      text: "You want to delete this user!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Deleted!',
          'Your user has been deleted.',
          'success'
        )
        this.user.Updatepharmacysubuser(requestt, referenceid).subscribe(
          (data) => {
            if (data) {
              this.getsubuserlist(this.referenceid,this.page)
            } else {
              this.errorMessage = 'Unable to Save user';
            }
          },
          (error) => {
            this.errorMessage = error.error;
          }
        );
      }
    })
  
}

  reloadDataa() {
    this.getsubuserlist(this.referenceid,this.page)
  }
  getsubuserlist(referenceid, page): void {
                   this.spinner.show();

    this.user
      .Getsabuselist(referenceid, page)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.errorMessage = '';
            this.sabuserlist = data.data.listResult;
            this.collectionsize = data.data.pagination.totalCount
                   this.spinner.hide();


          }
        },
        (error) => {
          this.errorMessage = error.text;
        }
      );
  }
}
