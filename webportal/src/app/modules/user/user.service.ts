import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { AppSettings } from '../../app.settings';
import { Observable, throwError, from, Subject } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Headers, RequestOptions, Request, RequestMethod } from '@angular/http';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {}

  public Getsabuselist(referenceid, page): Observable<any> {
    console.log(referenceid);
    return this.http
      .get(
        AppSettings.apiurls.GETPHARMACYSUBUSER +
        referenceid +
          `?limit=10&offset=${(page - 1) * 10}`
      )
      .pipe(
        map((res) => {
          console.log(res);
          return res;
        }),
        catchError(this.errorHandler)
      );
  }
  
  Createsubuser(data) {
    return this.http
      .post<any>(AppSettings.apiurls.CREATEPHARMACYSUBUSER, { ...data })
      .pipe(
        map((createsubuser) => {
          console.log('Pharmacy User Data--------->', JSON.stringify(createsubuser));
          return createsubuser;
        }),
        catchError(this.errorHandler)
      );
  }

  Updatepharmacysubuser(subuserData, subuserreferenceid) {
    console.log(subuserreferenceid);
    console.log(AppSettings.apiurls.UPDATEPHARMACYSUBUSER + subuserreferenceid);
    return this.http
      .put<any>(AppSettings.apiurls.UPDATEPHARMACYSUBUSER + subuserreferenceid, subuserData)
      .pipe(
        map((updateuser) => {
          console.log(
            'Pharmacy User Data--------->',
            JSON.stringify(updateuser)
          );
          return JSON.stringify(updateuser);
        }),
        catchError(this.errorHandler)
      );
  }


  
  errorHandler(error: any) {
    console.log('Error--->', error);
    return throwError(error);
  }}
