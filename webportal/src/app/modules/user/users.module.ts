import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddsubuserComponent } from '../addsubuser/addsubuser.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule, ToastNoAnimation, ToastNoAnimationModule } from 'ngx-toastr';
import {UserComponent} from './user.component'
import { from } from 'rxjs';
import {UserRoutingModule} from '../user/user-routing.module'

@NgModule({
  declarations: [AddsubuserComponent,UserComponent],
  imports: [
    UserRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    HttpClientModule,
    NgbModule,
    ToastNoAnimationModule.forRoot(),
  ]
})
export class UsersModule { }
