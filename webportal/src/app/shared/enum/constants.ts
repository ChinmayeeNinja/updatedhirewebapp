import { parseHostBindings } from '@angular/compiler';

export const role = {
  SYSTEMADMIN: 1,
  PHARMACY: 2,
  PHARMACIST: 3,
  PHARMACYSUBUSER: 4,
  SUPERADMINSUBUSER: 5,
};

export const status = {
  PENDING: 'pending',
  ACTIVE: 'active',
  DECLINED: 'declined',
};

export const expertise = [
  { key: 'Retail', Value: 1 },
  { key: 'Hospital', Value: 2 },
  { key: 'Institutional', Value: 3 },
  { key: 'Long Term Care', Value: 4 },
  { key: 'Mail Order', Value: 5 },
];

export const additionalSkills = [
  { key: 'Vaccination ', Value: 1 },
  { key: 'Compounding', Value: 2 },
  { key: 'MTM', Value: 3 },
];

export const pharmacyOperation = [
  { key: 'Beginner ', Value: 1 },
  { key: 'Intermediate', Value: 2 },
  { key: 'Advanced', Value: 3 },
];

export const softwareSkills = [
  { key: 'Cerner Retail Pharmacy ', Value: 1 },
  { key: 'McKesson Pharmacy Systems', Value: 2 },
  { key: 'PrimeRx', Value: 3 },
  { key: 'Winpharm', Value: 4 },
  { key: 'NRx by QS1', Value: 5 },
  { key: 'RX30 Pharmacy System', Value: 6 },
  { key: 'Computer-Rx', Value: 7 },
  { key: 'PDX Classic', Value: 8 },
  { key: 'AbacusRx', Value: 9 },
  { key: 'BestRx', Value: 10 },
  { key: 'FrameworkLTC', Value: 11 },
  { key: 'ScriptPro', Value: 12 },
  { key: 'Chetu Pharmacy', Value: 13 },
  { key: 'PROScript 2000', Value: 14 },
  { key: 'SuiteRx', Value: 15 },
  { key: 'Advanced Rx', Value: 16 },
  { key: 'GuardianRx', Value: 17 },
  { key: 'PKon Rx by SRS ', Value: 18 },
  { key: 'RxMaster', Value: 19 },
  { key: 'RXQ by Liberty software', Value: 20 },
  { key: 'SharpRx by QS!', Value: 21 },
  { key: 'hCue Pharmacy', Value: 22 },
  { key: 'NewLeafRx', Value: 23 },
  { key: 'Asembia1', Value: 24 },
  { key: 'RMS', Value: 25 },
  { key: 'PioneerRx', Value: 26 },
  { key: 'WinRx', Value: 27 },
  { key: 'VIP Pharmacy', Value: 28 },
  { key: 'Pharmaserv', Value: 29 },
  { key: 'RxGenesys Suite', Value: 30 },
  { key: 'RxAxis Suite', Value: 31 },
];

export const jobVisibility = [
  { key: 'Anyone ', Value: 1 },
  { key: 'Only Hire Talent', Value: 2 },
  { key: 'Invite Only', Value: 3 },
];

export const PaymentType = [
  { key: 'Pay by Hour ', Value: 10 },
  { key: 'Fixed Price', Value: 100 },
];
export const Section = [
  { key: 'Jobs', Value: false, },
  { key: 'Hire', Value: false },
  // { key: 'Invitation', Value: true },
  { key: 'Message', Value: false },
  { key: 'Wallet', Value: false },
  { key: 'Contracts', Value: false },

];
export const Superadminsection = [
  { key: 'Pharmacy', Value: false, },
  { key: 'Pharmacist', Value: false },
  { key: 'Voucher', Value: false },
  { key: 'CredentialRequest', Value: false },
  { key: 'User', Value: false },
  { key: 'Membership', Value: false },


];
export const Paymentmethod = [
  { key: 'Credit/Debit', Value: 'Credit/Debit' },
  { key: 'Connect Bank', Value:'Connect Bank'},
]