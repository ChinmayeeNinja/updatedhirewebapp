import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class StatesService {
  constructor(private httpClient: HttpClient) {}

  public getUSStates(): Observable<any> {
    return this.httpClient.get('./assets/states-cities/states-cities.json');
  }
}
