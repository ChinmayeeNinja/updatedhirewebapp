export class States {
  Id: number;
  Name: string;

  constructor(id: number, name: string) {
    this.Id = id;
    this.Name = name;
  }
}
