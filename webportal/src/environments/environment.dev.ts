export const environment = {
  production: false,
  APIURLS: {
    GETAUTHUSER: 'http://dev-api.covermyday.com:81/auth/',
    USERSIGNUP: 'http://dev-api.covermyday.com:81/auth/signup',
    USERSIGNIN: 'http://dev-api.covermyday.com:81/auth/signin',
    GENERATECODE: 'http://dev-api.covermyday.com:81/auth/code',
    VERIFYCODE: 'http://dev-api.covermyday.com:81/auth/verifycode',
    FORGOTPASSWORD: 'http://dev-api.covermyday.com:81/auth/forgotpassword',
    CREATEPHARMACYUSER: 'http://dev-api.covermyday.com:81/pharmacy/pharmacyUser/create',
    CREATEPHARMACY: 'http://dev-api.covermyday.com:81/pharmacy/create',
    LISTPHARMACY: 'http://dev-api.covermyday.com:81/pharmacy/pharmacyUser/auth/',
    UPDATEPHARMACY: 'http://dev-api.covermyday.com:81/pharmacy/',
    GETPHARMACYDETAILSBYPHARMACYID: 'http://dev-api.covermyday.com:81/pharmacy/',
    CREATEJOBPOST: 'http://dev-api.covermyday.com:81/pharmacy/jobpost/create',
    GETJOBPOSTBYID: 'http://dev-api.covermyday.com:81/pharmacy/jobpost/',
    UPDATEJOBPOST: 'http://dev-api.covermyday.com:81/pharmacy/jobpost/',
    GETPHARMACYUSERBYAUTHID:
      'http://dev-api.covermyday.com:81/pharmacy/pharmacyUser/auth/',
    GETPHARMACYDETAILSBYAUTHID: 'http://dev-api.covermyday.com:81/pharmacy/auth',
    GETPHARMACYJOBLISTING:
      'http://dev-api.covermyday.com:81/superadmin/pharmacy/',
    LISTPHARMACYCONTRACT:
      'http://dev-api.covermyday.com:81/pharmacy/listcontract/',
    GETMEMBERSHIP: 'http://dev-api.covermyday.com:81/superadmin/membership/pharmacy?state=',
    PHARMACYMEMBERSHIPBYID: 'http://dev-api.covermyday.com:81/pharmacy/pharmacyUser/',
    PAYMENTDETAILS: 'http://dev-api.covermyday.com:81/payment/s/config',
    // CREATECUSTOMER:
    //   'http://dev-api.covermyday.com:81/payment/s/cc/createcustomer',
          CREATECUSTOMER: 'http://dev-api.covermyday.com:81/payment/s/createcustomer',

      // CREATECUSTOMERACH:
      // 'http://dev-api.covermyday.com:81/payment/s/ach/createcustomer',
    SEARCHPHARMACIST: 'http://dev-api.covermyday.com:81/pharmacy/search/',
    SAVEPHARMACIST: 'http://dev-api.covermyday.com:81/pharmacy/savedpharmacist',
    SAVEDPHARMACISTLIST:
      'http://dev-api.covermyday.com:81/pharmacy/savedpharmacist/',
    INVITEPHARMACIST:
      'http://dev-api.covermyday.com:81/pharmacy/invitepharmacist',
    INVITEDPHARMACIST:
      'http://dev-api.covermyday.com:81/pharmacy/invitedpharmacist/',
    MYHIRE: 'http://dev-api.covermyday.com:81/pharmacy/hiredpharmacist/',
    LISTPROPOSAL: 'http://dev-api.covermyday.com:81/pharmacy/listproposal?',
    UPDATEPROPOSAL: 'http://dev-api.covermyday.com:81/pharmacy/proposal/',
    CREATECONTRACT: 'http://dev-api.covermyday.com:81/pharmacy/createcontract',
    LISTCONTRACTS: 'http://dev-api.covermyday.com:81/pharmacy/listcontract/',
    VIEWPROPOSAL: 'http://dev-api.covermyday.com:81/pharmacy/proposal/',
    WRITEREVIEW: 'http://dev-api.covermyday.com:81/pharmacy/createreview/',
    GETPHARMACYSUBUSER:'http://dev-api.covermyday.com:81/pharmacy/userpermission/',
    CREATEPHARMACYSUBUSER:'http://dev-api.covermyday.com:81/pharmacy/user/',
    UPDATEPHARMACYSUBUSER:'http://dev-api.covermyday.com:81/pharmacy/userpermission/',
    LISTHIRE: 'http://dev-api.covermyday.com:81/pharmacy/hire/',
    PHARMACYTRASCATION:'http://dev-api.covermyday.com:81/pharmacy/transactions/',
    REDEEMEDVOUCHER:'http://dev-api.covermyday.com:81/superadmin/voucher/redeem',
    RESETPASSWORD: 'http://dev-api.covermyday.com:81/auth/validateresetpasswordlink/',
    RESETPASSWORDFORM:'http://dev-api.covermyday.com:81/auth/resetpassword/', 
    GETSUBUSERBYAUTHONTICATIONID: 'http://dev-api.covermyday.com:81/pharmacy/user/',
    CHANGEPASSWORD: 'http://dev-api.covermyday.com:81/auth/changepassword/',
    LISTJOBPOSTBASEDONPHARMACYUSERID: 'http://dev-api.covermyday.com:81/pharmacy/jobpost/list/',
    UPDATECONTACTBASEUPONCONTRACTREFERENCEID: 'http://dev-api.covermyday.com:81/pharmacy/contarct/',
    UPDATESAVEDPHARMACIST:"http://dev-api.covermyday.com:81/pharmacy/savedpharmacist/",
    LISTCURRENTUSERSPAYMENTMETHODS: 'http://dev-api.covermyday.com:81/payment/s/modes',
    GETCUSTOMER: 'http://dev-api.covermyday.com:81/payment/s/customer',
    PAY: 'http://dev-api.covermyday.com:81/payment/s/pay',
    PAYMENTMODES: 'http://dev-api.covermyday.com:81/payment/s/modes',
    CCPAYMENTATTACHED: 'http://dev-api.covermyday.com:81/payment/s/paymethod/attach',
    ACHPAYMENTMETHOD: 'http://dev-api.covermyday.com:81/payment/s/ach/create',
      UPDATEPAYMENT: 'http://dev-api.covermyday.com:81/payment/s/customer/',
      DEFAULTKEY:'http://dev-api.covermyday.com:81/payment/s/default/paymentmethod/',
      CALCULATECONTRACTCHARGES:'http://dev-api.covermyday.com:81/pharmacy/contract/charges/',
    GETWALLETPOINT:'http://dev-api.covermyday.com:81/pharmacy/pharmacyUser/auth/',
      UPDATECONTRACTUSERREFID:'http://dev-api.covermyday.com:81/pharmacy/contarct/',
      CREATEPAYMENTTRANSATION:'http://dev-api.covermyday.com:81/payment/s/create/transaction',
      LISTMYHIREBYPHARMACYUSERREFERENCEID:'http://dev-api.covermyday.com:81/pharmacy/myhires/',
      GETPHARMACISTBYPHAMACISTREFEMECEID:'http://dev-api.covermyday.com:81/pharmacist/profile/details/',
      CREATEMESSAGE:'http://dev-api.covermyday.com:9500/message',
      GETMESSAGEHISTORY:'http://dev-api.covermyday.com:9500/message/getall',
      PRESIGNEDURLIMG:"http://dev-api.covermyday.com:9001/signedurl/",
      GETSIGNEDPROFILEURL:"http://dev-api.covermyday.com:9001/get/",
    // superadmin
    SUPERADMINDASHBOARD: 'http://dev-api.covermyday.com:81/superadmin/dashboard',
    SUPERADMINPHARMACYLIST: 'http://dev-api.covermyday.com:81/superadmin/pharmacy',
    SUPERADMINPHARMACISTLIST: 'http://dev-api.covermyday.com:81/superadmin/pharmacist',
    GETPHARMACYDETAILFORSUPERADMIN: 'http://dev-api.covermyday.com:81/superadmin/pharmacy/',
    LISTVOUCHER: 'http://dev-api.covermyday.com:81/superadmin/voucher/list',
    CREATEVOUCHER:'http://dev-api.covermyday.com:81/superadmin/voucher/create',
    LISTPHARMACYCREDENTIALREQUEST:'http://dev-api.covermyday.com:81/superadmin/credential?isPharmacy=1',
    LISTPHARMACISTCREDENTIALREQUEST:'http://dev-api.covermyday.com:81/superadmin/credential?isPharmacy=0',
    CREATEMEMBERSHIP:'http://dev-api.covermyday.com:81/superadmin/membership/create',
    LISTMEMBERSHIP:'http://dev-api.covermyday.com:81/superadmin/membership/pharmacy?state=all',
    UPDATEMEMBERSHIP:'http://dev-api.covermyday.com:81/superadmin/membership/',
     UPDATEPHARMACIST :'http://dev-api.covermyday.com:81/pharmacist/',
UPDATECREDENTIAL:'http://dev-api.covermyday.com:81/pharmacist/credential/',
    VIEWPHARMACISTLIST:'http://dev-api.covermyday.com:81/superadmin/pharmacist/',
    SUPERADMINUSERLIST:'http://dev-api.covermyday.com:81/pharmacy/superadmin/users',



  },
};




